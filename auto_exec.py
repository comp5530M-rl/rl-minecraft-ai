
import pathlib
import sys
import subprocess
import psutil


def kill(proc_pid):
    process = psutil.Process(proc_pid)
    for proc in process.children(recursive=True):
        proc.kill()
    process.kill()

if __name__ == "__main__":
    # p = subprocess.run(["python", "start_training.py"])
    while True:
        p = subprocess.Popen([sys.executable, str(pathlib.Path(__file__).parent.joinpath("train_dreamer.py").absolute())])
        try:
            p.wait(timeout=120) # restart ery 20 mins
        except subprocess.TimeoutExpired:
            print("Timeout expired, killing process")
            kill(p.pid)
            print("Process killed")

