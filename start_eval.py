from mindcraft.Eval import evaluate_agent, EvalCollection

if __name__ == "__main__":
    evaluate_agent(EvalCollection.ALL, repeats=1, debug=True)