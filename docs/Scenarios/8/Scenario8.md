# Test suite 8

## World structure
* Empty brimstone box
* Food sources located in each corner
* Hostile target present at each food source location

## Player & Inventory structure
* Agent is equipped with diamond sword and armor
* Agent has 0 saturation (to ensure health starts ticking down and only source of food being locked behind hostiles)

## Scoring
total_score = total_enemies_defeated + food_items_consumed.

## Motivation
This test assesses the hostile/avoidance capabilities of our agents, as well as their ability to
incorporate food consumption in between battles. 
