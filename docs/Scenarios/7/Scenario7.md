# Test suite 7

## World structure
* Box contains food item placed at the end of obstacle path
* Separate tests can be implemented for the following obstacles:
    * maze - walls as high as the box, allow turns in both directions sometimes, one leading to dead-end
    * bridge on top of lava
    * water pool that is wide enough to require swimming or may cause drowning, food is located on the opposite bank
    * completely dark straight tunnel, exactly 1 block wide and walled off on all sides

## Player structure
* Agent can be spawned with saturated hunger (20) depending on complexity of task (if maze requires substantial time to solve).
* Alternatively, agent can be spawned with 0 saturation, such that it is on a timer and stimulated

## Scoring
positive if the food at the end of the obstacle was consumed, negative otherwise.

## Motivation
If successful up to this stage, the agent already demonstrates intelligent behaviour in a survival context. It can therefore be challenged by creating more sophisticated obstacles in its path to food