# Test suite 1

## World structure
Empty brimstone box (size of box is irrelevant) 

## Player structure
Agent is spawned with full hunger (0 saturation)

## Inventory structure
* Agent is equipped with any food item in hand (hotbar slot 0)
* Stack size of food item is irrelevant

## Scoring
* Positive if food was consumed (health restored)
* Negative otherwise (use deadline in case agent does not eat or track death)

## Motivation
This environment tests whether the agent can perform a simple vegetative task, which is a capability of all life-forms and even their infants.   
