# Test suite 3

## World structure
* Empty brimstone box
* Box contains food item dropped on floor, positioned in front of the player 

## Player structure
* Agent is spawned with full hunger (0 saturation)

## Scoring
* Positive if food was consumed (health restored)
* Negative otherwise (use deadline timer in case agent does not eat or end on death)
* Trajectory to food can also be considered when scoring by subtracting 1 for
 each movement action issued.

## Motivation
This addresses the aspect of detecting nearby food and navigating towards it - which is again a capability of all life-forms but is significantly more difficult and may take more time to successfully learn to accomplish.