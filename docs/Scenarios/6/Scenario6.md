# Test suite 6

## World structure
* Box contains one or multiple food sources of the same type (either only sheep or only grain);
 exact location is irrelevant as long as it is not in close proximity to agent.
* Box contains no obstacles between agent and food source.

## Extensions
* Box contains an obstacle between the player and food source
    * a row of 1-block high elevation that spans the width of the plane (tests jumping capability)
    * a row of 1-block deep water (tests raising from water)
    * food is located on top of a hill, 3-4 blocks high (tests repeated jumps)
    * food is behind the player and all obstacles above are sequentially applied between agent and food.
    * sequential combinations of any of these may be applied

## Even further possible extensions
We can periodically spawn just enough food at certain locations, such that if the agent consumes food rationally, it will be sufficient for them to survive a longer period of time. 

## Player structure
Agent is spawned with full hunger (0 saturation)

## Scoring
total_score = hunger_restored – potential_food_restored (what the food item can restore). 

## Motivation
Agents are assessed on their ability to manage their food sources, i.e. do they understand the concept of rationing. Note that this is not something that even some more complicated life-forms are concerned about (for instance, the intrinsic programming of rats does not allow them to even recognise the notion of over-consumption and this often leads to fatalities if food is abundant). Planning requires more sophisticated intelligence. 