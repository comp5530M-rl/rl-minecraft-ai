# Test suite 2

## World structure
Empty brimstone box (size of box is irrelevant)

## Player structure
Agent is spawned with full hunger (0 saturation)

## Inventory structure
* Agent has any food item in random hotbar slot (1-8). DON'T use hotbar slot 0 for this test.
* Stack size of food item is irrelevant

## Scoring
* Positive if food was consumed (health restored)
* Negative otherwise (use deadline timer in case agent does not eat or end on death)

## Motivation
While all life forms eventually recognise available food sources, this is more frequently a learned mechanism - i.e. infancts may not have that capacity. 