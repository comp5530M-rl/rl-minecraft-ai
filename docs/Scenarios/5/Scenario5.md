# Test suite 5

## World structure
* Box contains one or multiple food sources of the same type (either only sheep or only grain);
 exact location is irrelevant as long as it is not in close proximity to agent.
* Box contains no obstacles between agent and food source.

## Special note
Separate tests should be implemented for acquisition of sheep vs grain in order to asses the ability of agents
to gather specific type of food.

## Extensions
* Box contains an obstacle between the player and food source
    * a row of 1-block high elevation that spans the width of the plane (tests jumping capability)
    * a row of 1-block deep water (tests raising from water)
    * food is located on top of a hill, 3-4 blocks high (tests repeated jumps)
    * food is behind the player and all obstacles above are sequentially applied between agent and food.
    * sequential combinations of any of these may be applied

## Player structure
*Agent is spawned with full hunger (0 saturation)

## Scoring
*Positive if food was consumed (health restored)
*Negative otherwise (use deadline timer in case agent does not eat or end on death)
*Trajectory to food can also be considered when scoring by subtracting 1 for each movement action issued (including jump).

## Motivation
The motivation is that after a certain age (which may vary depending on species), individuals are usually expected to scavenge their own food (no matter if they're carnivores or herbivores), food in real-world scenarios rarely lies around waiting to be consumed in a ready-shape. If successful at this stage, the agent will be exhibiting minimalistic intelligence, sufficient to be considered a capable 'life-form'.