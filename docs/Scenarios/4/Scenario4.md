# Test suite 4

## World structure
* Box contains food item dropped on floor, positioned ahead of the player
* Box contains an obstacle between the player and food item (which is visible to agent) each obstacle is implemented as a separate test scenario
    * a row of 1-block high elevation that spans the width of the plane (tests jumping capability)
    * a row of 1-block deep water (tests raising from water)
    * food is located on top of a hill, 3-4 blocks high (tests repeated jumps)
    * food is behind the player and all obstacles above are sequentially applied between agent and food.

## Special note
To make it easier one can add intermediary tests that contain more than one source of food. Which is also more realistic.

## Player structure
Agent is spawned with full hunger (0 saturation)

## Scoring
* Positive if food was consumed (health restored)
* Negative otherwise (use deadline timer in case agent does not eat or end on death)
* Trajectory to food can also be considered when scoring by subtracting 1 for each movement action issued (including jump).

## Motivation
the environments of life-forms is never obstacle free, food is scarce and has to be identified and navigated towards. Yet this is still a task many animals can perform (perhaps jumping and z-axis navigation already indicates higher life-form intelligence).