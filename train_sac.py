from stable_baselines3 import SAC
from stable_baselines3.common.vec_env import SubprocVecEnv, DummyVecEnv, VecMonitor, VecNormalize, VecCheckNan
from stable_baselines3.common.callbacks import CheckpointCallback
from stable_baselines3.common.env_util import make_vec_env

from mindcraft.Controller import ComputeManager
from mindcraft.Util import GLOBAL_CONFIG, ServerConfig, ControllerConfig, ClientGraphicsConfig
from mindcraft.RL.envs import MCEnv
import gym


def train_sac(manager: ComputeManager, num_instances=2, num_steps:int=5000, parallel:bool=False, training_interval=1000):
    
    # Create environment
    if num_instances == 1:
        env = gym.make('Minecraft-v0')
        env.connect_to_server(manager)
    else:
        if parallel:
            env = make_vec_env(MCEnv, n_envs=num_instances, vec_env_cls=SubprocVecEnv, vec_env_kwargs=dict(start_method='fork'))
        else:
            env = make_vec_env(MCEnv, n_envs=num_instances)
        env.env_method('connect_to_server', manager)
        env = VecCheckNan(env)
        env = VecMonitor(env)
        env = VecNormalize(env, norm_obs=False)


    # Load model if it exists, otherwise create new model
    try:
        model = SAC.load(
            "out/sac/model.zip",
            env,
            device="cuda",
            custom_objects={"n_envs": num_instances}, # May need to change this
        )
        print(f"Loaded model")
    except:
        print("model not found, creating new model")

        model = SAC(
            'MultiInputPolicy',
            env,
            verbose=1,
            buffer_size=1000,
            tensorboard_log="out/sac/logs/",
            device="cuda",
        )

    # Train model and save checkpoints
    callback = CheckpointCallback(training_interval, save_path="out/sac/checkpoints/", name_prefix="model")
    try:
        while True:
            model.learn(total_timesteps=num_steps, log_interval=10, callback=callback)
            model.save(f"out/sac/model")
    except KeyboardInterrupt:
        print("Saving model")
        model.save(f"out/sac/model")
        print("Save complete")


if __name__ == "__main__":

    GLOBAL_CONFIG["logging_level"] = "WARN"

    serverConfig = ServerConfig({
        "server_world":"survival9066_16x16",
        "break_modifier":0,
    })

    clientConfig = ControllerConfig({
        "graphics_config" : ClientGraphicsConfig({
            "hide_hud": False,
        })
    })

    computeManager = ComputeManager(serverConfig, clientConfig)
    computeManager.wait_for_server()

    train_sac(computeManager, num_instances=2, parallel=True)