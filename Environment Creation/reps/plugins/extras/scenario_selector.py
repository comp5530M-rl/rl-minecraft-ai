import wx

from typing import Tuple

from amulet.api.data_types import BlockCoordinates
from amulet.api.selection.box import SelectionBox

from amulet_map_editor.programs.edit.api.ui.select_location.select_location import SelectLocationUI
from amulet_map_editor.programs.edit.api.ui.select_location.events import EVT_LOCATION_CHANGE, LocationChangeEvent
from amulet_map_editor.api.opengl.camera import Projection

from plugins.extras.inventory_selector import InventorySelectorPanel
from plugins.extras.custom_render_selection import CustomRenderSelection
from plugin_data import data_interactor

class ScenarioSelectorPanel(wx.Panel):
    # Create a panel for the scenario selector
    def __init__(self, parent: wx.Window, canvas, **kwargs):
        # Initialise Panel and set Sizer to a vertical box
        super().__init__(parent, style=wx.BORDER_SIMPLE)
        self._sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self._sizer)

        # Retreive reward selection
        self._reward_functions = data_interactor.reward_data(
            ["get_food", "get_wood"]
        )

        # Create UI
        self._create_reward_input()
        self._create_health_input()
        self._create_xp_input()
        self._create_hunger_input()
        self._create_saturation_input()
        self._create_coordinate_input()
        self._create_coordinate_highlight(canvas)
        self._create_inventory_input()

        # Initialise canvas
        self.canvas = canvas

    def _create_reward_input(self):
        self._reward_choice = wx.ListBox(self, choices=self._reward_functions, style=wx.LB_MULTIPLE | wx.LB_SORT)
        self._add_row("Reward Functions:",self._reward_choice)

    def _create_xp_input(self):
        self._xp_input = wx.SpinCtrl(self,min=0,max=100,value="0")
        self._add_row("XP Level:",self._xp_input)

    def _create_hunger_input(self):
        self._hunger_input = wx.SpinCtrl(self,min=0,max=20,value="20")
        self._hunger_input.Bind(wx.EVT_SPINCTRL, self._saturation_validator)
        self._add_row("Hunger Level:",self._hunger_input)

    def _create_saturation_input(self):
        self._saturation_input = wx.SpinCtrl(self,min=0,max=20,value="20")
        self._saturation_input.Bind(wx.EVT_SPINCTRL, self._saturation_validator)
        self._add_row("Saturation Level:",self._saturation_input)

    def _create_health_input(self):
        self._health_input = wx.SpinCtrl(self,min=0,max=20,value="20")
        self._add_row("Hearts:",self._health_input)

    def _create_coordinate_input(self):
        self._add_row("Spawn Point:")
        self._select_tool = SelectLocationUI(self)
        for checkbox in [self._select_tool._copy_air,self._select_tool._copy_water,self._select_tool._copy_lava]:
            checkbox.GetContainingSizer().Hide(0)
            checkbox.GetContainingSizer().Hide(1)
        self._select_tool.Bind(EVT_LOCATION_CHANGE, self._selector_moved)
        self._add_row(None, self._select_tool)

    def _create_coordinate_highlight(self, canvas):
        self._selection = CustomRenderSelection(canvas.context_identifier, canvas.renderer.opengl_resource_pack, [0.9314,0.7863,0.3157])

    def _create_inventory_input(self):
        self._item_select = InventorySelectorPanel(self)
        self._add_row(None, self._item_select)

    def _selector_moved(self, event):
        global_pos = self._to_global_pos(event.location)
        self._selection.selection_box = SelectionBox(global_pos, list(map(lambda a: a+1 ,global_pos)))

    def _saturation_validator(self, _):
        self._saturation_input.Value = min(self._saturation_input.Value, self._hunger_input.Value)

    def draw(self):
        if self.canvas.camera.projection_mode == Projection.TOP_DOWN:
            camera = None
        else:
            camera = self.canvas.camera.location
        self._selection.draw(self.canvas.camera.transformation_matrix, camera)
    
    def _add_row(self, label=None, *inputs):
        # Create horizontal box to add label and elements
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        self._sizer.Add(sizer, 0, wx.EXPAND | wx.ALL, 5)

        # Add label to the box
        if label != None:
            text = wx.StaticText(self, label=label, style=wx.ALIGN_CENTER)
            sizer.Add(text, 1)

        #Add element to the box
        for i in inputs:
            sizer.Add(i, 2)

    def _to_global_pos(self, pos):
        if len(self.canvas.selection.selection_group) == 0:
            wx.MessageDialog(self, f"A selection is required to continue.", style=wx.OK,).ShowModal()
            return pos
        selection_min = self.canvas.selection.selection_group.min
        return list(map(lambda a,b: a+b,selection_min,pos))
    
    def _from_global_pos(self, pos):
        if len(self.canvas.selection.selection_group) == 0:
            wx.MessageDialog(self, f"A selection is required to continue.", style=wx.OK,).ShowModal()
            return pos
        selection_min = self.canvas.selection.selection_group.min
        return list(map(lambda a,b: b-a,selection_min,pos))

    @property
    def local_position(self) -> BlockCoordinates:
        return self._select_tool.location
    
    @local_position.setter
    def local_position(self, position: Tuple[int, int, int]):
        self._select_tool.location = position
        self._selector_moved(LocationChangeEvent(position))
    
    @property
    def global_position(self) -> BlockCoordinates:
        return self._to_global_pos(self.local_position)
    
    @global_position.setter
    def global_position(self, position: Tuple[int, int, int]):
        self.local_position = self._from_global_pos(position)

    @property
    def rewards(self) -> Tuple[str]:
        return list(self._reward_choice.GetString(i) for i in self._reward_choice.GetSelections())
    
    @rewards.setter
    def rewards(self, rewards: Tuple[str]):
        # Set selection state of each item in reward box if it is in given rewards parameter
        for item in self._reward_choice.GetItems():
            self._reward_choice.SetStringSelection(item, item in rewards)

    @property
    def inventory(self) -> Tuple[Tuple[int,int,str,str]]:
        return self._item_select.inventory
    
    @inventory.setter
    def inventory(self, inventory:Tuple[Tuple[int,int,str,str]]):
        self._item_select.inventory = inventory

    @property
    def health(self) -> int:
        return self._health_input.Value
    
    @health.setter
    def health(self, health:int):
        self._health_input.Value = health

    @property
    def xp(self) -> int:
        return self._xp_input.Value
    
    @xp.setter
    def xp(self, xp:int):
        self._xp_input.Value = xp

    @property
    def hunger(self) -> int:
        return self._hunger_input.Value
    
    @hunger.setter
    def hunger(self, hunger:int):
        self._hunger_input.Value = hunger

    @property
    def saturation(self) -> int:
        return self._saturation_input.Value
    
    @saturation.setter
    def saturation(self, saturation:int):
        self._saturation_input.Value = saturation