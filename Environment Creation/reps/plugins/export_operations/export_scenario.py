import wx, tempfile

from amulet.api.selection import SelectionGroup
from amulet.api.errors import ChunkLoadError
from amulet.api.data_types import Dimension, OperationReturnType, BlockCoordinates
from amulet.api.block import Block

from amulet.level.formats.sponge_schem import SpongeSchemFormatWrapper

from amulet_nbt import (
    AbstractBaseTag,
    StringTag,
    ListTag,
    CompoundTag,
    IntArrayTag,
    DoubleTag,
    ByteTag,
    FloatTag,
    IntTag,
    load as load_nbt,
    from_snbt
)

from amulet_map_editor.api.opengl.camera import Projection
from amulet_map_editor.api.wx.ui.version_select import VersionSelect
from amulet_map_editor.programs.edit.api.operations import (SimpleOperationPanel, OperationError)

from plugins.extras.scenario_selector import ScenarioSelectorPanel
 

class ExportScenario(SimpleOperationPanel):
    # Create a panel for the scenario export tool
    def __init__(self, parent: wx.Window, canvas, world, options_path: str):
        # Initialise panel and load any previously selected options
        SimpleOperationPanel.__init__(self, parent, canvas, world, options_path)
        self.options = self._load_options({})

        # Create UI
        self._create_file_input()
        self._create_version_input(world.translation_manager)
        self._create_scenario_input(canvas)
        self._add_run_button("Export")

        # Apply layout to screen
        self.Layout()

    def _create_file_input(self):
        self._file_picker = wx.FilePickerCtrl(self, path=self.options.get("path", ""), wildcard="Scenario file (*.scenario)|*.scenario", style=wx.FLP_USE_TEXTCTRL | wx.FLP_SAVE | wx.FLP_OVERWRITE_PROMPT)
        self._add_row(None, self._file_picker)

    def _create_version_input(self, translation_manager):
        self._version_define = VersionSelect(self, translation_manager, "java", (1,18,0), allow_universal=False)
        self._version_define._version_choice.Disable()
        self._version_define._platform_choice.Disable()
        self._add_row(None,self._version_define)

    def _create_scenario_input(self, canvas):
        self._scenario_picker = ScenarioSelectorPanel(self, canvas)
        self._scenario_picker.rewards = self.options.get("rewards", [])
        self._scenario_picker.local_position = self.options.get("position", (0, 0, 0))
        self._scenario_picker.inventory = self.options.get("inventory", ())
        self._scenario_picker.xp = self.options.get("xp", 0)
        self._scenario_picker.health = self.options.get("health", 20)
        self._scenario_picker.hunger = self.options.get("hunger", 20)
        self._scenario_picker.saturation = self.options.get("saturation", 20)
        self._add_row(None, self._scenario_picker)

    # Update draw function to include scenario picker
    def _on_draw(self, evt):
        self.canvas.renderer.start_draw()
        if self.canvas.camera.projection_mode == Projection.PERSPECTIVE:
            self.canvas.renderer.draw_sky_box()
        self.canvas.renderer.draw_level()
        self._scenario_picker.draw()
        self._selection.draw()
        if self._show_pointer:
            self._pointer.draw()
        self.canvas.renderer.end_draw()

    # On disable save options for creating again later
    def disable(self):
        self._save_options({
            "path": self._file_picker.GetPath(),
            "rewards": self._scenario_picker.rewards,
            "position": self._scenario_picker.local_position,
            "inventory": self._scenario_picker.inventory,
            "xp": self._scenario_picker.xp,
            "health": self._scenario_picker.health,
            "hunger": self._scenario_picker.hunger,
            "saturation": self._scenario_picker.saturation
        })

    def _add_row(self, label=None, *inputs):
        # Create horizontal box to add label and elements
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        self._sizer.Add(sizer, 0, wx.EXPAND | wx.ALL, 5)

        # Add label to the box
        if label != None:
            text = wx.StaticText(self, label=label, style=wx.ALIGN_CENTER)
            sizer.Add(text, 1)

        #Add element to the box
        for i in inputs:
            sizer.Add(i, 2)

    def _inject_metadata(self, fieldName: str, NBT: AbstractBaseTag):
        if self._schematic.tag.get("Metadata") == None:
            self._schematic.tag["Metadata"] = CompoundTag({fieldName:NBT})
        else:
            self._schematic.tag["Metadata"][fieldName] = NBT

    def _inject_entity(self, entity: CompoundTag):
        if self._schematic.tag.get("Entities") == None:
            self._schematic.tag["Entities"] = ListTag([entity])
        else:
            self._schematic.tag["Entities"].append(entity)

    def _global_to_local_pos(self, position: BlockCoordinates):
        return self._scenario_picker._from_global_pos(position)
   
    def _operation(self, world, dimension: Dimension, selection: SelectionGroup) -> OperationReturnType:
        # Retreive version to export selection to
        path = self._file_picker.GetPath()
        schem_path = '.schem'.join(path.rsplit('.scenario', 1))
        platform = self._version_define.platform
        version = self._version_define.version_number

        # Check path is valid, and a platform & version is selected
        if not isinstance(path, str) or not platform or not version:
            raise OperationError("Please specify a save location in the options before running.")
        
        # Prepare to write in .construction format to the specified path
        wrapper = SpongeSchemFormatWrapper(schem_path)
        if wrapper.exists:
            response = wx.MessageDialog(self, f"A file is already present at {schem_path}. Do you want to continue?", style=wx.YES | wx.NO,).ShowModal()
            if response == wx.ID_NO:
                return
            
        # Filter out entity blocks within the selection before starting
        hidden_blocks = {}
        for x, y, z in selection.blocks:
            this_block = world.get_block(x,y,z,dimension)
            if this_block.namespace == "entity":
                hidden_blocks[(x,y,z)] = this_block
                world.set_version_block(x, y, z, dimension, ("java", (1, 18, 0)), Block("minecraft","air"))
            
        # Setup wrapper with specified selection
        wrapper.create_and_open(platform, version, selection, True)
        wrapper.translation_manager = world.translation_manager
        wrapper_dimension = wrapper.dimensions[0]

        # For chunk within selected blocks, get selected blocks within that chuck and append them to construction file
        chunk_count = len(list(selection.chunk_locations()))
        yield 0, f"Proccessing Chunks"
        for chunk_index, (cx, cz) in enumerate(selection.chunk_locations()):
            try:
                chunk = world.get_chunk(cx, cz, dimension)
                wrapper.commit_chunk(chunk, wrapper_dimension)
            except ChunkLoadError:
                continue
            yield (chunk_index + 1) / chunk_count / (4/5)
        # Save to a temporary folder for injection
        yield 0.8, f"Injecting Scenario"
        with tempfile.TemporaryFile(mode="wb+") as f:
            wrapper.save_to(f)
            f.seek(0)

            # Load text in NBT format version 2 of sponge schematic (https://github.com/SpongePowered/Schematic-Specification/blob/master/versions/schematic-2.md)
            text = load_nbt(f.read(), compressed=True)
            self._schematic = text

            # Inject rewards into metadata
            self._inject_metadata("RewardFunctions",ListTag(map(StringTag,self._scenario_picker.rewards)))

            # Create Player Dat
            playerDat = CompoundTag({})
            playerDat["Inventory"] = ListTag(map(lambda x: CompoundTag({
                **dict(from_snbt(x[3])),
                "Count":ByteTag(x[0]),
                "Slot":ByteTag(x[1]),
                "id":StringTag(x[2]),
            }),self._scenario_picker.inventory))
            playerDat["Pos"] = IntArrayTag(self._scenario_picker.local_position)
            playerDat["Health"] = FloatTag(self._scenario_picker.health)
            playerDat["foodLevel"] = IntTag(self._scenario_picker.hunger)
            playerDat["foodSaturationLevel"] = FloatTag(self._scenario_picker.saturation)
            playerDat["XpLevel"] = IntTag(self._scenario_picker.xp)

            self._inject_metadata("PlayerDat",playerDat)
            yield 0.9
            
            # Inject entities
            for i, ((x, y, z), block) in enumerate(hidden_blocks.items()):
                entity_nbt = from_snbt(block.properties["entity"].py_str)
                entity_nbt["Pos"] = ListTag(map(DoubleTag,self._global_to_local_pos((x,y,z))))
                self._inject_entity(entity_nbt)
                world.set_version_block(x, y, z, dimension, ("java", (1, 18, 0)), block)
                yield .9 + (i/len(hidden_blocks)/10)

            yield 1, f"Saving to File"
     
            # Save schematic to custom .scenario file
            print(self._schematic)
            self._schematic.save_to(path, compressed=True)
        wrapper.close()

export = {
    "name": "\tExport Scenario",  # the name of the plugin
    "operation": ExportScenario,  # the UI class to display
}