from typing import Tuple

from amulet_map_editor.api.opengl.mesh.selection.box.render_selection import RenderSelection


class CustomRenderSelection(RenderSelection):
    def __init__(self, context_identifier: str, resource_pack, colour: Tuple[float, float, float] = (1,1,1)):
        self.colour = colour
        super().__init__(context_identifier, resource_pack)

    @property
    def box_tint(self) -> Tuple[float, float, float]:
        return self.colour