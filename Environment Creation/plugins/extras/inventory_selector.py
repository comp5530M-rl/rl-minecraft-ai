import wx

from typing import Tuple

from plugin_data import data_interactor

class InventorySelectorPanel(wx.Panel):
    # Create a panel for the inventory selector
    def __init__(self, parent: wx.Window, **kwargs):
        # Initialise Panel and set Sizer to a vertical box
        super().__init__(parent, style=wx.BORDER_SIMPLE)
        self._sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self._sizer)

        # Retreive item selection
        self._item_names, self._item_codes = data_interactor.item_data(
            ["Diamond Pickaxe", "Diamond Sword", "Golden Carrot"],
            ["diamond_pickaxe", "diamond_sword", "golden_carrot"]
        )

        # Create UI
        self._add_row("Inventory:")
        self._create_table()
        self._create_count_input()
        self._create_slot_input()
        self._create_item_input()
        self._create_tag_input()
        self._create_buttons()

    def _create_table(self):
        self._table = wx.ListCtrl(self, style=wx.LC_REPORT)
        self._table.InsertColumn(0,"Count",width=45)
        self._table.InsertColumn(1,"Slot",width=40)
        self._table.InsertColumn(2,"ID",width=150)
        self._table.InsertColumn(3,"Tag")
        self._sizer.Add(self._table, 0, wx.EXPAND | wx.ALL, 5)

    def _create_count_input(self):
        self._count_input = wx.SpinCtrl(self,min=1,max=64,value="1")
        self._add_row("Count:",self._count_input)

    def _create_slot_input(self):
        self._slot_input = wx.SpinCtrl(self,min=-106,max=103,value="0")
        self._slot_input.Bind(wx.EVT_SPINCTRL, self._slot_validator)
        self._add_row("Slot:",self._slot_input)

    def _create_item_input(self):
        self._id_input = wx.Choice(self, choices = self._item_names, style = wx.CB_SORT)
        self._id_input.Select(0)
        self._add_row("Item:",self._id_input)

    def _create_tag_input(self):
        self._tag_input = wx.TextCtrl(self, value="{}")
        self._add_row("Tag:",self._tag_input)

    def _create_buttons(self):
        self._add_button = wx.Button(self, label="Add Item")
        self._add_button.Bind(wx.EVT_BUTTON, self._add_item)
        self._delete_button = wx.Button(self, label="Delete Item")
        self._delete_button.Bind(wx.EVT_BUTTON, self._delete_item)
        self._add_row(None, self._add_button, self._delete_button)

    def _add_item(self, _):
        # Check if slot exists for item, then add it to the table
        count = self._table.GetItemCount()
        for i in range(count):
            slot = int(self._table.GetItemText(i,1))
            if self.slot == slot:
                response = wx.MessageDialog(self, f"An item is already present at slot {slot}. Do you want to continue?", style=wx.YES | wx.NO,).ShowModal()
                if response == wx.ID_CANCEL:
                    return
                self._table.DeleteItem(i)
                break
        self._table.Append([self.count,self.slot,self.item_id,self.tag])

    def _delete_item(self, _):
        # Delete all selected items from the table
        selected = self._table.GetFirstSelected()
        if selected == -1:
            wx.MessageDialog(self, f"You must select an inventory item in order to delete it.", style= wx.OK,).ShowModal()
            return
        while selected > -1:
            self._table.DeleteItem(selected)
            selected = self._table.GetFirstSelected()

    def _add_row(self, label=None, *inputs):
        # Create horizontal box to add label and elements
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        self._sizer.Add(sizer, 0, wx.EXPAND | wx.ALL, 5)

        # Add label to the box
        if label != None:
            text = wx.StaticText(self, label=label, style=wx.ALIGN_CENTER)
            sizer.Add(text, 1)

        #Add element to the box
        for i in inputs:
            sizer.Add(i, 2)

    def _slot_validator(self, evt):
        pos = evt.GetPosition()
        # Check is pos is a forbidden slot, and continue to the next slot available
        if -105 <= pos < -50:
            self._slot_input.SetValue("0")
        elif 36 <= pos < 68:
            self._slot_input.SetValue("100")
        elif -50 <= pos < 0:   
            self._slot_input.SetValue("-106")
        elif 68 <= pos < 100:   
            self._slot_input.SetValue("35")

    @property
    def count(self) -> int:
        return self._count_input.GetValue()
    
    @property
    def slot(self) -> int:
        return self._slot_input.GetValue()
    
    @property
    def item_name(self) -> int:
        return self._id_input.GetStringSelection()
    
    @property
    def item_id(self) -> int:
        return "minecraft:"+self._item_codes[self._item_names.index(self.item_name)]
    
    @property
    def tag(self) -> int:
        return self._tag_input.GetValue()
    
    @property
    def inventory(self) -> Tuple[Tuple[int,int,str,str]]:
        count = self._table.GetItemCount()
        return tuple((
            int(self._table.GetItemText(i,0)),
            int(self._table.GetItemText(i,1)),
            self._table.GetItemText(i,2),
            self._table.GetItemText(i,3)
        ) for i in range(count))

    @inventory.setter
    def inventory(self, inventory: Tuple[Tuple[int,int,str,str]]):
        self._table.DeleteAllItems()
        for item in inventory:
            self._table.Append(item)