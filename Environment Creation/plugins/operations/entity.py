from amulet.api.selection import SelectionGroup
from amulet.api.level import BaseLevel
from amulet.api.data_types import Dimension
from amulet.api.block import Block

from amulet_nbt import (
    ByteTag,
    ShortTag,
    FloatTag,
    StringTag,
    ListTag,
    CompoundTag,
    from_snbt
)

from plugin_data import data_interactor

ENTITY_NAMES, ENTITY_CODES = data_interactor.entity_data(
    ["Creeper", "Zombie", "Phantom"],
    ["creeper", "zombie", "phantom"]
)

operation_options = {  # options is a dictionary where the key is the description shown to the user and the value describes how to build the UI
    "Entity Selection:": ["str_choice"] + ENTITY_NAMES,
    "Persistence Required:": ["bool", True],
    "Horizontal Rotation (clockwise from north):": ["float", -180, -180, 179.999],
    "Passengers:": ["str", "[]"],
    "Armor Items:": ["str","[]"],
    "Armor Drop Chance:": ["float",0,0,1],
    "Hand Items:": ["str", "[]"],
    "Hand Drop Chance:": ["float",0,0,1],
    "No AI:": ["bool", False],
    "No Gravity:": ["bool", False],
    "On Fire:": ["int", 0, 0, 32767],
    "Invunerable:": ["bool", False],
    "Health:": ["float", 0, 0, 1000],
    "Item Entity ID:": ["str", ""],
    "Item Entity Count:": ["int", 1, 1, 64],
}

def spawn_entity(world: BaseLevel, dimension: Dimension, selection: SelectionGroup, options: dict):
    global ENTITY_NAMES, ENTITY_CODES

    default_values = {
        "Passengers:": "[]",
        "Armor Items:": "[]",
        "Hand Items:": "[]",
        "Health:": float(0)
    }

    entity_code = ENTITY_CODES[ENTITY_NAMES.index(options["Entity Selection:"])]

    translator = {
        "Entity Selection:": lambda _: ("Id",StringTag("minecraft:"+entity_code)),
        "Persistence Required:": lambda x: ("PersistenceRequired",ByteTag(int(x))),
        "Horizontal Rotation (clockwise from north):": lambda x: ("Rotation",ListTag([FloatTag(x),FloatTag(0)])),
        "Passengers:": lambda x: ("Passengers",from_snbt(x)),
        "Armor Items:": lambda x: ("ArmorItems",from_snbt(x)),
        "Armor Drop Chance:": lambda x: ("ArmorDropChances",ListTag([FloatTag(x),FloatTag(x),FloatTag(x),FloatTag(x)])),
        "Hand Items:": lambda x: ("HandItems",from_snbt(x)),
        "Hand Drop Chance:": lambda x: ("HandDropChances",ListTag([FloatTag(x),FloatTag(x)])),
        "No AI:": lambda x: ("NoAI",ByteTag(int(x))),
        "No Gravity:": lambda x: ("NoGravity",ByteTag(int(x))),
        "On Fire:": lambda x: ("Fire", ShortTag(x)),
        "Invunerable:": lambda x: ("Invulnerable",ByteTag(int(x))),
        "Health:": lambda x: ("Health", FloatTag(x)),
        "Item Entity ID:": lambda x, y: ("Item",CompoundTag({"id": StringTag("minecraft:"+x), "Count": ByteTag(y)})),
    }
    
    entity_nbt = CompoundTag({})
    for key, value in options.items():
        if key in default_values.keys() and default_values[key] == value:
            continue
        print("translating "+key + " with value "+str(value))

        # ignore the item count, handled in ID
        if key == "Item Entity Count:": continue

        # apply entity ID and count only to item entities
        if key == "Item Entity ID:":
            if entity_code != "item": continue
            if value == "": continue
            string, tag = translator[key](value,options["Item Entity Count:"])
        else:
            string, tag = translator[key](value)
        entity_nbt[string] = tag

    valid_locations = []
    blacklist_stand_on = ["water","lava","air","cactus","cobweb","torch"]
    whitelist_stand_in = ["water","air","cobweb","torch"]

    for x, y, z in selection.blocks:
        block1 = world.get_block(x,y-1,z,dimension)
        block2 = world.get_block(x,y,z,dimension)
        block3 = world.get_block(x,y+1,z,dimension)
        if not block1.base_name in blacklist_stand_on and block2.base_name == "air" and block3.base_name in whitelist_stand_in:
            valid_locations.append((x, y, z))

    entity_block = Block("entity",entity_code,{"entity":StringTag(entity_nbt.to_snbt())})
    world.block_palette.register(entity_block) 

    for x, y, z in valid_locations:
        world.set_version_block(x, y, z, dimension, ("java", (1, 18, 0)), entity_block)


export = {
    "name": "Entity",
    "operation": spawn_entity,
    "options": operation_options,  # The options you defined above should be added here to show in the UI
}