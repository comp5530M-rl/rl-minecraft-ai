# Environment Editor Guide

## Installing Amulet Editor

1. Install any version of python from v3.7 - v3.10
2. Optionally setup a [Python Virtual Environment](https://docs.python.org/3/tutorial/venv.html) so you don't run into dependency conflicts
3. Install amulet-map-editor and its dependencies using one of the commands below:

    ``` cmd
    python3.8 -m pip install amulet-map-editor==0.10.8 --upgrade
    ```

4. If you are using linux you will also need to find and install wxPython from [this list](https://extras.wxpython.org/wxPython4/extras/linux/gtk3/). More information can be found [here](https://wxpython.org/pages/downloads/index.html).
5. Start Amulet Editor by using the command below, inside your current working directory:

    ``` cmd
    python3.8 -m amulet_map_editor
    ```

6. Optionally create a shortcut to open Amulet Editor for you with the following properties:

    ``` javascript
    {
        target: "C:\Windows\System32\cmd.exe /c python3.8 -m amulet_map_editor"
        start_in: "path_to_working_directory"
    }
    ```

**Common Issues:**

- If you get the following Numpy Error ```ValueError: numpy.ndarray size changed, may indicate binary incompatibility.``` then you need to update Numpy using the following command ```python3.8 -m pip install --upgrade numpy```.

### Importing Plugins

After you have started the Amulet Editor inside your working directory, you should see multiple folders created by the application, most notably the plugins folder. It should have an identical structure to the plugins folder within this directory. All you need to do is copy the plugins from this directory into the plugins folder within your working directory, and then restart Amulet Editor, or use the 'Refresh Operations' button when selecting an operation / export operation.

### Using the Editor

You can get answers to FAQ's on their [Website](https://www.amuletmc.com/faq) and a limited tutorial on their [GitHub Page](https://github.com/Amulet-Team/Amulet-Map-Editor/tree/0caccee1dfda57d373e7c037f2bfc29a2ee6e9c3/amulet_map_editor). The plugins designed here are supposed to be easy to understand and matches the design theme of the Amulet Editor, if you need more information you can reference the API below.

## Plugin API

### Entity Tool

A plugin operation for creating entity blocks for a scenario, these blocks will spawn an entity as said block when converted to a scenario using the Scenario Export Tool.

#### Entity Selection

Choose from a predetermined set of mobs and animals.

#### Persistence Required

Should this entity despawn naturally (if checked the entity will not despawn naturally).

#### Horizontal Rotation

Which direction should the entity be facing, -180° (north), -90° (east), 0° (south), 90° (west) or 180° (north).

#### Passengers

Should another entity be riding this entity, this field should be a list filled with entities in nbt format. For more information visit the [Minecraft Wiki](https://minecraft.fandom.com/wiki/Tutorials/Command_NBT_tags#Entities). For example:

``` java
[
    {id:"minecraft:sheep", ...},
    ...
]
```

#### Armor Items

What armor should this entity be wearing, accepts 4 selections within a list, in the order of Feet, Legs, Chest, Head, filled with item data in nbt format. For more information visit the [Minecraft Wiki](https://minecraft.fandom.com/wiki/Tutorials/Command_NBT_tags#Entities). For example you could have:

``` java
[
    {Count:1b,id:"minecraft:diamond_boots",tag:{...}},
    {Count:0b,id:"minecraft:diamond_leggings",tag:{...}},
    {Count:1b,id:"minecraft:diamond_chestplate",tag:{...}},
    {Count:1b,id:"minecraft:diamond_helmet",tag:{...}}
]
```

#### Armor Drop Chance

What is the chance that a piece of armor should drop when the entity is killed? This accepts a float of range 0 (not possible to drop) to 1 (will definitely drop).

#### Hand Items

What items should this entity be holding, accepts 2 selections within a list, in the order of Main-hand, Off-hand, filled with item data in nbt format. For more information visit the [Minecraft Wiki](https://minecraft.fandom.com/wiki/Tutorials/Command_NBT_tags#Entities). For example you could have:

``` java
[
    {Count:1b,id:"minecraft:diamond_sword",tag:{...}},
    {Count:64b,id:"minecraft:golden_carrot",tag:{...}}
]
```

#### Hand Drop Chance

What is the chance that an item should drop from hand when the entity is killed? This accepts a float of range 0 (not possible to drop) to 1 (will definitely drop).

#### No AI

Should this entity have an AI, checked will remove the AI from this entity.

#### No Gravity

Should this entity obey gravity, checked will remove gravity from this entity. Note that entities without gravity cannot move whilst in the air.

#### On Fire

How many in game ticks should this entity be on fire for, accepts integers from 0 (0 seconds) to 32767 (27.3 minutes).

#### Invulnerable

Should this entity be immune to damage, if checked then the entity cannot take damage.

#### Health

How many hitpoints should this entity have, accepts float values ranging from 0 to 1000.

#### Item Entity ID

For `item` entities ONLY. ID of the item entity to be spawned. `minecraft:` namespace will always be prepended.
Examples:
- `apple`
- `diamond_sword`
- `golden_carrot`

#### Item Entity Count

For `item` entities ONLY. Number of items to be spawned. Stack size 1-64

### Scenario Export Tool

This tool will export to our custom .scenario file, it is not recommended for use until the final step of creation to convert entity blocks to entities within the schematic, and add reward functions and a spawn location within the schematic.

#### Reward Functions

Select one or multiple reward functions you want this scenario to be rewarded by.

#### Hearts

Choose the amount of hearts the player has, from 0 to 20 hearts (full)

#### XP Level

Choose the XP level of the player, accepts any integer from 0 to 100

#### Hunger Level

Choose the hunger level of the player, from 0 (empty hunger bar) to 20 (full hunger bar).

#### Saturation Level

Choose the saturation level of the player, from 0 (no saturation) to hunger level (max saturation).

#### Spawn Point

Adjust the coordinates of the spawn point in the world (marked with a gold coloured selection), this position is relative to your current selection.

#### Inventory

Use the fields below the inventory to add or remove from the inventory.

##### Inventory Count

How many items do you want in an inventory slot, accepts any integer from 1 to 64.

##### Inventory Slot

Which slot do you want to put your item in, accepts values -106 (offhand slot), 0 to 8 (hotbar slots), 9 to 35 (inventory slots) and 100 to 103 (armor slots).

![An image to show which slot numbers correlate to which ingame inventory slot](https://static.wikia.nocookie.net/minecraft_gamepedia/images/b/b2/Items_slot_number.png/revision/latest?cb=20170708121246 "Inventory Slots")

##### Inventory Item

Which item will be placed in that slot?

##### Inventory Tag

Optional NBT tag to give the items enchants or modifiers, accepts any valid NBT format string. For example you could have:

``` java
{
    display: {
        Name: '[{"text":"A Very Friendly Item","italic":false,"color":"red"}]',
        Lore: "This item definitely won't hurt you"
    }
}
```

## Scenario File

The scenario file is an extension to the [.schema file format](https://github.com/SpongePowered/Schematic-Specification/blob/master/versions/schematic-2.md) created by sponge. Included in the file metadata is the reward functions, passed as a list of strings, as well as the spawn location relative to the schematic, inventory, health and hunger. The export to this file format also converts all entity blocks into a valid entity format within the schematic. An example of how the additions are formatted within the file are shown below, merged with the original .schema file format:

``` java
schematic:{
    Version: 2, // version of the .schema specification
    DataVersion: 2860, // version of minecraft used to create schema
    Width: 2s, // width of the schematic
    Height: 2s, // height of the schematic
    Length: 2s, // length of the schematic
    Offset: [1, 1, 1], // relative offset of schematic from minimal point
    PaletteMax: 4 // maximum index of the block palette
    Palette: { // what blocks are used within the schematic
        "minecraft:air": 0,
        "minecraft:stone": 1,
        ...
    }
    BlockData: [ // list of blocks with index = x + z * Width + y * Width * Length (using blocks in the palette)
        1, 1, 1, 1, 0, 0, ...
    ]
    Entities:[ // list of entities and their relative position from the minimal point
        {Id:"minecraft:creeper", Pos:[1d, 1d, 3d], ...},
        ...
    ],
    Metadata: { // additional properties to store scenario variables
        RewardFunctions: ["get_food", "get_wood", ...], // Reward functions to test for in scenario
        PlayerDat: {
            Inventory: [
                { // An item for a single slot of the inventory
                    Count: 64b,
                    Slot: 0b,
                    id: "minecraft:diamond_pickaxe",
                    tag: {...}
                },
                ...
            ],
            Pos: [4d, 1d, 3d], // spawn point relative to minimum point of schematic
            Health: 17.0f, // player health
            foodLevel: 20, // player food level
            foodSaturationLevel: 15, // player food saturation level
            XpLevel: 0 // player xp level
        }
    },
    ...
}
```
