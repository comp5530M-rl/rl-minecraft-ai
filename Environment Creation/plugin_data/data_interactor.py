import os, json, warnings

from typing import Tuple

DATA_PATH = os.getcwd()+"\\plugin_data\\"

def item_data(default_names: Tuple[str], default_codes: Tuple[str]):
    global DATA_PATH
    if os.path.isfile(DATA_PATH + "items.json"):
        with open(DATA_PATH + "items.json","r") as items_json:
            items = json.loads(items_json.read())
            default_names = items["Item"]
            default_codes = items["Resource location"]
    else:
        warnings.warn("Could not load items, expected items.json but was not found.")
    return default_names, default_codes

def entity_data(default_names: Tuple[str], default_codes: Tuple[str]):
    global DATA_PATH
    if os.path.isfile(DATA_PATH + "entities.json"):
        with open(DATA_PATH + "entities.json","r") as entities_json:
            entities = json.loads(entities_json.read())
            default_names = entities["Entity"]
            default_codes = entities["Resource location"]
    else:
        warnings.warn("Could not load entities, expected entities.json but was not found.")
    return default_names, default_codes

def reward_data(default_rewards: Tuple[str]):
    global DATA_PATH
    if os.path.isfile(DATA_PATH + "rewards.json"):
        with open(DATA_PATH + "rewards.json","r") as rewards_json:
            rewards = json.loads(rewards_json.read())
            default_rewards = rewards["Reward Functions"]
    else:
        warnings.warn("Could not load rewards, expected rewards.json but was not found.")
    return default_rewards