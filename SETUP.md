# Preparing your environment

## Installing DreamerV3 [Windows]

These are instructions for installing if you are using an NVIDIA GPU, if you want to use only CPU, skip step 1 and choose a `jaxlib` library in step 2 that starts with `cpu/jaxlib-...`

1. Install necessary Cuda and cuDNN files. For windows you must use [Cuda 11.1](https://developer.nvidia.com/cuda-11.1.0-download-archive) and [cuDNN 8.2](https://developer.nvidia.com/rdp/cudnn-archive). You can follow this guide for instructions on installing cuDNN: [NVIDIA Installation Guide Section 2.3](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html#installwindows).
2. Install [JAX](https://jax.readthedocs.io/en/latest/developer.html) using a [Windows Builder](https://github.com/cloudhan/jax-windows-builder). Choose the `jaxlib` package from the list here: [whls.blob.core.windows.net](https://whls.blob.core.windows.net/unstable/index.html), you will be looking for files relating to your specific version of python. You will be looking for a file in the form `cuda111/jaxlib-0.3.17+cuda11.cudnn82-cp38-cp38-win_amd64.whl` where `jaxlib-0.3.17` is the version of jaxlib you want and `cp38` is the version of python you have. You will also need to choose a compatible version of JAX for your selected jaxlib, a safe choice is usually the same or a few higher than jaxlib. We then install using the following command with your specific choices:

    ``` cmd
    pip3.8 install jax==0.3.20 https://whls.blob.core.windows.net/unstable/cuda111/jaxlib-0.3.17+cuda11.cudnn82-cp38-cp38-win_amd64.whl
    ```

3. Now we need to install all the python dependencies for `dreamerv3` and our project. You can do this using the `pip3.8 install -r requirements.txt` command.

**Hotfixes:**

Issue: `[Errno 2] No such file or directory: 'configs.yaml'`  
Solution: Go to file `C:\Users\___\AppData\Local\Programs\Python\Python38\Lib\site-packages\dreamerv3\agent.py`, replace `line 1` with `from pathlib import Path` and replace `line 25` with the lines:  

``` python
with open(Path(__file__).parent.joinpath('configs.yaml'), "r") as f:
    configs = yaml.YAML(typ='safe').load(f.read())
```  

Issue: `module 'tensorflow' has no attribute 'summary'`  
Solution: This may be caused by an inconsistency with the `torch` mdoule, try manually installing with `pip3.8 install torch==1.10.0`.  

Issue: `module 'tensorflow' has no attribute 'io'`  
Solution: Try manually installing tensorflow and tensorflow-io using:  

``` cmd
pip3.8 install tensorflow==2.11.0
pip3.8 install tensorflow-io==0.31.0
```

Issue: `Could not load dynamic library 'cudnn64_8.dll'; dlerror: cudnn64_8.dll not found`  
Solution: This issue solved itself after some time, possibly check you installed cuDNN correctly or try restarting your system.  

## Installing Client/Server Environment

You must have [Java Development Kit](https://adoptium.net/releases.html) for Java 17.

### Starting the server

1. To use the minecraft server you must first accept the eula provided, and build your server environment. These steps can be completed by running the `build_environment.py` file in the `project-mindcraft` folder
2. To connect to your server from a local machine you use the ip address `127.0.0.1` and check the `server-port` value in `server.properties`, this gives you a connection address of `127.0.0.1:25565` by default. Alternatively start the server controller and this will handle IP addresses and ports automatically for you. To connect to the server from another machine you may need to adjust port-forwarding properties on your router (***i am not an expert at this part***).
3. We have implemented a Server Verification mode if you wish to use it, this works in part with the client side where the client will automatically send a verification message when joining a server. If no verification message is received within a specified time-frame, then the recently connected client is kicked from the server. This is toggled with the `server_verification_time` in `system.py` initialisation, and is **disabled** by default.

## Setting up your environment [VSCODE]

To help with testing you can set up you can initialise these files, and place them within the `.vscode` folder in the root of the directory:

### launch.json

``` json
{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Python: Current File",
      "type": "python",
      "request": "launch",
      "program": "${file}",
      "console": "integratedTerminal"
    },
    {
      "name": "Reset Environment",
      "type": "python",
      "request": "launch",
      "program": "${workspaceFolder}\\project-mindcraft\\build_environment.py",
      "console": "integratedTerminal"
    },
    {
      "name": "Start Training",
      "type": "python",
      "request": "launch",
      "program": "${workspaceFolder}\\project-mindcraft\\start_training.py",
      "console": "integratedTerminal"
    },
    {
      "name": "Start Server Controller",
      "type": "python",
      "request": "launch",
      "program": "${workspaceFolder}\\project-mindcraft\\start_server_controller.py",
      "console": "integratedTerminal"
    },
    {
      "name": "Start Client Controller",
      "type": "python",
      "request": "launch",
      "program": "${workspaceFolder}\\project-mindcraft\\start_client_controller.py",
      "console": "integratedTerminal"
    },
    {
      "type": "java",
      "name": "Minecraft Client",
      "request": "launch",
      "cwd": "${workspaceFolder}\\project-mindcraft\\Simulator\\run",
      "console": "internalConsole",
      "stopOnEntry": false,
      "mainClass": "net.fabricmc.devlaunchinjector.Main",
      "vmArgs": "-Dfabric.dli.config\u003dC:\\Users\\...\\Project@@0020MindCraft\\project-mindcraft\\Simulator\\.gradle\\loom-cache\\launch.cfg -Dfabric.dli.env\u003dclient -Dfabric.dli.main\u003dnet.fabricmc.loader.impl.launch.knot.KnotClient",
      "args": ""
    },
    {
      "type": "java",
      "name": "Minecraft Server",
      "request": "launch",
      "cwd": "${workspaceFolder}\\project-mindcraft\\Simulator\\run",
      "console": "internalConsole",
      "stopOnEntry": false,
      "mainClass": "net.fabricmc.devlaunchinjector.Main",
      "vmArgs": "-Dfabric.dli.config\u003dC:\\Users\\...\\Project@@0020MindCraft\\project-mindcraft\\Simulator\\.gradle\\loom-cache\\launch.cfg -Dfabric.dli.env\u003dserver -Dfabric.dli.main\u003dnet.fabricmc.loader.impl.launch.knot.KnotServer",
      "args": "nogui"
    }
  ]
}
```

### settings.json

``` json
{
    "python.analysis.extraPaths":[
        "./project-mindcraft",
    ]
}
```
