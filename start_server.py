from mindcraft.Controller import ComputeManager
from mindcraft.Util import GLOBAL_CONFIG, ServerConfig, ControllerConfig, ClientGraphicsConfig
import time

if __name__ == "__main__":


    GLOBAL_CONFIG["logging_level"] = "WARN"

    serverConfig = ServerConfig({
        "server_world":"survival9066_16x16",
        "break_modifier":0,
    })

    clientConfig = ControllerConfig({
        "graphics_config" : ClientGraphicsConfig({
            "hide_hud": False,
        })
    })
    num_clients = 2

    compute = ComputeManager(serverConfig, clientConfig)
    compute.wait_for_server()


    for _ in range(num_clients):
        compute.start_client()

    # Test reset function
    if False:
        for c in compute.clients:
            compute.wait_for_client(c)
        time.sleep(10)
        compute.reset_server()
