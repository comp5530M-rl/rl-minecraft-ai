from mindcraft.Util import GLOBAL_CONFIG, ServerConfig, ControllerConfig, ClientGraphicsConfig
from mindcraft.Controller import ComputeManager
from mindcraft.RL.tests.test import *

if __name__ == "__main__":
    GLOBAL_CONFIG["logging_level"] = "WARN"

    trainConfig = ServerConfig({
        "server_world":"survival9066_16x16",
        "break_modifier":0,
    })

    clientConfig = ControllerConfig({
        "graphics_config" : ClientGraphicsConfig({
            "hide_hud": False,
        })
    })

    # NOTE Uncomment below to test
    computeManager = ComputeManager(trainConfig)
    computeManager.wait_for_server()

    env = gym.make('Minecraft-v0')
    env.connect_to_server(computeManager)

    test_observations_decode(env)
    test_action_decode(env)
    test_action_decode_50(env)
    test_items(env)
    test_rewards(env)
    test_singleagent_ppo(env)

    print("Testing Complete!")