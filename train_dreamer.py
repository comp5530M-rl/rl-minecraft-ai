from threading import Thread
from functools import partial

from mindcraft.Util import GLOBAL_CONFIG, ServerConfig, ControllerConfig, ClientGraphicsConfig
from mindcraft.Util import LOGGER
from mindcraft.Controller import ComputeManager

import dreamerv3
from dreamerv3.embodied.envs import from_gym
from dreamerv3 import embodied
from dreamerv3.embodied import wrappers
import gym


def make_batched_env(envs, manager:ComputeManager, config):

    envs = map(lambda e: from_gym.FromGym(e), envs)
    envs = list(map(lambda e: dreamerv3.wrap_env(e, config), envs))
    return embodied.BatchEnv(envs, parallel=False)

def make_batched_parallel_env(envs, manager:ComputeManager, config):

    envs = map(lambda e: from_gym.FromGym(e), envs)
    envs = map(lambda e: dreamerv3.wrap_env(e, config), envs)

    if config.envs.parallel != 'none':
        envs = map(lambda e: partial(embodied.Parallel, lambda: e, config.envs.parallel), envs)
    else:
        raise("Invalid parallel type for config.envs.parallel")
    if config.envs.restart:
        envs = map(lambda e: partial(wrappers.RestartOnException, e), envs)
    
    envs = [e() for e in envs]

    return embodied.BatchEnv(envs, parallel=config.envs.parallel != 'none')

def prepare_for_dreamer(envs, manager:ComputeManager, config:embodied.Config, parallel:bool):
    env = make_batched_parallel_env(envs, manager, config) if parallel else make_batched_env(envs, manager, config)

    step = embodied.Counter()
    logger = embodied.Logger(step, [
        embodied.logger.TerminalOutput(),
        embodied.logger.JSONLOutput(config.logdir+'/metrics', 'metrics.jsonl'),
        embodied.logger.TensorBoardOutput(config.logdir+'tensorboard'),
    ])

    agent = dreamerv3.Agent(env.obs_space, env.act_space, step, config)

    replay = embodied.replay.Uniform(
        config.batch_length, config.replay_size, config.logdir+'/replay'
    )

    args = embodied.Config(
        **config.run,
        logdir=config.logdir,
        batch_steps=config.batch_size * config.batch_length,
    )

    return agent, env, replay, logger, args

def train_dreamer(manager: ComputeManager, num_instances=2, num_steps:int=9e9, parallel:bool=False, startup_parallel:bool=False):
    import warnings
    import dreamerv3
    from dreamerv3.embodied.envs import from_gym
    from dreamerv3 import embodied
    warnings.filterwarnings('ignore', '.*tuncated to dtype int32.*')
    
    # Create the environments
    envs = [gym.make('Minecraft-v0') for _ in range(num_instances)]
    if startup_parallel:
        threads = [Thread(target=e.connect_to_server, args=(manager,)) for e in envs]
        for t in threads:
            t.start()
        for t in threads:
            t.join()
    else:
        for e in envs:
            e.connect_to_server(manager)
    LOGGER.info("All Clients Started!")


    # Dreamer Config
    mlp_keys = [*envs[0].observation_space.spaces.keys()]
    cnn_keys = []
    if 'image' in mlp_keys:
        mlp_keys.remove('image')
        cnn_keys.append('image')
    config = embodied.Config(dreamerv3.configs['defaults'])
    config = config.update(dreamerv3.configs['medium'])
    config = config.update({
        'replay_size':1e3,
        'logdir': 'out/dreamerv3/run1',
        'envs.amount' : num_instances,
        'run.train_ratio': 16,
        'run.log_every': 30,  # Seconds
        'batch_size': 16,
        'run.save_every': 600,
        'run.steps': num_steps,
        'jax.prealloc': False,
        'encoder.mlp_keys': '|'.join(mlp_keys),
        'decoder.mlp_keys': '|'.join(mlp_keys),
        'encoder.cnn_keys': '|'.join(cnn_keys),
        'decoder.cnn_keys': '|'.join(cnn_keys),
        #'jax.platform': 'cpu',
    })
    config = embodied.Flags(config).parse()


    # Build Agent, wrap env and build utils
    agent, env, replay, logger, args = prepare_for_dreamer(envs, manager, config, parallel)

    # Train
    embodied.run.train(agent, env, replay, logger, args)


if __name__ == "__main__":

    GLOBAL_CONFIG["logging_level"] = "WARN"

    serverConfig = ServerConfig({
        "server_world":"survival9066_16x16",
        "break_modifier":0,
    })

    clientConfig = ControllerConfig({
        "graphics_config" : ClientGraphicsConfig({
            "hide_hud": False,
        })
    })

    computeManager = ComputeManager(serverConfig, clientConfig)
    computeManager.wait_for_server()

    train_dreamer(computeManager, num_instances=2, parallel=True)