from mindcraft.RL.Util.reward_functions import *

# calculates the total reward, given  a observation and reward function 
# specification (with args) for each agent parameter (health, hunger, saturation)
#
# Example use:
    # stat_reward_spec = StatRewardFuncSpec(linear_reward,
    #                                     ['x',0.5,1],
    #                                     linear_reward,
    #                                     ['x',0.25,1],
    #                                     exponential_reward,
    #                                     ['x',0.25,1]
    #                                     )
    # get_rewards(player_status, stat_reward_spec)
#
# use only 'x' as first parameter if not using change of stats for reward calculations
def get_rewards(player_status, stat_reward_spec: StatRewardFuncSpec):
    
    rewards = 0

    if stat_reward_spec.health_func is not None:
        assert (stat_reward_spec.health_func_params is not None), "health_func_params is None"
        rewards += stat_reward_spec.health_func(player_status['health'], *stat_reward_spec.health_func_params[1:])
    if stat_reward_spec.hunger_func is not None:    
        assert (stat_reward_spec.hunger_func_params is not None), "hunger_func_params is None"
        rewards += stat_reward_spec.hunger_func(player_status['hunger'], *stat_reward_spec.hunger_func_params[1:])
    if stat_reward_spec.saturation_func is not None:
        assert (stat_reward_spec.saturation_func_params is not None), "saturation_func_params is None"
        rewards += stat_reward_spec.saturation_func(player_status['saturation'], *stat_reward_spec.saturation_func_params[1:])

    rewards += death_penalty_reward(player_status['health'], 100)
    return rewards