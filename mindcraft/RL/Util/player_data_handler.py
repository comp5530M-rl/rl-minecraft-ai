class PlayerStats():
    health = int
    hunger = int
    saturation = int
    def __init__(self, _health, _hunger, _saturation):
        self.health = _health
        self.hunger = _hunger
        self.saturation = _saturation
    
    def __str__(self):
        return f'health: {self.health}, hunger: {self.hunger}, saturation: {self.saturation}'

    def __repr__(self):
        return f'PlayerStats({self.health}, {self.hunger}, {self.saturation})'


def get_player_stats_from_obs(obs) -> PlayerStats:
    data = obs['playerData']
    return PlayerStats(data['health'], data['hunger'], data['saturation'])


# Change of stats from previous observation to current.
# Values will be positive if the agent's stats has improved,
# negative otherwise. Improvement is defined as follows: 
# (more healthy, less hungry, more saturated) from prev stats.
def get_stats_change(curr_stats: PlayerStats, prev_stats: PlayerStats) -> PlayerStats:
    health_diff = curr_stats.health - prev_stats.health
    # NOTE: value of hunger is lower in Minecraft when more hungry
    hunger_diff = curr_stats.hunger - prev_stats.hunger
    satur_diff = curr_stats.saturation - prev_stats.saturation
    return PlayerStats(health_diff, hunger_diff, satur_diff)

