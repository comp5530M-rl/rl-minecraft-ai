import numpy as np, cv2
from gym.spaces import Dict

from mindcraft.RL.Util.reward_functions import StatRewardFuncSpec
from mindcraft.RL.Util.reward_definition import get_rewards


# List of all possible observations, env uses a subset
observation_keys = [
    'image',
    'health', 
    'hunger', 
    'saturation', 
    'breath', 
    'isOnGround', 
    'equipped', 
    'hotbar', 
    'inventory',
]

# Decodes the raw observation into a dictionary of numpy arrays for the agent to use
def decode_observation(
        raw_obs, 
        obs_keys: list, 
        item2encoding, 
        num_items_encodings, 
        reward_spec: StatRewardFuncSpec, 
        normalise_img=False,
        normalise_vec=False,
        no_drop=0,
        resolution=(64,64),
        image_only=False,
    ): 

        obs = {}
        player_status = {}
        player_status['health'] = float(raw_obs['playerData']['health'])
        player_status['hunger'] = float(raw_obs['playerData']['hunger'])
        player_status['saturation'] = float(raw_obs['playerData']['saturation'])
        decoded_hotbar = False

        for key in obs_keys:
            assert key in observation_keys, f'Key Error: "{key}" not in observation_keys'

            if key == 'image':
                # Convert to numpy array
                val = np.array(raw_obs['screenBytes'], dtype=np.uint8)
                val = val.reshape((raw_obs['screenHeight'], raw_obs['screenWidth'], -1))
                # from ARGB to RGB
                val = cv2.cvtColor(val, cv2.COLOR_BGRA2RGB)
                # Center crop
                if raw_obs['screenHeight'] > raw_obs['screenWidth']:
                    val = val[int((raw_obs['screenHeight'] - raw_obs['screenWidth'])/2):-int((raw_obs['screenHeight'] - raw_obs['screenWidth'])/2), :, :]
                elif raw_obs['screenHeight'] < raw_obs['screenWidth']:
                    val = val[:, int((raw_obs['screenWidth'] - raw_obs['screenHeight'])/2):-int((raw_obs['screenWidth'] - raw_obs['screenHeight'])/2), :]
                #resize to resolution
                val = cv2.resize(val, resolution, interpolation=cv2.INTER_AREA)
                val = cv2.flip(val, 0)
                if normalise_img:
                    val = val.astype(np.float32) / 255.0
                if image_only:
                    obs = val
                    break


            elif key in ['health', 'hunger', 'saturation']:
                val = np.array([raw_obs['playerData'][key]], dtype=np.float32)
                if normalise_vec:
                    val = val/20.0
            
            elif key == 'breath':
                val = np.array([raw_obs['playerData'][key]], dtype=np.float32)
                if normalise_vec:
                    val = (val+20.0)/320.0

            elif key == 'isOnGround':
                val = np.array([raw_obs['playerData'][key]], dtype=np.float32)

            elif key == 'equipped':
                val = np.array([raw_obs['inventory'][i]['isActive'] for i in range(9)], dtype=np.float32)

            elif key == 'hotbar':
                val = np.zeros(((9-no_drop)*num_items_encodings), dtype=np.float32)
                for slot in range(no_drop, 9):

                    item_id = item2encoding.get(raw_obs['inventory'][slot]['itemName'])
                    if not item_id:
                        item_id = item2encoding['other']

                    quantity = raw_obs['inventory'][slot]['stackSize']
                    if normalise_vec:
                        quantity = quantity/64.0
                    val[(slot-no_drop)*num_items_encodings  + item_id] = quantity
                decoded_hotbar = True

            elif key == 'inventory':
                # To prevent adding hotbar info twice
                i = no_drop
                if decoded_hotbar:
                    i = 9
                val = np.zeros(((40-i)*num_items_encodings), dtype=np.float32)
                for slot in range(i, 40):

                    item_id = item2encoding.get(raw_obs['inventory'][slot]['itemName'])
                    if not item_id:
                        item_id = item2encoding['other']

                    quantity = raw_obs['inventory'][slot]['stackSize']
                    if normalise_vec:
                        quantity = quantity/64.0
                    val[(slot-i)*num_items_encodings + item_id] = quantity
            
            obs[key] = val

        reward = get_rewards(player_status, reward_spec)
        
        done = player_status['health'] == 0.0

        return obs, reward, done, {}