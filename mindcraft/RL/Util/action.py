import gym
from typing import List

from mindcraft.Controller.action import ActionManager as Action, ActionType
from mindcraft.RL.Util.player_data_handler import *

# removes warnings for casts when defining custom gym spaces
gym.logger.set_level(40)


# Decodes the output from the agent's policy for the game client
def decode_action(actions_issued: gym.spaces.Box, actions, safe_slots=0, diametric_actions=0) -> List[ActionType]:
    num_camera_actions = 2
    num_slots = 9
    safe_slots = safe_slots # first n slots which can't be dropped

    num_drop_actions = num_slots - safe_slots
    num_equip_actions = num_slots

    offset = 0
    diametric_action_idx = 0
    action_list = []

    skip = False
    for idx in range(len(actions)):
        
        # For diametric actions, we only want to take one of the two actions
        if skip:
            skip = False
            continue

        action = actions[idx]
        if action == Action.CAMERA:
            # max turn angle is 30 degrees in each direction
            action_list = [action(actions_issued[0]*30.0, actions_issued[1]*30.0)]
            offset += num_camera_actions

        elif action == Action.DROP:
            # Only drop max 1 item, if action > 0.0 (Can't drop slots 0-4)
            arg_max = actions_issued[offset:offset+num_drop_actions].argmax()
            if actions_issued[offset+arg_max] > 0.75:
                action_list.append(action(arg_max+safe_slots))
            offset += num_drop_actions 

        elif action == Action.HOTBAR:
            # Only equip max 1 item, if action > 0.0
            arg_max = actions_issued[offset:offset+num_equip_actions].argmax()
            if actions_issued[offset+arg_max] > 0.75:
                action_list.append(action(arg_max))
            offset += num_equip_actions
        
        elif diametric_action_idx < diametric_actions:
            action_opp = actions[idx+1]
            arg_max = actions_issued[offset:offset+2].argmax()
            if arg_max == 1:
                action = action_opp
            if actions_issued[offset+arg_max] > 0.0:
                action_list.append(action)
            offset += 2
            diametric_action_idx += 1
            skip = True

        elif isinstance(action, ActionType):
            if actions_issued[offset] > 0.0:
                action_list.append(action)
            offset += 1
        
        else:
            raise Exception("Invalid action type")
    
    return action_list