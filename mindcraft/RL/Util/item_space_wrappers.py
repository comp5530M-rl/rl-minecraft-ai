from typing import List, Union

import gym
from gym.spaces import MultiDiscrete


#  "itemDamage": Discrete(2) it does damage or not (exclude amount)
#  "stackSize": Discrete(3) 0, 1, 2 where 1 if stack < 5; 2 if stack >= 5
#  "isActive": Discrete(2), # True or False
#  "itemName": Text(min_length = 1, max_length = 40) TODO: CHANGE TO DISCRETE ITEM TYPES
def item_space() -> gym.spaces.MultiDiscrete:
    return MultiDiscrete([2, 3, 2, 40])
    

# create a list of items, each item in the above MultiDiscrete format
def encode_items(inventory) -> List[Union[int, bool, str]]:
    encoded_items = []
    for i in range(9): # go through the hotbar only (first 9 slots)
        item = inventory[i] # hotbar slot

        dmg = 1 if item['itemDamage'] > 0 else 0 # deals damage or not

        # stack is 0, few or many
        stack = item['stackSize'] 
        if stack > 0 and stack < 5: 
            stack = 1
        elif stack >= 5: stack = 2
        
        active = 1 if item['isActive'] else 0

        name = item['itemName'] # TODO: CHANGE TO DISCRETE SUBSET OF ITEM TYPES
        pad = 40 - len(name)
        name = name + pad*' '
        encoded_items.append([dmg, stack, active, name])

    return encoded_items