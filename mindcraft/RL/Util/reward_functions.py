import math, numpy as np
from typing import List, Union, Callable

from mindcraft.RL.Util.player_data_handler import *


# Class to encapsulate reward function definitions and args 
# for each type of player stats
class StatRewardFuncSpec():
    health_func: Callable
    health_func_params: List[Union[float, int, bool, str]]
    hunger_func: Callable
    hunger_func_params: List[Union[float, int, bool, str]]
    saturation_func: Callable
    hunger_func_params: List[Union[float, int, bool, str]]

    def __init__(self, 
                _health_func = None, 
                _health_func_params = None,
                _hunger_func = None, 
                _hunger_func_params = None,
                _saturation_func = None,
                _saturation_func_params = None
                ):
        self.health_func = _health_func
        # NOTE: the first func parameter should be 'x' if current value
        # is to be evaluated or 'change' if the difference
        # between current and previous value should be taken as x.
        # get_rewards() in reward_functions package will handle
        # passing of the correct value to the corresponding functions.
        # Thus fixed parameters are located in self.[stat]_func_params[1:]
        # NOTE: this applies to all stat types
        self.health_func_params = _health_func_params
        self.hunger_func = _hunger_func
        self.hunger_func_params = _hunger_func_params
        self.saturation_func = _saturation_func
        self.saturation_func_params = _saturation_func_params



# Negative reward for dying
def death_penalty_reward(health:float, reward_amount: float) -> float:
    if health <= 0.0:
        return -reward_amount
    else: return 0


# Reward based on difference (change) from previous time point.
def stat_difference_reward(change: float, reward_amount: float) -> float:
    return change * reward_amount


# REWARD FUNCTION DOCUMENTATION AT:
# https://gitlab.com/comp5530M-rl/rl-minecraft-ai/-/wikis/Reward-Functions

# @params
# x=input
# a=minimum point
# b=maximum point

# NOTE Restrict: a != b 
def linear_reward(x: float, a: float, b: float) -> float: 
    assert a != b, "LINEAR_REWARD: a and b cannot be equal"
    return (x - a) / (b-a)


# NOTE: Restrict: 0 <= a < b
def polynomial_reward(x: float, a: float, b: float) -> float:
    assert a >= 0, "POLYNOMIAL_REWARD: a must be greater than or equal to 0"
    assert a < b, "POLYNOMIAL_REWARD: a must be less than b"
    return (pow(x,2) - pow(a,2)) / (pow(b,2) - pow(a,2))


# NOTE: Restrict: 0 < a < b NOTE: (division of 0 is very probable)
def logarithmic_reward(x:float, a:float, b:float, c:float=1.0) -> float: 
    assert a > 0, "LOGARITHMIC_REWARD: a must be greater than 0"
    assert a < b, "LOGARITHMIC_REWARD: a must be less than b"
    return np.log((x+c) / a) / np.log(b / a)


# NOTE: Restrict a < b
def exponential_reward(x: float, a: float, b: float) -> float:
    assert a < b, "EXPONENTIAL_REWARD: a must be less than b"
    return (pow(math.e,x) - pow(math.e,a)) / (pow(math.e,b) - pow(math.e,a))