from mindcraft.RL.Util.player_data_handler import *
from mindcraft.RL.Util.item_space_wrappers import *
from mindcraft.RL.Util.reward_functions import *
from mindcraft.RL.Util.reward_definition import *
from mindcraft.RL.Util.action import *
from mindcraft.RL.Util.observation import *



def test_observations_decode(env):
    import csv
    
    item2encoding = {}

    with open('RL/Util/minimal.csv', 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in enumerate(reader):
            item2encoding[row[1][0]] = int(row[1][1])
            num_item_encodings = int(row[1][1])+1

    obs = env.reset()

    for key in env.observation_space.spaces.keys():
        assert key in obs.keys(), 'Key {} not in observation'.format(key)

    print("Mission complete!")


def test_action_decode(env):
    action = env.action_space.sample()
    print(action)
    action = decode_action(action, env.actions, env.no_drop, env.num_diametric_actions)
    env.controller.send_actions(*action)
    print("Mission complete!")

def test_action_decode_50(env):
    for _ in range(50):
        action = env.action_space.sample()
        action = decode_action(action, env.actions, env.no_drop, env.num_diametric_actions)
        env.controller.send_actions(*action)
    print("Mission complete!")


def test_items(env):
    obs = env.controller.request_observation()
    print(encode_items(obs['inventory']))
    print("Mission complete!")


def test_rewards(env):
    player_status = {'health': 15, 'hunger': 10, 'saturation': 10}
    stat_reward_spec = StatRewardFuncSpec(linear_reward,
                                        ['x',0.5,1],
                                        linear_reward,
                                        ['x',0.25,1],
                                        exponential_reward,
                                        ['x',0.25,1]
                                        )
    print(get_rewards(player_status, stat_reward_spec))
    print("Mission complete!")

def test_singleagent_ppo(env):
    
    from stable_baselines3 import PPO
    from stable_baselines3.common.env_util import make_vec_env
    from stable_baselines3.common.env_checker import check_env

    check_env(env)
    
    model = PPO("MultiInputPolicy", env, verbose=1)
    model.learn(total_timesteps=5)

    model_dir = "out/ppo_test"
    model.save(model_dir)
    del model
    model = PPO.load(model_dir)

    obs = env.reset()
    for _ in range(64):
        action, _states = model.predict(obs)
        obs, rewards, dones, info = env.step(action)
        env.render()
    
    print("Mission complete!")

