from gym.envs.registration import register

register(
    id='Minecraft-v0',
    entry_point='mindcraft.RL.envs:MCEnv',
    max_episode_steps=10000,
)