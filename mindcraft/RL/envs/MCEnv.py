import pprint, csv, gym, numpy as np, time
from gym.spaces import MultiBinary, Box, Discrete, MultiDiscrete, Dict
from typing import List
from pathlib import Path

from mindcraft.Controller.action import ActionManager as Action, ActionType
from mindcraft.RL.Util.reward_functions import StatRewardFuncSpec, stat_difference_reward, linear_reward, exponential_reward, logarithmic_reward
from mindcraft.RL.Util.action import decode_action
from mindcraft.RL.Util.observation import decode_observation

import time
from collections import deque

# removes warnings for casts when defining custom gym spaces
gym.logger.set_level(40)

class MCEnv(gym.Env):
    """Custom Environment that follows gym interface."""

    # metadata = {"render.modes": ["human"]} # IDK what this is for

    # We never use this but it's required by gym
    # Initialise env using gym.make('Minecraft-v0')
    def __init__(self):
        super().__init__()

        # TODO: change to dict where key is name of action and value is length, (e.g. camera = 2, hotbar = 9, forward = 1)

        self.resolution = (64,64) # The Downsampled resolution of the image
        self.sleep_time = None    # Time to sleep between each step    
        self.reset_sleep_time = 2
        self.scenario = 'equipped_unhealthy_food_full-inv'  # The scenario to use
        self.no_drop = 5    # Number of first n slots that can't be dropped
        self.num_diametric_actions = 3
        self.normalise_img = True
        self.normalise_vec = True

        # lengths in comments
        self.actions = [
            Action.CAMERA,          # 2          [func]
            Action.DROP,            # 9-no_drop  [func]
            Action.HOTBAR,          # 9          [func]
            Action.LEFT_CLICK,      # 1
            Action.RIGHT_CLICK,     # 1
            Action.MOVE_LEFT,       # 1
            Action.MOVE_RIGHT,      # 1
            Action.MOVE_FORWARD,    # 1
            Action.MOVE_BACKWARD,   # 1
            Action.JUMP,            # 1
            Action.SNEAK,           # 1
            Action.SPRINT,          # 1
        ]
        self.actions_len = 2 + (9-self.no_drop) + 9 + 9
        
        # TODO: specify scenario as class variable and use in connect_to_server & reset()

        # a coding of the items & blocks in the game. check minimal.csv for details
        self.item2encoding = {}
        with open(Path(__file__).parent.parent.joinpath("Util/minimal.csv"), 'r') as csvfile:
            reader = csv.reader(csvfile)
            for row in enumerate(reader):
                self.item2encoding[row[1][0]] = int(row[1][1])
                self.num_item_encodings = int(row[1][1])+1

        # Define the reward function, StatRewardFuncSpec(health_func, health_args, hunger_func, hunger_args, saturation_func, saturation_args)
        self.stat_reward_spec = StatRewardFuncSpec(
            exponential_reward,
            ['x',0.0,20.0],
            logarithmic_reward,
            ['x',3.0,20.0],
            # linear_reward,
            # ['x',0.0,20.0]
        )
        self.controller = None
        self.prev_observation = None # for caching incase of obs err

        # For benchmarking
        self.benchmark = False
        self.last_stepped = None
        self.step_times = deque(maxlen=100)

    def benchmark(self):
        self.benchmark = True

    # Used by the env to create and secure a client
    # MUST be called after gym.make(), cannot be called in __init__() 
    def connect_to_server(self, manager):
        manager.start_client()
        controller = manager.clients[-1]
        manager.wait_for_client(controller)
        self.controller = controller 
        self.controller.send_actions(*[Action.SCENARIO(self.scenario)]) # Init Scenario

    def step(self, action):

        actions = self._get_act(action)
        self.controller.send_actions(*actions)
        
        # For benchmarking
        if self.benchmark:
            if self.last_stepped is not None:
                self.step_times.append(time.time() - self.last_stepped)
            self.last_stepped = time.time()
            if np.random.rand() < 0.1:
                print(f"Average step time: {np.mean(self.step_times)}")
        
        observation, reward, done, info = self._observe()
        return observation, reward, done, info
    
    def get_raw_obs(self):
        return self.controller.request_observation(*self.observation_space.spaces.keys())
        
    def _observe(self):
        if self.sleep_time is not None:
            time.sleep(self.sleep_time)
        raw_obs = self.get_raw_obs()

        if raw_obs is None:
            time.sleep(0.1)
            observation, reward, done, info = self._observe()
        else:
            observation, reward, done, info = self._get_obs(raw_obs)
            # self.prev_observation = observation
            # print(f'size of observation: {sys.getsizeof(observation)}')
            # print(f'size of prev_observation: {sys.getsizeof(self.prev_observation)}')
            # print(f'size of reward: {sys.getsizeof(reward)}')
        return observation, reward, done, info


    def set_scenario(self, scenario, reset=False):
        self.scenario = scenario
        if reset:
            self.reset()

    def reset(self):
        
        # self.manager.send_actions(*[Action.RESET(self.task.name)))
        self.controller.send_actions(*[Action.SCENARIO(self.scenario)]) # Init Scenario
        observation, _, _, _ = self._observe()
        return observation


    # We render by default and can't disable it (otherwise no pixels)
    def render(self, mode="human"):
        pass


    def close(self):
        self.controller.send_actions(*[Action.TERMINATE])
        ...


    # Specifies the date each agent will receive
    @property
    def observation_space(self) -> gym.spaces.Dict:
        # wrap camera actions and all other actions together to form the action space
        if self.normalise_vec:
            return gym.spaces.Dict({
                'image':    Box(low=0,   high=1.0, shape=(self.resolution[0], self.resolution[1], 3), dtype=np.float32) if self.normalise_img 
                    else    Box(low=0,   high=255, shape=(self.resolution[0], self.resolution[1], 3), dtype=np.uint8),
                'health':   Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32),
                'hunger':   Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32),
                'saturation':   Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32),
                'breath':   Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32),
                'isOnGround': Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32),
                'equipped': Box(low=0.0, high=1.0, shape=(9,), dtype=np.float32), # Should be One-hot
                'hotbar':   Box(low=0.0, high=1.0, shape=((9-self.no_drop)*self.num_item_encodings,), dtype=np.float32),
            })
        else:
            return gym.spaces.Dict({
                'image':    Box(low=0,   high=1.0, shape=(self.resolution[0], self.resolution[1], 3), dtype=np.float32) if self.normalise_img 
                    else    Box(low=0,   high=255, shape=(self.resolution[0], self.resolution[1], 3), dtype=np.uint8),
                'health':   Box(low=0.0, high=20.0, shape=(1,), dtype=np.float32),
                'hunger':   Box(low=0.0, high=20.0, shape=(1,), dtype=np.float32),
                'saturation':   Box(low=0.0, high=20.0, shape=(1,), dtype=np.float32),
                'breath':   Box(low=-20.0, high=300.0, shape=(1,), dtype=np.float32),
                'isOnGround': Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32),
                'equipped': Box(low=0.0, high=1.0, shape=(9,), dtype=np.float32), # Should be One-hot
                'hotbar':   Box(low=0.0, high=64.0, shape=((9-self.no_drop)*self.num_item_encodings,), dtype=np.float32),
            })



    # Used to decode the raw observation from the server into dict for the agent
    def _get_obs(self, raw_obs) -> Dict:
        return decode_observation(raw_obs, 
                                  self.observation_space, 
                                  self.item2encoding, 
                                  self.num_item_encodings, 
                                  self.stat_reward_spec, 
                                  self.normalise_img,
                                  self.normalise_vec,
                                  no_drop=self.no_drop, 
                                  resolution=self.resolution
                                  )


    # Specifies which actions the agent can take
    @property
    def action_space(self) -> gym.spaces.Box:
        return gym.spaces.Box(low=-1.0, high=1.0, shape=(self.actions_len,), dtype=np.float32)
    

    # used to decode the output of the policy network into a list of actions to send to the server
    def _get_act(self, actions_issued: gym.spaces.Box) -> List[ActionType]:
        return decode_action(actions_issued, self.actions, self.no_drop, self.num_diametric_actions)
