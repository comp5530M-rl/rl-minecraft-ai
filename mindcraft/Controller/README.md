# Minecraft Client Setup Guide

## Setting Up Your Environment

Follow the steps outlined on the [fabric website](https://fabricmc.net/wiki/tutorial:setup) to build and import the project to your specific IDE. You must have [Java Development Kit](https://adoptium.net/releases.html) for Java 17.

This should setup your launch and debug settings for your IDE, where you can run the implemented *Minecraft Client* or *Minecraft Server*, or you can run our python scripts for starting the various controllers. You can customise your respective ```launch.json``` file  for your IDE to be able to run from the root folder, for *vscode* this might look like:

``` javascript
{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Python: Current File",
      "type": "python",
      "request": "launch",
      "program": "${file}",
      "console": "integratedTerminal"
    },
    {
      "name": "Reset Environment",
      "type": "python",
      "request": "launch",
      "program": "${workspaceFolder}\\project-mindcraft\\build_environment.py",
      "console": "integratedTerminal"
    },
    {
      "name": "Start Training",
      "type": "python",
      "request": "launch",
      "program": "${workspaceFolder}\\project-mindcraft\\start_training.py",
      "console": "integratedTerminal"
    },
    {
      "name": "Start Server Controller",
      "type": "python",
      "request": "launch",
      "program": "${workspaceFolder}\\project-mindcraft\\start_server_controller.py",
      "console": "integratedTerminal"
    },
    {
      "name": "Start Client Controller",
      "type": "python",
      "request": "launch",
      "program": "${workspaceFolder}\\project-mindcraft\\start_client_controller.py",
      "console": "integratedTerminal"
    },
    {
      "type": "java",
      "name": "Minecraft Client",
      "request": "launch",
      "cwd": "${workspaceFolder}\\project-mindcraft\\Simulator\\run",
      "console": "internalConsole",
      "stopOnEntry": false,
      "mainClass": "net.fabricmc.devlaunchinjector.Main",
      "vmArgs": "-Dfabric.dli.config\u003dC:\\Users\\...\\Project@@0020MindCraft\\project-mindcraft\\Simulator\\.gradle\\loom-cache\\launch.cfg -Dfabric.dli.env\u003dclient -Dfabric.dli.main\u003dnet.fabricmc.loader.impl.launch.knot.KnotClient",
      "args": ""
    },
    {
      "type": "java",
      "name": "Minecraft Server",
      "request": "launch",
      "cwd": "${workspaceFolder}\\project-mindcraft\\Simulator\\run",
      "console": "internalConsole",
      "stopOnEntry": false,
      "mainClass": "net.fabricmc.devlaunchinjector.Main",
      "vmArgs": "-Dfabric.dli.config\u003dC:\\Users\\...\\Project@@0020MindCraft\\project-mindcraft\\Simulator\\.gradle\\loom-cache\\launch.cfg -Dfabric.dli.env\u003dserver -Dfabric.dli.main\u003dnet.fabricmc.loader.impl.launch.knot.KnotServer",
      "args": "nogui"
    }
  ]
}
```

### Setting up for the Server

1. To use the minecraft server you must first accept the eula provided, and build your server environment. These steps can be completed by running the `build_environment.py` file in the `project-mindcraft` folder
2. To connect to your server from a local machine you use the ip address `127.0.0.1` and check the `server-port` value in `server.properties`, this gives you a connection address of `127.0.0.1:25565` by default. Alternatively start the server controller and this will handle IP addresses and ports automatically for you. To connect to the server from another machine you may need to adjust port-forwarding properties on your router (*__i am not an expert at this part__*).
3. We have implemented a Server Verification mode if you wish to use it, this works in part with the client side where the client will automatically send a verification message when joining a server. If no verification message is received within a specified time-frame, then the recently connected client is kicked from the server. This is toggled with the `server_verification_time` in `system.py` initialisation, and is __disabled__ by default.

## Using the System Controller

The system controller `system.py` is designed to be run as a module, it can be tested by running the `start_server_controller.py` file in the `project-mindcraft` folder, where it will display debugging information.

### Requirements

This module has some additional prerequisites before you can run it:

``` cmd
python3.8 -m pip install gpustat==1.0.0 --upgrade
python3.8 -m pip install configobj==5.0.8 --upgrade
python3.8 -m pip install ipython==7.29.0 --upgrade
```

For python versions less that 3.8 you will require also:

``` cmd
python -m pip install typing-extensions
```

### Controller.ComputeManager Class

The `ComputeManager` class is the main interface to interact with when creating multiple minecraft instances (and a server) on your machine, it will take care of communication and performance optimizations itself, where you can focus on actions and observations for each of the instances. It can by initialised with a [`ServerConfig`](#using-the-configuration-classes) and [`ControllerConfig`](#using-the-configuration-classes) class which can be modified via the following fields:  

__Util.ServerConfig Fields:__  

``` python
online_mode: bool = False # Does the server use minecraft auth (recommended to be False with unverified accounts)
server_verification_time: int = -1 # How long will the server wait for client verification, values less that 1000milliseconds will result in no verification taking place
scenario_spacing: int = 1 # When placing scenarios, how many blocks gap should be placed in-between each one
scenario_direction: int = 0 # When placing scenarios, should they be placed adjacent in the x=0 or z=2 direction
scenario_pos: [*int] = [100,63,100] # Where should the server start placing scenarios (ideally this should be high, away from other blocks and spawn)
auto_spawn: bool = False # Should this class be able to start or stop instances automatically? (WARNING: This could lead to unpredictable behaviour in the middle of training)
monitor_frequency: int = 30 # How often should this class monitor performance (in seconds)
client_connection: Literal["local_server", "external_server", "singleplayer"] = "local_server" # Will an initiated client attempt to connect to a local/external server, or will it join a singleplayer world?
server_ip: str = "127.0.0.1" # The server IP the client will connect to (only used for external servers)
server_port: int = "25565" # The server Port the client will connect to (not used for singleplayer clients)
server_world: str = "default" # The default world the server will use, these worlds should be located in "mindcraft-loader/server worlds/" with a folder name equal to the name provided here
logger_name: str = "Minecraft Server" # The name of the source file when displaying in the logs
```

__Util.ControllerConfig Fields:__  

``` python
default_singleplayer: str = "Void World" # The default singleplayer world to autoload on startup
server_ip: str = "127.0.0.1" # The server ip that the client will try to connect to
server_port: int = "25565" # The server port that the client will try to connect to
bridge_port: int = "42000" # The bridge port that the client will listen for actions on
server_autoconnect: bool = True # Will the server attempt to autoconnect to the server?
break_modifier: float = 50 # Break blocks X times faster
eat_modifier: float = 50 # Eat food X times faster
blacklisted_ports: [*int] = [] # Which ports should the client not use as a bridge port
autostart_client_instance: bool = True # Should the controller start a Minecraft Client with it?
logger_name: str = "Minecraft Client" # The name of the source file when displaying in the logs
```

#### Use Cases

##### Starting a local server

Pass the following arguments to instantiate the class:

``` python
client_connection: "local_server"
```

##### Starting a local server on a specified port

Pass the following arguments to instantiate the class:

``` python
client_connection: "external_server"
server_port: "..."
```

Let the following arguments assume their default values:

``` python
server_ip: "127.0.0.1"
```

##### Start local training without a server

Pass the following arguments to instantiate the class:

``` python
client_connection: "singleplayer"
```

##### Perform training on an external server

Pass the following arguments to instantiate the class:

``` python
client_connection: "external_server"
server_ip: "..."
server_port: "..."
```

#### Clients

This is the main field `clients` you should interact with, it returns `List[CommandManager]` type where you should use as per the [Command Manager Documentation](#commandmanager-class).

#### Start Client

__Method:__ `start_client`  
__Parameters:__ `None`  
__Description:__  
This method will manually start another minecraft instance, you will be able to access the new `CommandManager` in the `clients` field. This method will return `int` corresponding to the position of the controller in the `clients` field

#### End Client

__Method:__ `end_client`  
__Parameters:__ `pos: int = -1`  
__Description:__  
This method will manually terminate the last created minecraft instance, or if specified the instance at pos.

#### Calibrate

__Method:__ `calibrate`  
__Parameters:__ `None`  
__Description:__  
This method will determine automatically if you have the resources to start a new minecraft instance, or too many resources for the current instances running, and either start or terminate an instance. If you have recently started a minecraft instance you will have to wait at-least a minute for performance monitoring values to stabilize, this method will do nothing if called within a minute of last creating or termination of a client. This method will run in rhythm with the process monitor if the `auto_spawn` property is enabled.  
Returns `-1` if a client is terminated, `1` if a client is started, and `0` otherwise.

#### Is Ready

__Method:__ `is_ready`  
__Parameters:__ `None`  
__Description:__  
This method will return `True` if the Minecraft Server is ready to receive players, otherwise it will return `False`.

#### Reset Server

__Method:__ `reset_server`  
__Parameters:__ `blocking:bool = True`  
__Description:__  
This method will reset the server to a fresh world, if `blocking` is set to `True`, it will not complete until all clients have reconnected to the server again, otherwise it will return after the server process has begun starting up.

## Using the Controller

The controller is designed to be the point of interaction between our training program, and the Minecraft environment. It is designed to be used as a module, however can be tested by running the `start_client_controller.py` file in the `project-mindcraft` folder, where it will display debugging information and allow user input for actions / observations.

### CommandManager Class

The `CommandManager` class is used as the main interface for the Minecraft mod, communicating through sockets. On initialisation of the CommandManager class, a new Minecraft Client process will start and this class will connect to it once ready. It can be initialised with a [`ControllerConfig`](#using-the-configuration-classes) class which can be modified via the following fields:  

__Util.ControllerConfig Fields:__  

``` python
default_singleplayer: str = "Void World" # The default singleplayer world to autoload on startup
server_ip: str = "127.0.0.1" # The server ip that the client will try to connect to
server_port: int = "25565" # The server port that the client will try to connect to
bridge_port: int = "42000" # The bridge port that the client will listen for actions on
server_autoconnect: bool = True # Will the server attempt to autoconnect to the server?
break_modifier: float = 50 # Break blocks X times faster
eat_modifier: float = 50 # Eat food X times faster
blacklisted_ports: [*int] = [] # Which ports should the client not use as a bridge port
autostart_client_instance: bool = True # Should the controller start a Minecraft Client with it?
logger_name: str = "Minecraft Client" # The name of the source file when displaying in the logs
```

In this class you have the following methods:

#### Is Ready

__Method:__ `is_ready`  
__Parameters:__ `None`  
__Description:__  
This method will return `True` if the Minecraft Instance is ready to receive requests, otherwise it will return `False`.

#### Send Action

__Method:__ `send_action`  
__Parameters:__  

``` python
*actions: ActionType
```

These are types returned by the [ActionManager Class](#action-class) described below.

__Description:__
This method sends a list of actions to the Minecraft Client to perform. If actions provided are key presses, the key will be held until the next send_action request without that key press action.

#### Request Observation

__Method:__ `request_observation`  
__Parameters:__  

``` python
*observations: ObservationType
```

These are types returned by the [ObservationManager Class](#observation-class) described below.

__Description:__  
This method requests the observation space from the Minecraft Client and returns the data, the format of the [observation space](#observation-space) is described below. If `ObservationType` parameters are provided, only the selected observations will be received.

### ActionManager Class

The `ActionManager` class is used as the main way to access actions that the Minecraft Client can perform, the return type will always be of `ActionType`.

#### Fields

- `JUMP` press jump key
- `LEFT_CLICK` left click
- `RIGHT_CLICK` right click
- `PICK` middle click
- `MOVE_LEFT` press left key
- `MOVE_RIGHT` press right key
- `MOVE_FORWARD` press up key
- `MOVE_BACKWARD` press down key
- `SNEAK` press shift key
- `SPRINT` press ctrl key
- `NONE` press no key (equivalent to unpressing all keys)
- `SWAP_OFFHAND` swaps the currently selected items and the offhand items
- `DROP_SELECTED` drops all of the currently selected items
- `DROP_OFFHAND` drops all of the offhand items
- `RESPAWN` requests that the player is respawned

#### Methods

- `HOTBAR(slot:int = 0)` select hotbar slots 0-8
- `DROP(slot:int = 0)` drops all of the item in a slot 0-40
- `DROP_ARMOUR(slot:int = 0)` drops all of the items in a armour slot 0-3 in order boots, leggings, chestplate, helmet
- `DROP_INVENTORY(slot:int = 0)` essentially the same as `DROP` where slots 0-8 are hotbar slots and slots 9-35 are inventory slots
- `SWAP(slot:int = 0)` swaps the currently selected items and items in a slot 0-40
- `SWAP_ARMOUR(slot:int = 0)` swaps the currently selected items and items in a armour slot 0-3 in order boots, leggings, chestplate, helmet
- `SWAP_INVENTORY(slot:int = 0)` essentially the same as `SWAP` where slots 0-8 are hotbar slots and slots 9-35 are inventory slots
- `CAMERA_LEFT(degrees:float = 10)` move mouse left by degrees
- `CAMERA_RIGHT(degrees:float = 10)` move mouse right by degrees
- `CAMERA_UP(degrees:float = 10)` move mouse up by degrees
- `CAMERA_DOWN(degrees:float = 10)` move mouse down by degrees
- `CAMERA(horizontal:float = 0, vertical:float = 0)` move mouse right by horizontal and up by vertical
- `SET_CAMERA(horizontal:float = 0, vertical:float = 0)` set camera angles absolutely in the world
- `SCENARIO(name:str = "")` load scenario with name at player position (if on singleplayer otherwise defined by server) and teleport inside, these scenarios should be located within the scenarios folder. This action will automatically perform the `RESPAWN` action before performing it's own action. All entities and blocks in this area will be cleared.

### ObservationManager Class

The `ObservationManager` class is used as the main way to access observation modifiers that can modify the observation from the Minecraft Client, the return type will always be of `ObservationType`.

#### Fields

- `ALL` whitelist all fields of the observation
- `HOTBAR` whitelist only hotbar slots __NOT IMPLEMENTED__
- `INVENTORY` whitelist the inventory field of the observation
- `BLOCK_ID` whitelist the blocks field for a 16x16x16 range of the observation
- `SCREEN_PIXEL` whitelist the screen pixels field of the observation
- `ENTITIES` whitelist the entities field for a 16x16x16 range of the observation
- `PLAYER_DAT` whitelist the player data field of the observation
- `CHANGES_ONLY` limits the received data to only where data has changed from previous observation, this is limited to blocks and inventory.

#### Methods

- `BLOCK_ID_RANGE(range: Union[str,int])` whitelist the blocks field for a specified range of the observation
- `ENTITIES_RANGE(range: Union[str,int])` whitelist the entities field for a specified range of the observation

## Using the Configuration Classes

The configuration classes accessed in the `Util` package are designed to immitate a python `dict` with a few extra rules to satisfy the need as a config class:

- They are initialised with specific fields and rules
- The fields are __not__ open to extension, only given fields will affect behaviour and trying to add new fields will result in an exception
- The fields must follow rules or specified data types, if given data does not follow these rules an exception will be raised
- Changes to the config will be locked when used by their respective class to reduce unpredictability, and changes after passing it as an argument will result in an exception
- If a value is not provided, the default value will be given when accessing values

__Util.GLOBAL_CONFIG Fields:__  

This is a global config file that can be accessed and read by multiple classes, it should be modified __before__ using other classes that may rely upon it since it will be locked afterwards.

``` python
logging_string: str = "%(cb)s[%(time)s]%(ec)s %(logging_colour)s[%(logging_level)s:%(caller_file)s/%(caller_line)s]%(ec)s %(cv)s(%(channel)s)%(ec)s %(message)s" # How should the logging messages in the terminal be formatted
logging_stream: class = sys.stdout # Where should logging messages be sent, this should be a class containing a write() method
logging_level: str = "INFO" # What level priority messages should the logger show, it will show the same or higher priority messages in the order: DEBUG < INFO < WARN < ERROR < NONE
```

## Using the LOGGER

The global logger can be imported with `from Util import LOGGER` and has methods such as `debug`, `info`, `warn`, `error` to log on the different levels. If no channel is provided it will use the current functions name instead.

## Observation Space

The observation space will be in the following JSON format:

``` javascript
{
    "inventory": [  // for all slots in the inventory
        {
            "itemName": "minecraft:wooden_sword", 
            "itemDamage": 5,
            "stackSize": 1,
            "isActive": false
        },
        null, // represents empty slot
        null,
        ...
    ],
    "inventory_changes": [  // alternative for inventory changes
        {
            "i": 1,
            "slot": null
        },
        {
            "i": 9,
            "slot": {
                "itemName": "minecraft:diamond_sword", 
                "itemDamage": 5,
                "stackSize": 1,
                "isActive": false
            }
        },
        ...
    ],
    "blocks": [  // for all blocks in a 33x33x33 area around the player (this should be offset by -16 in each direction)
        ["minecraft:stone", "minecraft:gold_block", null, ...],
        [null, ...], // represents air blocks
        [...],
        ...
    ], 
    "block_changes": [  // alternative for block changes (already offset by -16 in each coordinate)
        {"x": 0, "y": 5, "z": 10, "block": "minecraft:stone"},
        ...
    ],
    "entities": [ // for all entities in a 16x16x16 area around the player
        {
            "entityId": "minecraft:skeleton",
            "position": [1.0, 1.0, 1.0],
            "velocity": [0.0, 0.0, 0.0],
            "lookVector": [5.0, 0.0, 1.0]
        },
        {
            "entityId": "minecraft:pig",
            "position": [5.0, 1.0, 1.0],
            "velocity": [0.0, 0.0, 3.0],
            "lookVector": [0.0, 0.0, 1.0]
        },
        ...
    ],
    "playerData": {
        "position": [5.0, 1.0, 1.0],
        "velocity": [0.0, 0.0, 3.0],
        "lookVector": [0.0, 0.0, 1.0],
        "isOnGround": true,
        "isSneaking": false,
        "health": 20.0,
        "saturation": 2.0,
        "armorLevel": 5,
        "hunger": 20,
        "xp" : 120,
        "breath": -15
    },
    "screenBytes": [43, 64, 151, ...], // List of pixels for the screen
    "screenWidth": 420, // width of the screen in pixels
    "screenHeight": 240, // height of the screen in pixels
}
```
