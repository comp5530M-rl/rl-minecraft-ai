from collections import OrderedDict
from typing import List, Union, Callable
from mindcraft.Util import is_sequence


class ObservationType:
    def __init__(self, observation: str) -> None:
        self._type = observation

    def __repr__(self) -> str:
        return "Observation'%s'" % (self.type)

    @property
    def type(self):
        return self._type


# Helper class for selecting from available observations.
class ObservationManager:
    ALL = ObservationType("ALL")
    HOTBAR = ObservationType("HOTBAR")
    INVENTORY = ObservationType("INVENTORY")
    CHANGES_ONLY = ObservationType("CHANGES")
    BLOCK_ID = ObservationType("BLOCKS")
    BLOCK_ID_RANGE: Callable[
        [Union[str, int]], ObservationType
    ] = lambda x: ObservationType("BLOCKS " + str(x))
    SCREEN_PIXEL = ObservationType("PIXELS")
    ENTITIES = ObservationType("ENTITY")
    ENTITIES_RANGE: Callable[
        [Union[str, int]], ObservationType
    ] = lambda x: ObservationType("ENTITIES " + str(x))
    PLAYER_DAT = ObservationType("PLAYER_DAT")
    SCENARIO = ObservationType("SCENARIO")

    COMMAND_LIST = {
        "image": SCREEN_PIXEL,
        "health": PLAYER_DAT,
        "hunger": PLAYER_DAT,
        "saturation": PLAYER_DAT,
        "breath": PLAYER_DAT,
        "isOnGround": PLAYER_DAT,
        "equipped": INVENTORY,
        "hotbar": INVENTORY,
        "inventory": INVENTORY,
        "all": ALL,
        "blocks": BLOCK_ID,
        "blocksR": BLOCK_ID_RANGE,
        "changes": CHANGES_ONLY,
        "entity": ENTITIES,
        "entityE": ENTITIES_RANGE,
        "player": PLAYER_DAT,
        "screen": SCREEN_PIXEL,
        "scenario": SCENARIO,
        "reward_functions": SCENARIO,
    }

    @staticmethod
    def command(
        *observations: Union[ObservationType, str, List[Union[ObservationType, str]]]
    ) -> str:
        header = "MindclientProtocol/0.1 REQUEST OBSERVATION"
        for obs in observations:
            if is_sequence(obs):
                for subObs in obs:
                    if isinstance(subObs, str):
                        if subObs in ObservationManager.COMMAND_LIST.keys():
                            subObs = ObservationManager.COMMAND_LIST[subObs]
                        else:
                            raise ValueError(
                                "Parameter must be a valid string, not '%s'. You can choose a valid string from: %s"
                                % (subObs, list(ObservationManager.COMMAND_LIST.keys()))
                            )
                    header += "\n" + subObs.type
            else:
                if isinstance(obs, str):
                    if obs in ObservationManager.COMMAND_LIST.keys():
                        obs = ObservationManager.COMMAND_LIST.get(obs)
                    else:
                        raise ValueError(
                            "Parameter must be a valid string, not '%s'. You can choose a valid string from: %s"
                            % (obs, list(ObservationManager.COMMAND_LIST.keys()))
                        )
                header += "\n" + obs.type
        return (
            "\n".join(list(OrderedDict.fromkeys(header.split("\n"))))
            + "\nMindclientProtocol/0.1 END_REQUEST\n"
        )
