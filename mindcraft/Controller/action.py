from collections import OrderedDict
from typing import List, Union

from mindcraft.Util import is_sequence

class ActionType:
    def __init__(self, action: str) -> None:
        self._type = action

    def __repr__(self) -> str:
        return "Action'%s'"%(self.type)

    @property
    def type(self):
        return self._type
    

# Helper class for selecting from available actions.
class ActionManager:
    JUMP = ActionType("JUMP")

    LEFT_CLICK = ActionType("ATTACK")
    RIGHT_CLICK = ActionType("USE")
    PICK_BLOCK = ActionType("PICK")

    MOVE_LEFT = ActionType("LEFT")
    MOVE_RIGHT = ActionType("RIGHT")
    MOVE_FORWARD = ActionType("FWD")
    MOVE_BACKWARD = ActionType("BACK")

    SNEAK = ActionType("SNEAK")
    SPRINT = ActionType("SPRINT")
    RESPAWN = ActionType("RESPAWN")

    DISCONNECT = ActionType("DISCONNECT")

    RESET = ActionType("RESET")

    NONE = ActionType("NONE")



    @staticmethod
    def HOTBAR(slot: Union[str, int] = 0) -> ActionType:
        return ActionType("HOTBAR %d" % (int(slot)))

    @staticmethod
    def SWAP(slot: Union[str, int] = 0) -> ActionType:
        return ActionType("SWAP %d" % (int(slot)))

    @staticmethod
    def SWAP_ARMOUR(slot: Union[str, int] = 0) -> ActionType:
        return ActionManager.SWAP(slot + 36)

    SWAP_INVENTORY = SWAP
    SWAP_OFFHAND = ActionType("SWAP 40")

    @staticmethod
    def DROP(slot: Union[str, int] = 0) -> ActionType:
        return ActionType("DROP %d" % (int(slot)))

    DROP_SELECTED = ActionType("DROP")

    @staticmethod
    def DROP_ARMOUR(slot: Union[str, int] = 0) -> ActionType:
        return ActionManager.DROP(slot + 36)

    DROP_INVENTORY = DROP
    DROP_OFFHAND = ActionType("DROP 40")

    @staticmethod
    def CAMERA_LEFT(degrees: Union[str, int, float] = 10) -> ActionType:
        return ActionType("CAMERA %f 0" % (-float(degrees)))

    @staticmethod
    def CAMERA_RIGHT(degrees: Union[str, int, float] = 10) -> ActionType:
        return ActionType("CAMERA %f 0" % (float(degrees)))

    @staticmethod
    def CAMERA_UP(degrees: Union[str, int, float] = 10) -> ActionType:
        return ActionType("CAMERA 0 %f" % (float(degrees)))

    @staticmethod
    def CAMERA_DOWN(degrees: Union[str, int, float] = 10) -> ActionType:
        return ActionType("CAMERA 0 %f" % (-float(degrees)))

    @staticmethod
    def CAMERA(
        horizontal_degrees: Union[str, int, float] = 0,
        vertical_degrees: Union[str, int, float] = 0,
    ) -> ActionType:
        return ActionType(
            "CAMERA %f %f" % (float(horizontal_degrees), float(vertical_degrees))
        )

    @staticmethod
    def SET_CAMERA(
        horizontal_degrees: Union[str, int, float] = 0,
        vertical_degrees: Union[str, int, float] = 0,
    ) -> ActionType:
        return ActionType(
            "SETCAMERA %f %f" % (float(horizontal_degrees), float(vertical_degrees))
        )

    @staticmethod
    def SCENARIO(name: str = "") -> ActionType:
        return ActionType("INIT %s" % (name.strip()))
    
    COMMAND_LIST = {
        "init": SCENARIO.__func__,
        "jump": JUMP,
        "click": LEFT_CLICK,
        "left": MOVE_LEFT,
        "right": MOVE_RIGHT,
        "forward": MOVE_FORWARD,
        "backward": MOVE_BACKWARD,
        "camera": CAMERA.__func__,
        "sneak": SNEAK,
        "none": NONE,
        "hotbar": HOTBAR.__func__,
        "swap": SWAP.__func__,
        "sprint": SPRINT,
        "use": RIGHT_CLICK,
        "drop": DROP_SELECTED,
        "pick": PICK_BLOCK,
        "respawn": RESPAWN,
        "reset": RESET,
    }
    
    # Wraps multiple actions into a single request
    @staticmethod
    def command(*actions: Union[ActionType, List[ActionType]]) -> str:
        header = "MindclientProtocol/0.1 REQUEST ACTION"
        for action in actions:
            if is_sequence(action):
                for subAction in actions:
                    header += "\n" + subAction.type
            else:
                header += "\n" + action.type
        return "\n".join(list(OrderedDict.fromkeys(header.split("\n")))) + "\nMindclientProtocol/0.1 END_REQUEST\n"