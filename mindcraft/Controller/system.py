import psutil, pathlib, subprocess, time, gpustat, configobj, socket, os, platform, shutil, sys

from threading import Thread, Lock
from datetime import datetime, timedelta
from typing import List, Union

from mindcraft.Util import ServerConfig, ControllerConfig, LOGGER, logger
from mindcraft.Controller import CommandManager, ActionManager
from mindcraft.build_environment import _sign_eula


class ComputeManager:
    _LISTEN_MESSAGE = 'For help, type "help"'

    _PATH = pathlib.Path(__file__).parent.parent.joinpath("Simulator")
    _SERVER_PROPERTIES_PATH = _PATH.joinpath("run/server.properties")
    _SERVER_EULA_PATH = _PATH.joinpath("run/eula.txt")
    _SERVER_WORLD_PATH = _PATH.joinpath("run/world")
    _SERVER_WORLDS_PATH = _PATH.joinpath("server worlds")
    _START_SERVER_COMMAND = f"{'' if platform.system() == 'Windows' else 'bash ./'}gradlew runServer -Dfabric.dli.config=%(argPath)s\\.gradle\\loom-cache\\launch.cfg -Dfabric.dli.env=server -Dfabric.dli.main=dnet.fabricmc.loader.impl.launch.knot.KnotServer"

    _CREATION_LOCK = Lock()

    # Start performance monitor and the server
    def __init__(
        self,
        server_config: ServerConfig = ServerConfig(),
        client_config: ControllerConfig = ControllerConfig(),
    ):
        self.client_config = client_config.copy()
        self.config = server_config
        self.config.lock()

        self._initialise_variables()
        self.client_config.set_values({
            "server_ip": "127.0.0.1" if self.config["client_connection"] != "external_server" else self.config["server_ip"],
            "server_port": self._server_properties["server-port"] if self.config["client_connection"] != "external_server" and "server-port" in self._server_properties else self.config["server_port"],
            "server_autoconnect": True if self.config["client_connection"] != "singleplayer" else False,
            "autostart_client_instance": True,
            "break_modifier":self.config["break_modifier"],
            "eat_modifier":self.config["eat_modifier"]
        })

        if (
            self.client_config["server_ip"] == "127.0.0.1"
            and self.client_config["server_autoconnect"]
        ):
            self._start_server()
        self._start_monitor()

    def __getstate__(self):
        print("GETSTATE")
        return {
            "clients": self._clients,
            "client_config": self.client_config,
            "server_config": self.config,
            "server": self._server.pid,
            "server_world": self._server_world
        }
    
    # For multiprocessing, needs testing and potentially extra protections to make it thread safe
    # TODO: Test & Protect Class
    def __setstate__(self, state):
        self.config = state["server_config"]
        self.client_config = state["client_config"]
        self._initialise_variables()
        self._clients = state["clients"]
        self._server = psutil.Process(state["server"])
        self._server_world = state["server_world"]



    # Set default values for variables used within this class
    def _initialise_variables(self):
        LOGGER.debug("Initialising Variables")
        self._clients: List[CommandManager] = []
        self._ready: bool = False
        self._last_client_change: datetime = datetime.min
        self._server_drain: List[float] = [0, 0, 0]
        self._client_drain: List[float] = [0, 0, 0]
        self._cpu_percent: float = psutil.cpu_percent()
        self._mem_percent: float = psutil.virtual_memory().percent
        self._initial_gpu: float = gpustat.new_query().gpus[0].utilization
        self._gpu_percent: float = gpustat.new_query().gpus[0].utilization
        self._server: Union[subprocess.Popen[str], None] = None
        self._server_properties = configobj.ConfigObj(
            str(self._SERVER_PROPERTIES_PATH.absolute()), write_empty_values=True
        )
        LOGGER.debug("Provided server world {}found".format("" if self._SERVER_WORLDS_PATH.joinpath(self.config["server_world"]).is_dir() else "not "))
        self._server_world: pathlib.Path = (
            self._SERVER_WORLDS_PATH.joinpath(self.config["server_world"])
            if self._SERVER_WORLDS_PATH.joinpath(self.config["server_world"]).is_dir()
            else self._SERVER_WORLDS_PATH.joinpath("default")
        )

    def _rebuild_server(self) -> None:
        LOGGER.info("Rebuilding server, please be patient...")
        path = pathlib.Path(__file__).parent.parent.joinpath("build_environment.py")
        subprocess.run([sys.executable, str(path.absolute())])


    def _check_server_build_status(self) -> bool:
        LOGGER.debug("Checking Server Build Status")
        result = True
        if not self._SERVER_EULA_PATH.is_file():
            LOGGER.info("Server EULA does not exist")
            result = False
        else:
            config = configobj.ConfigObj(str(self._SERVER_EULA_PATH.absolute()), write_empty_values=True)
            if config["eula"] == "false":
                LOGGER.info("Server EULA is not signed")
                _sign_eula(self._SERVER_EULA_PATH)
        if not self._SERVER_PROPERTIES_PATH.is_file():
            LOGGER.info("Server Properties does not exist")
            result = False
        return result
        

    # Start the server
    def _start_server(self, locked_properties=False) -> None:
        if not self._check_server_build_status():
            self._rebuild_server()
        command = self._START_SERVER_COMMAND % {
            "argPath": str(self._PATH.cwd()).replace(" ", "@@0020")
        }
        environment = {
            "verificationTime": str(self.config["server_verification_time"]),
            "scenarioSpacing": str(self.config["scenario_spacing"]),
            "scenarioPos": ",".join(map(str, self.config["scenario_pos"])),
            "difficulty": str(self.config["difficulty"]),
            "breakModifier": str(self.config["break_modifier"]),
            "eatModifier": str(self.config["eat_modifier"]),
            "keepInventory": str(self.config["keep_inventory"]),
        }
        LOGGER.info("Replacing server world with {}".format(self._server_world.name))
        shutil.rmtree(self._SERVER_WORLD_PATH,ignore_errors=True)
        shutil.copytree(self._server_world, self._SERVER_WORLD_PATH)
        if not locked_properties:
            self._select_port()
            self._server_properties["server-port"] = self._port
            self.client_config["server_port"] = self._port
            self._server_properties["online-mode"] = str(self.config["online_mode"]).lower()
            self._server_properties["spawn-protection"] = "0"
            self._server_properties.write()
            LOGGER.info("Server will be running in online mode: {!r}, world: {!r} and server port: {!r}".format(self.config["online_mode"], self._server_world.name, self._port),"Manager")

        LOGGER.debug("Starting Server Process")
        self._server = subprocess.Popen(
            command,
            stdout=subprocess.PIPE,
            stdin=subprocess.DEVNULL,
            universal_newlines=True,
            shell=True,
            cwd=self._PATH,
            env=dict(os.environ, **environment),
        )

        LOGGER.add_stream(self._server.stdout, self._process_terminal_line, self.config["logger_name"])

    def _kill_server(self):
        process = psutil.Process(self._server.pid)
        for p in process.children(recursive=True):
            p.kill()
        process.kill()

    def _reconnect_clients(self):
        self.wait_for_server()
        for controller in self.clients:
            controller._send_message(controller._RECONNECT)

    def reset_server(self, blocking:bool = True):
        LOGGER.warn("RESTARTING SERVER")
        if not self._ready:
            raise ProcessLookupError("Cannot reset a server that does not exist, try calling wait_for_server() before calling reset_server()")
        LOGGER.info("Disconnecting Clients")
        for controller in self.clients:
            self.wait_for_client(controller)
            controller.send_actions(ActionManager.DISCONNECT)
            controller._ready = False
        time.sleep(0.5)
        LOGGER.info("Rebooting Server")
        self._kill_server()
        self._ready = False
        self._start_server(True)
        LOGGER.info("Starting Client Reconnection Thread")
        reconnection = Thread(target=self._reconnect_clients, name="Client Reconnection")
        reconnection.start()
        if blocking:
            LOGGER.info("Waiting for client reconnections")
            self.wait_for_server()
            for controller in self.clients:
                self.wait_for_client(controller)


    def _process_terminal_line(self, line: str):
        if self._LISTEN_MESSAGE in line:
            self._ready = True
            LOGGER.debug("Server is Ready")
        return logger.format_java_string(line)

    def _select_port(self) -> None:
        self._port = self.config["server_port"]
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            while s.connect_ex(("localhost", self._port)) == 0:
                s.shutdown(socket.SHUT_RDWR)
                self._port += 1
        LOGGER.debug("Selected server port {}".format(self._port))
        self.client_config["blacklisted_ports"] += [self._port]

    # Start the performance monitor
    def _start_monitor(self) -> None:
        monitor_thread = Thread(name="Process Monitor", target=self._process_monitor)
        monitor_thread.start()

    # Start a client instance and track it
    def start_client(self) -> int:
        self._CREATION_LOCK.acquire(True)
        self.client_config["logger_name"] = "Client {}".format(len(self._clients)+1)
        self._clients.append(CommandManager(self.client_config.copy()))
        pos = len(self._clients) -1
        self._CREATION_LOCK.release()
        self.client_config["blacklisted_ports"] += [self._clients[-1]._port]
        self._last_client_change = datetime.now()
        LOGGER.info("Started Client","Manager")
        return pos

    # Remove the last client instance
    def end_client(self, pos = -1):
        client = self._clients.pop(pos)
        self.client_config["blacklisted_ports"] = self.client_config["blacklisted_ports"].remove(client._port)
        self._last_client_change = datetime.now()
        LOGGER.info("Terminated client as position {}".format(pos),"Manager")

    def _get_process_child(self, process: psutil.Process) -> Union[psutil.Process, None]:
        try:
            return process.children()[0].children()[0].children()[0]
        except IndexError:
            return None


    # Find the client processes so they can be monitored
    def _find_client_processes(self) -> List[psutil.Process]:
        return list(
            filter(
                None,
                map(
                    self._get_process_child,
                    map(lambda c: psutil.Process(c.client.pid), self._clients),
                ),
            )
        )

    # Find the server process so it can be monitored
    def _find_server_process(self) -> Union[psutil.Process, None]:
        if self._server:
            return self._get_process_child(psutil.Process(self._server.pid))
        return None

    def _above_performance_limit(self) -> bool:
        return (
            self._cpu_percent > 95 or self._mem_percent > 95 or self._gpu_percent > 95
        )

    def _performance_allows_client(self) -> bool:
        return (
            self._cpu_percent < 95 - self._client_drain[0]
            and self._mem_percent < 95 - self._client_drain[1]
            and self._gpu_percent < 95 - self._client_drain[2]
        )

    # Starts a new minecraft instance if there is enough performance to do so effectively (and kills instances if there are too many)
    def calibrate(self):
        if datetime.now() - self._last_client_change >= timedelta(seconds=30):
            if self._above_performance_limit() and len(self._clients) > 1:
                self.end_client()
                return -1
            elif self._performance_allows_client():
                self.start_client()
                return 1
            else:
                LOGGER.debug("Performance is optimal","Monitoring Thread")
        return 0

    # Blocks the thread until the server is ready
    def wait_for_server(self):
        while not self.is_ready():
            time.sleep(1)

    # Blocks the thread until the client is ready
    def wait_for_client(self, client: CommandManager):
        client.wait_for_client()

    def wait_for_all_clients(self):
        for client in self.clients:
            self.wait_for_client(client)

    # Monitoring Thread to track performance of instances
    def _process_monitor(self):
        if self.config["monitor_frequency"] == -1:
            return
        if self._server:
            self.wait_for_server()
        if self.config["auto_spawn"]:
            self.start_client()

        cpu_cores = psutil.cpu_count()

        time.sleep(10)
        server_process = self._find_server_process()
        client_processes = self._find_client_processes()
        LOGGER.info("Starting Process Monitoring","Monitoring Thread")

        # Start CPU Monitoring
        psutil.cpu_percent()
        if server_process:
            server_process.cpu_percent()
        for process in client_processes:
            process.cpu_percent()

        while True:
            time.sleep(self.config["monitor_frequency"])

            client_processes = list(filter(lambda c: c.is_running() ,client_processes))

            self._cpu_percent = psutil.cpu_percent()
            self._mem_percent = psutil.virtual_memory().percent
            self._gpu_percent = gpustat.new_query().gpus[0].utilization

            if server_process and server_process.is_running():
                self._server_drain[0] = server_process.cpu_percent(None) / cpu_cores
                self._server_drain[1] = server_process.memory_percent("vms")
                LOGGER.debug("Server [CPU: %2.2f%% MEM: %2.2f%% GPU: ~%2.2f%%]" % (self._server_drain[0],self._server_drain[1],self._server_drain[2]), "Monitoring Thread")

            client_cpus = []
            client_mems = []

            if len(self._clients) > 0:
                self._client_drain[2] = (
                    gpustat.new_query().gpus[0].utilization - self._initial_gpu
                ) / len(self._clients)
            for i, cp in enumerate(client_processes):
                client_cpus.append(cp.cpu_percent(None) / cpu_cores)
                client_mems.append(cp.memory_percent("vms"))
                LOGGER.debug("Client %d [CPU: %2.2f%% MEM: %2.2f%% GPU: ~%2.2f%%]" % (i + 1, client_cpus[-1], client_mems[-1], self._client_drain[2]), "Monitoring Thread")

            if len(client_cpus) > 0:
                self._client_drain[0] = sum(client_cpus) / len(client_cpus)
                self._client_drain[1] = sum(client_mems) / len(client_mems)

            if self.config["auto_spawn"]:
                self.calibrate()

            if len(self._clients) != len(client_processes):
                client_processes = self._find_client_processes()

                for process in client_processes:
                    process.cpu_percent()

    def is_ready(self) -> bool:
        return self._ready

    @property
    def clients(self):
        return self._clients