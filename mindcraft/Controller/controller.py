import json, pathlib, os, platform, subprocess, socket
import time
from typing import Union, List

from threading import Lock
from mindcraft.Util import LOGGER, logger, ControllerConfig
from mindcraft.Controller.action import ActionManager, ActionType
from mindcraft.Controller.observation import ObservationManager, ObservationType

# Base class for all controller errors
class BaseControllerError(Exception):
    pass

# Raise when controller needs to be ready
class NotReadyError(BaseControllerError):
    def __init__(self) -> None:
        super().__init__("The controller is not ready yet!")

class RunscriptGenerationError(BaseControllerError):
    def __init__(self) -> None:
        super().__init__("Failed to generate runscript! Check gradle task printRunClientTask")

# Create and manage a connection to a minecraft client
class CommandManager:
    _CONFIRM = "MindclientProtocol/0.1 CONFIRM ACTION\n"
    _VERIFY = "MindclientProtocol/0.1 VERIFICATION\n"
    _RECONNECT = "MindclientProtocol/0.1 REQUEST CONNECTION\n"
    _VERIFY_CONFIRM = "MindclientProtocol/0.1 CONFIRM VERIFICATION"
    _LISTEN_MESSAGE = "Sending verification to server"

    _IP = "127.0.0.1"

    _PATH = pathlib.Path(__file__).parent.parent.joinpath("Simulator")
    _RUNFILE_LOCK = Lock()
    _GENERATE_RUNSCRIPT = 'gradlew.bat printRunClientTask' if platform.system() == 'Windows' else './gradlew printRunClientTask'
    _START_CLIENT_COMMAND = 'run.bat' if platform.system() == 'Windows' else './run.sh'

    # Starts a minecraft client with selected environment variables and listening on next available port
    def __init__(self, config: Union[ControllerConfig, None] = None) -> None:
        self.config = ControllerConfig() if config == None else config
        self.config.lock()

        self._port = self.config["bridge_port"]
        if self.config["autostart_client_instance"]:
            self._ready = False
            self._start_client()
        else:
            self._ready = True
            self._create_connection()

    def __getstate__(self):
        global LOGGER
        if not self._ready:
            raise NotReadyError()
        return {
            "port":self._port,
            "config":self.config,
            "logger":LOGGER
        }
    
    # For multiprocessing, needs testing and potentially extra protections to make it thread safe
    def __setstate__(self, state):
        global LOGGER
        self._port = state["port"]
        self.config = state["config"]
        LOGGER = state["logger"]
        self._ready = True
        self._create_connection()

    # Search for a port that is not in use
    def _select_port(self) -> None:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            while (
                self._port in self.config["blacklisted_ports"]
                or s.connect_ex(("localhost", self._port)) == 0
            ):
                try:
                    s.shutdown(socket.SHUT_RDWR)
                except OSError:
                    pass
                self._port += 1
        LOGGER.debug("Selected bridge port {}".format(self._port))

    # Start a new subprocess for the minecraft client
    def _start_client(self) -> None:
        # check that run script exists, generate by using gradlew
        if self._RUNFILE_LOCK.acquire(timeout=5):
            self._RUNFILE_GENERATED = True
            genFile = subprocess.Popen(
                self._GENERATE_RUNSCRIPT,
                stdout=subprocess.PIPE,
                stdin=subprocess.DEVNULL,
                universal_newlines=True,
                shell=True,
                cwd=self._PATH,
            )
            LOGGER.add_stream(
                genFile.stdout, logger.format_textonly_string("Gradle"), "Gradle Builder"
            )
            genFile.wait()
            if genFile.poll() != 0:
                raise RunscriptGenerationError()

        self._select_port()
        command = self._START_CLIENT_COMMAND
        environment = {
            "port": str(self._port),
            "defaultWorld": self.config["default_singleplayer"],
            "serverIP": self.config["server_ip"],
            "serverPort": str(self.config["server_port"]),
            "serverAutoconnect": str(self.config["server_autoconnect"]),
            "breakModifier": str(self.config["break_modifier"]),
            "eatModifier": str(self.config["eat_modifier"]),
            **{key:str(item) if not isinstance(item,list) else ",".join(map(str,item)) for key,item in self.config["graphics_config"].items()}
        }

        LOGGER.debug("Starting Client Process")
        self.client = subprocess.Popen(
            command,
            stdout=subprocess.PIPE,
            stdin=subprocess.DEVNULL,
            universal_newlines=True,
            shell=True,
            cwd=self._PATH.joinpath("run"),
            env=dict(os.environ, **environment),
        )
        LOGGER.add_stream(
            self.client.stdout, self._process_terminal_line, self.config["logger_name"]
        )

    def _process_terminal_line(self, line: str):
        if self._LISTEN_MESSAGE in line:
            self._ready = True
            self._create_connection()
            LOGGER.debug("Client is Ready")
        return logger.format_java_string(line)

    # Check if the socket is working, if not then try to fix
    def _ready_socket(self) -> bool:
        if self._check_connection():
            return True
        if not self._create_connection():
            return False
        return self._check_connection()

    # Check to see if i can send and receive a message through the socket
    def _check_connection(self) -> bool:
        if not self._ready or not hasattr(self, "socket"):
            return False
        LOGGER.debug("Checking Connection", "Socket")
        try:
            self.socket.sendall("ping\n".encode())
            return self.socket.recv(100).decode("utf-8") == "ping\n"
        except socket.timeout:
            LOGGER.warn(
                "Connection Failed due to timeout!", "Socket"
            )
            return False
        except socket.error as err:
            LOGGER.warn(
                "Connection Failed [%i] '%s'" % (err.errno, err.strerror), "Socket"
            )
            return False

    # Close any previous connections and create a new connection, and verify that it is a minecraft client
    def _create_connection(self) -> bool:
        if not self._ready:
            LOGGER.warn("Client not ready!", "Socket")
            return False
        try:
            self.socket.close()
            LOGGER.debug("Closed previous socket!", "Socket")
        except Exception as err:
            pass
        LOGGER.info("Connecting to Minecraft Client {}:{}".format(self._IP, self._port), "Socket")
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.socket.connect((self._IP, self._port))
            self.socket.settimeout(10)
            self.socket.sendall(self._VERIFY.encode())
            return self._receive_message() == self._VERIFY_CONFIRM
        except socket.error as err:
            LOGGER.error(
                "Could not create connection! [%i] '%s'" % (err.errno, err.strerror),
                "Socket",
            )
            return False

    # Check the input stream at 10000 byte intervals until a full message has been received
    def _receive_message(self, chunk_size: int = 10000, max_requests: int = -1) -> str:
        fragments = []
        req_num = 0
        LOGGER.debug("Waiting for message", "Socket")
        while req_num < max_requests or max_requests == -1:
            try:
                chunk = self.socket.recv(chunk_size).decode("utf-8")
            except socket.timeout:
                LOGGER.error("Timed out when receiving message", "Socket")
                return None
            fragments.append(chunk)
            if len(chunk) < chunk_size and len(chunk) > 0 and chunk[-1] == "\n":
                break
            req_num += 1
        return "".join(fragments)[:-1]

    # Send a message through the socket for the minecraft client
    def _send_message(self, message: str) -> None:
        LOGGER.debug("Sending message","Socket")
        try:
            self.socket.sendall(message.encode())
        except ConnectionAbortedError as err:
            self._log_message(
                "Connection Aborted [%i] '%s'" % (err.errno, err.strerror),
                channel="Socket",
                level="ERROR",
            )

    # Continuously try to reconnect until a max number of tries has been reached (default 2 minutes)
    def _wait_for_connection(self, max_tries = 60) -> bool:
        iter = 0
        while not self._ready_socket():
            LOGGER.warn("Connection did not work, retrying later")
            if iter > max_tries:
                return False
            iter += 1
            time.sleep(2)
        return True

    # Send a collection of actions to the minecraft client
    def send_actions(self, *actions: Union[ActionType, List[ActionType]], max_tries: int = 60) -> bool:
        if not self._wait_for_connection(max_tries):
            return False
        LOGGER.info("Sending Actions {!r} to '{}:{}'".format(actions,self._IP,self._port), "Controller")
        payload = ActionManager.command(*actions)
        self._send_message(payload)
        return self._receive_message() == self._CONFIRM

    # Request an observation from the minecraft client
    def request_observation(
        self,
        *observations: Union[ObservationType, str, List[Union[ObservationType, str]]],
        max_tries: int = 60
    ) -> Union[object, None]:
        if not self._wait_for_connection(max_tries):
            return None
        LOGGER.info("Requesting Observations {!r} from '{}:{}'".format(observations,self._IP,self._port), "Controller")
        payload = ObservationManager.command(*observations)
        self._send_message(payload)

        message = self._receive_message()
        if message == None or len(message) == 0:
            return None
        else:
            try:
                return json.loads(message)
            except json.JSONDecodeError as e:
                LOGGER.error(
                    "Observation format error on data '%s...%s' with message '%s'"
                    % (message[:20], message[-20:], e.msg),
                    "Controller",
                )

    # Check if the minecraft client is ready to receive messages
    def is_ready(self) -> bool:
        return self._ready

    # Close the connection to the minecraft client
    def close_connection(self) -> None:
        self.socket.close()

    def wait_for_client(self):
        while not self.is_ready():
            time.sleep(1)
