import jinja2
import pdfkit
import datetime
import numpy as np


def score2grade(score: float) -> str:
    if score >= 0.9:
        return 'A*'
    elif score >= 0.75:
        return 'A'
    elif score >= 0.60:
        return 'B'
    elif score >= 0.45:
        return 'C'
    elif score >= 0.30:
        return 'D'
    elif score >= 0.15:
        return 'E'
    elif score > 0.0:
        return 'F'
    else:
        return 'DNF'


def generate_report(results):
    today_date = datetime.datetime.now().strftime("%d-%m-%Y")
    now_time = datetime.datetime.now().strftime("%H:%M:%S")    

    context = {'today_date': today_date,
               'now_time': now_time,

               'eval1':'nan',
               'eval2':'nan',
               'eval3':'nan',
               'eval4':'nan',
               'eval5':'nan',
               'eval6':'nan',
               'eval7':'nan',

               'grade1':'nan',
               'grade2':'nan',
               'grade3':'nan',
               'grade4':'nan',
               'grade5':'nan',
               'grade6':'nan',
               'grade7':'nan',
               'grade8':'nan',

               'score1':'nan',
               'score2':'nan',
               'score3':'nan',
               'score4':'nan',
               'score5':'nan',
               'score6':'nan',
               'score7':'nan',
               'score8':'nan',

               'scenario_1_1':'nan',
               'grade_1_1':'nan',
               'score_1_1':'nan', 

               'scenario_2_1':'nan',
               'grade_2_1':'nan',
               'score_2_1':'nan',
               'scenario_2_2':'nan',
               'grade_2_2':'nan',
               'score_2_2':'nan',
               'scenario_2_3':'nan',
               'grade_2_3':'nan',
               'score_2_3':'nan',
               'scenario_2_4':'nan',
               'grade_2_4':'nan',
               'score_2_4':'nan',

               'scenario_3_1':'nan',
               'grade_3_1':'nan',
               'score_3_1':'nan',

               'scenario_4_1':'nan',
               'grade_4_1':'nan',
               'score_4_1':'nan',

               'scenario_5_1':'nan',
               'grade_5_1':'nan',
               'score_5_1':'nan',

               'scenario_6_1':'nan',
               'grade_6_1':'nan',
               'score_6_1':'nan',

               'scenario_7_1':'nan',
               'grade_7_1':'nan',
               'score_7_1':'nan',
               'scenario_7_2':'nan',
               'grade_7_2':'nan',
               'score_7_2':'nan',
            }

    for i, res in enumerate(results):

        context['eval' + str(i+1)] = res.eval.label
        context['grade' + str(i+1)] = score2grade(res.scores.mean())
        context['score' + str(i+1)] = np.around(res.scores.mean(), decimals=2).item()
        res.scores = np.around(res.scores, decimals=2)

        for j, scenario in enumerate(res.eval.scenarios):
            context['scenario_' + str(i+1) + '_' + str(j+1)] = scenario
            context['grade_' + str(i+1) + '_' + str(j+1)] = score2grade(res.scores[j,:].mean())
            context['score_' + str(i+1) + '_' + str(j+1)] = res.scores[j,:].mean().item()

    template_loader = jinja2.FileSystemLoader(searchpath="mindcraft/Eval/report/")
    template_env = jinja2.Environment(loader=template_loader)
    template = template_env.get_template('no_ration.html')
    output_text = template.render(context)

    config = pdfkit.configuration(wkhtmltopdf='/usr/bin/wkhtmltopdf')
    pdfkit.from_string(output_text, 'report.pdf', configuration=config)

if __name__ == "__main__":
    generate_report("")