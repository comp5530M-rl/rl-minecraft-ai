import sys

from pathlib import Path
root = Path(__file__).parent.parent.parent
sys.path.append(str(root.joinpath("Eval/report")))


from report import generate_report