from abc import ABC, abstractmethod

# This file is used to define the evals that are used in the project.
# Specify name of Eval, and the scenarios that are used to evaluate it.

class Eval(ABC):
    def __init__(self, label, scenarios):
        self.label = label
        self.scenarios = scenarios

class Eating(Eval):
    def __init__(self):
        super().__init__("Eating", ["1_1_apple_hotbar"])

class BasicNavigation(Eval):
    def __init__(self):
        super().__init__("Navigation", ["2_1_bushes_1", "2_2_bushes_2", "2_3_hills_1", "2_4_hills_2"])

class Exploration(Eval):
    def __init__(self):
        super().__init__("Exploration", ["3_1_hard_terrain"])
    
class HostileSurvival(Eval):
    def __init__(self):
        super().__init__("Hostile Survival", ["4_1_hostile_forest"])

class HostileAvoidance(Eval):
    def __init__(self):
        super().__init__("Hostile Avoidance", ["5_1_hostile_plains"])

class Foraging(Eval):
    def __init__(self):
        super().__init__("Foraging", ["6_1_passive_forest"])

class HazardNavigation(Eval):
    def __init__(self):
        super().__init__("Hazard Navigation", ["7_1_lava_bridge", "7_2_river_crossing"])

class EvalCollection:
    ALL = [Eating(), 
           BasicNavigation(), 
           Exploration(), 
           HostileSurvival(),
           HostileAvoidance(), 
           Foraging(), 
           HazardNavigation()
        ]
