from abc import ABC, abstractmethod
import time

class Eval(ABC):
    
    @property
    @abstractmethod
    def done(self) -> bool:
        pass

    @property
    @abstractmethod
    def score(self) -> float:
        pass

    @abstractmethod
    def step(self, playerData):
        pass

# Util function
def clamp(x, min, max):
    return max if x > max else min if x < min else x

class EatFood(Eval):
    def __init__(self, player_data, time_limit=120, debug=False):
        self.name = "EatFood"
        print(player_data)
        self.player_data = player_data
        self.food_eaten = 0.0
        self.time_limit = time_limit
        self.time_start = time.time()
        self.time_elapsed = 0.0
        if debug:
            print(f'eat food time limit: {time_limit}')

    # done if the max food has been eaten, the player is dead, or the time limit has been reached
    @property
    def done(self) -> bool:
        return (self.food_eaten >= 1.0) or (self.player_data["health"] <= 0) or (self.time_elapsed > self.time_limit)

    # score is a combination of the amount of food eaten and the time remaining
    @property
    def score(self) -> float:
        score = self.food_eaten * (self.time_limit - self.time_elapsed) / self.time_limit
        return clamp(score, 0.0, 1.0)

    # recognises changes in hunger between steps to determine whether food was eaten.
    def step(self, player_data):
        if player_data["hunger"] - self.player_data["hunger"] >= 1.0:
            self.food_eaten += 1.0
        self.player_data = player_data
        if not self.done:
            self.time_elapsed = time.time() - self.time_start


class SurviveTime(Eval):
    def __init__(self, player_data, time_limit=120, debug=False):
        self.name = "SurviveTime"
        self.player_data = player_data
        self.time_limit = time_limit
        self.time_start = time.time()
        self.time_elapsed = 0.0
        if debug:
            print(f'survive time time limit: {time_limit}')

    # done if the player is dead or the time limit has been reached
    @property
    def done(self) -> bool:
        return (self.player_data["health"] <= 0) or (self.time_elapsed >= self.time_limit)

    # score is the proportion of the time limit survived
    @property
    def score(self) -> float:
        score = self.time_elapsed / self.time_limit
        return clamp(score, 0.0, 1.0)

    def step(self, player_data):
        self.player_data = player_data
        if not self.done:
            self.time_elapsed = time.time() - self.time_start


class MaintainHealth(Eval):
    def __init__(self, player_data, time_limit=120, debug=False):
        self.name = "MaintainHealth"
        self.player_data = player_data
        self.time_limit = time_limit
        self.time_start = time.time()
        self.time_elapsed = 0.0
        self.total_health = 0.0
        self.max_health = 0.0
        if debug:
            print(f'maintain health time limit: {time_limit}')

    # done if the player is dead or the time limit has been reached
    @property
    def done(self) -> bool:
        return (self.player_data["health"] <= 0) or (self.time_elapsed >= self.time_limit)

    # score is the average health proportion over the evaluation
    @property
    def score(self) -> float:
        return (self.total_health / self.max_health) * (self.time_elapsed / self.time_limit)

    def step(self, player_data):
        self.player_data = player_data
        if not self.done:
            self.time_elapsed = time.time() - self.time_start
            self.total_health += self.player_data["health"]
            self.max_health += 20.0

class MaintainHunger(Eval):
    def __init__(self, player_data, time_limit=120, debug=False):
        self.name = "MaintainHunger"
        self.player_data = player_data
        self.time_limit = time_limit
        self.time_start = time.time()
        self.time_elapsed = 0.0
        self.total_hunger = 0.0
        self.max_hunger = 0.0
        if debug:
            print(f'maintain hunger time limit: {time_limit}')

    # done if the player is dead or the time limit has been reached
    @property
    def done(self) -> bool:
        return (self.player_data["health"] <= 0) or (self.time_elapsed >= self.time_limit)

    # score is the average hunger proportion over the evaluation
    @property
    def score(self) -> float:
        return (self.total_hunger / self.max_hunger) * (self.time_elapsed / self.time_limit)


    def step(self, player_data):
        self.player_data = player_data
        if not self.done:
            self.time_elapsed = time.time() - self.time_start


class MaintainCondition(Eval):
    def __init__(self, player_data, time_limit=120, debug=False):
        self.name = "MaintainCondition"
        self.player_data = player_data
        self.time_limit = time_limit
        self.time_start = time.time()
        self.time_elapsed = 0.0
        self.total_health = 0.0
        self.total_hunger = 0.0
        self.max_health = 0.0
        self.max_hunger = 0.0
        if debug:
            print(f'maintain condition time limit: {time_limit}')

    # done if the player is dead or the time limit has been reached
    @property
    def done(self) -> bool:
        return (self.player_data["health"] <= 0) or (self.time_elapsed >= self.time_limit)

    # score is the average health and hunger proportion over the evaluation
    @property
    def score(self) -> float:
        return (self.total_health / self.max_health) * (self.total_hunger / self.max_hunger) * (self.time_elapsed / self.time_limit)

    def step(self, player_data):
        self.player_data = player_data
        if not self.done():
            self.time_elapsed = time.time() - self.time_start
