from typing import List, Union
import numpy as np
import time

from mindcraft.Controller import ComputeManager
from mindcraft.Util import GLOBAL_CONFIG, ServerConfig, ControllerConfig, ClientGraphicsConfig
from mindcraft.RL.envs import MCEnv

from mindcraft.Eval.evals import Eval
from mindcraft.Eval.report import generate_report
from mindcraft.Eval.eval_mode import EatFood, SurviveTime, MaintainCondition, MaintainHealth, MaintainHunger

class Result():
    def __init__(self, eval: Eval, scores: np.ndarray):
        self.eval = eval
        self.scores = scores

def evaluate_agent(evals: List[Eval], policy=None, repeats=3, debug=False) -> np.ndarray:
    GLOBAL_CONFIG["logging_level"] = "NONE"

    server_config = ServerConfig({
        "server_world":"void",
        "break_modifier":0,
    })
    compute_manager = ComputeManager(server_config)
    compute_manager.wait_for_server()
    # env = gym.make('Minecraft-v0')
    env = MCEnv()
    env.connect_to_server(compute_manager)

    results = []
    for eval in evals:
        print("=============================================================")
        print(f"Running eval: {eval.label}")
        scores = np.zeros((len(eval.scenarios), repeats))

        for i, scenario in enumerate(eval.scenarios):
            env.set_scenario(scenario,)

            scenario_scores = np.zeros(repeats)
            for j in range(repeats):
                obs = env.reset()
                time.sleep(4)
                playerData = env.get_raw_obs()['playerData']

                mode = None
                k = 0
                mode_args = env.controller.request_observation("scenario")["reward_functions"]
                while mode_args == [] and k < 10:
                    time.sleep(1)
                    mode_args = env.controller.request_observation("scenario")["reward_functions"]
                    k += 1

                print('-------------------------------------------------------------------')
                if mode_args == []:
                    raise Exception("No mode_args received")
                else:
                    print(f'Loaded mode_args after {k} attempt(s)')

                k = 0
                while k < len(mode_args):
                    if mode_args[k].isnumeric():
                        mode_args[k] = int(mode_args[k])
                        k = k+1
                    else:
                        if mode is None:
                            mode = mode_args[k]
                            mode_args.remove(mode)
                        else:
                            raise Exception("Multiple eval modes not supported")

                print(f'scenario: {scenario}, mode: {mode}, args: {mode_args}')

                if mode == "EatFood":
                    mode = EatFood(playerData, *mode_args, debug=debug)
                elif mode == "SurviveTime":
                    mode = SurviveTime(playerData, *mode_args, debug=debug)
                elif mode == "MaintainHealth":
                    mode = MaintainHealth(playerData, *mode_args, debug=debug)
                elif mode == "MaintainHunger":
                    mode = MaintainHunger(playerData, *mode_args, debug=debug)
                elif mode == "MaintainCondition":
                    mode = MaintainCondition(playerData, *mode_args, debug=debug)
                else:
                    # raise key error
                    raise Exception(f"Reward function {mode} not supported")
                    

                if policy is None:
                    action = np.zeros_like(env.action_space.sample())
                while not mode.done:
                    if policy is not None:
                        action = policy(obs)
                        obs = env.step(action)[0]
                    else: # For showcase/debugging. actions made manually, not by an agent.
                        time.sleep(0.5)

                    playerData = env.get_raw_obs()['playerData']
                    mode.step(playerData)
                
                scenario_scores[j] = mode.score 
            
            print(f"Finished {scenario} scores: {scenario_scores}")
            scores[i,:] = scenario_scores
        
        results.append(Result(eval, scores))

    print("Generating report...")
    generate_report(results)
    print("Finished agent evaluation!")


# For testing
if __name__ == "__main__":
    from evals import EvalCollection
    evaluate_agent(EvalCollection.ALL, repeats=1)