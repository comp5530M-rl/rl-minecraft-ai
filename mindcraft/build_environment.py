import pathlib, platform, subprocess, shutil, time, configobj, psutil
from mindcraft.Util import LOGGER, logger

_PATH = pathlib.Path(__file__).parent.joinpath("Simulator")
_START_BUILDER_COMMAND = (
    f"{'' if platform.system() == 'Windows' else 'bash ./'}gradlew build"
)
_START_SERVER_COMMAND = f"{'' if platform.system() == 'Windows' else 'bash ./'}gradlew runServer -Dfabric.dli.config={str(_PATH.cwd()).replace(' ', '@@0020')}\\.gradle\\loom-cache\\launch.cfg -Dfabric.dli.env=server -Dfabric.dli.main=dnet.fabricmc.loader.impl.launch.knot.KnotServer"


def _sign_eula(path: pathlib.Path):
    eula = configobj.ConfigObj(str(path.absolute()), write_empty_values=True)
    if eula["eula"] == "false":
        time.sleep(1)
        i = input(
            "\nType YES to agree to the Minecraft Eula agreement (https://account.mojang.com/documents/minecraft_eula): "
        ).lower()
        while not i in ["y", "yes"]:
            i = input(
                "\nType YES to agree to the Minecraft Eula agreement (https://account.mojang.com/documents/minecraft_eula): "
            ).lower()
        del i

        eula["eula"] = "true"
        eula.write()


if __name__ == "__main__":
    LOGGER.info("CLEARING CURRENT BUILD", "Builder")

    try:
        shutil.rmtree(_PATH.joinpath(".gradle"))
    except PermissionError as e:
        LOGGER.warn("Could not remove .gradle due to {!r}".format(e.strerror))

    LOGGER.info("BUILDING ENVIRONMENT", "Builder")

    builder = subprocess.Popen(
        _START_BUILDER_COMMAND,
        stdout=subprocess.PIPE,
        stdin=subprocess.DEVNULL,
        universal_newlines=True,
        shell=True,
        cwd=_PATH,
    )

    LOGGER.add_stream(
        builder.stdout,
        lambda l: {
            "message": l.rstrip(),
            "channel": "Gradle",
            "level": "INFO",
            "caller_line": "",
        },
        "Gradle Builder",
    )

    while builder.poll() is None:
        time.sleep(0.5)

    LOGGER.info("CHECKING SERVER EULA EXISTS", "Builder")

    properties = _PATH.joinpath("run/server.properties")
    eula = _PATH.joinpath("run/eula.txt")

    SERVER_COMPLETE = False

    def _process_server_line(line: str, check="Failed to load eula.txt"):
        global SERVER_COMPLETE
        if check in line:
            SERVER_COMPLETE = True
        return logger.format_java_string(line)

    if not eula.is_file():
        LOGGER.info("STARTING SERVER TO INITIALISE EULA", "Builder")
        server = subprocess.Popen(
            _START_SERVER_COMMAND,
            stdout=subprocess.PIPE,
            stdin=subprocess.DEVNULL,
            universal_newlines=True,
            shell=True,
            cwd=_PATH,
        )

        LOGGER.add_stream(server.stdout, _process_server_line, "Minecraft Server")

        while server.poll() is None and not SERVER_COMPLETE:
            time.sleep(0.5)

        LOGGER.info("TERMINATING SERVER", "Builder")

        process = psutil.Process(server.pid)
        for p in process.children(recursive=True):
            p.kill()
        process.kill()

    LOGGER.info("CHECKING EULA CONTENTS", "Builder")

    _sign_eula(eula)

    LOGGER.info("CHECKING SERVER PROPERTIES STATUS", "Builder")
    SERVER_COMPLETE = False

    if not properties.is_file():
        LOGGER.info("STARTING SERVER TO INITIALISE SERVER PROPERTIES", "Builder")
        server = subprocess.Popen(
            _START_SERVER_COMMAND,
            stdout=subprocess.PIPE,
            stdin=subprocess.DEVNULL,
            universal_newlines=True,
            shell=True,
            cwd=_PATH,
        )

        LOGGER.add_stream(
            server.stdout,
            lambda l: _process_server_line(
                l, "Failed to load properties from file: server.properties"
            ),
            "Minecraft Server",
        )

        while server.poll() is None and not SERVER_COMPLETE:
            time.sleep(0.5)

        LOGGER.info("TERMINATING SERVER", "Builder")

        process = psutil.Process(server.pid)
        for p in process.children(recursive=True):
            p.kill()
        process.kill()

    LOGGER.info("SUCCESS", "Builder")
