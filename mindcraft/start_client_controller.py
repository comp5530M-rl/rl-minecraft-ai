import time, sys
from inspect import signature
from IPython.lib.pretty import pprint

from Controller import CommandManager, ActionManager, ObservationManager
from Util import GLOBAL_CONFIG, BufferedStreamPrinter

if __name__ == "__main__":
    controller_tag = "\x1b[92m[CONTROLLER]\x1b[0m"
    buffered_printer = BufferedStreamPrinter()
    GLOBAL_CONFIG["logging_stream"] = buffered_printer
    GLOBAL_CONFIG["logging_level"] = "INFO"

    # Define the valid commands
    commands = ["observe", "action", "exit"]

    # Define the valid actions & observations
    actions = ActionManager.COMMAND_LIST
    observations = ObservationManager.COMMAND_LIST

    # Create Command Manager
    manager = CommandManager()
    while not manager.is_ready():
        time.sleep(0.5)
        buffered_printer.print()

    while True:
        # Display message buffer from minecraft client
        buffered_printer.print()

        # Wait for user input of actions
        args = input("{} Enter Command: ".format(controller_tag)).lower().split()
        if len(args) == 0 or args[0] not in commands:
            print("{} Invalid Choice: {!r} (choose from {})".format(controller_tag, " ".join(args), commands))
            continue

        # Parse actions and send to minecraft client
        if args[0] == "observe":
            args = iter(args[1:])
            action_list = []

            for action in args:
                if not action in ObservationManager.COMMAND_LIST.keys():
                    print("{} Invalid Observation Choice: {!r} (choose from {})".format(controller_tag, action, ObservationManager.COMMAND_LIST.keys()))
                    continue
                try:
                    obs = ObservationManager.COMMAND_LIST[action]
                    if callable(obs):
                        sig = signature(obs).parameters
                        action_list.append(obs(*[next(args) for _ in range(len(sig))]))
                    else:
                        action_list.append(obs)
                except StopIteration:
                    print("{} Observation argument required for {} ".format(controller_tag, action))
                    break

            data = manager.request_observation(*action_list)
            if data:

                sys.stdout.write(controller_tag + " ")
                pprint(data, max_seq_length=10)
        elif args[0] == "action":
            if len(args) <= 1:
                print("{} Required Actions (choose from {})".format(controller_tag, ActionManager.COMMAND_LIST.keys()))
                continue

            args = iter(args[1:])
            action_list = []

            for action in args:
                if not action in ActionManager.COMMAND_LIST.keys():
                    print("{} Invalid Action Choice: {!r} (choose from {})".format(controller_tag, action, ActionManager.COMMAND_LIST.keys()))
                    continue
                try:
                    act = ActionManager.COMMAND_LIST[action]
                    if callable(act):
                        sig = signature(act).parameters
                        action_list.append(act(*[next(args) for _ in range(len(sig))]))
                    else:
                        action_list.append(act)
                except StopIteration:
                    print("{} Action argument required for {} ".format(controller_tag, action))
                    break

            if len(action_list) > 0 and manager.send_actions(*action_list):
                print("{} Actions Sent".format(controller_tag))
        elif args[0] == "exit":
            manager.close_connection()
            break