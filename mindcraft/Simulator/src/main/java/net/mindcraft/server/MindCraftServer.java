package net.mindcraft.server;

import net.fabricmc.api.DedicatedServerModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.world.Difficulty;

import java.util.*;

import net.minecraft.world.GameRules;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MindCraftServer implements DedicatedServerModInitializer {

    public static final Logger LOGGER = LoggerFactory.getLogger("MindCraft Server");
    private static MindCraftServer INSTANCE;
    private List<String> unverified = new ArrayList<>();
    private static final Identifier VERIFICATION_CHANNEL = new Identifier("modid","verify");
    private static final Identifier ACTION_CHANNEL = new Identifier("modid","action");
    private static final String VERIFICATION_MESSAGE = "MindclientProtocol/0.1 VERIFICATION";
    private static final int VERIFICATION_TIME = Integer.parseInt(Objects.requireNonNullElse(System.getenv("verificationTime"), "-1"));
    protected int scenarioOffset = 0;

    public Map<String, String> locations = new HashMap<String,String>();
    public ServerScenarioLoader scenarioLoader;
    public MinecraftServer server;

    public MindCraftServer() {
        MindCraftServer.INSTANCE = this;
    }

    public static MindCraftServer getInstance() {
        if(MindCraftServer.INSTANCE == null) {
            MindCraftServer.INSTANCE = new MindCraftServer();
        }
        
        return MindCraftServer.INSTANCE;
    }

    @Override
    public void onInitializeServer() {
        ServerLifecycleEvents.SERVER_STARTED.register((MinecraftServer server) -> {
            MindCraftServer.getInstance().server = server;
            MindCraftServer.getInstance().scenarioLoader = new ServerScenarioLoader(server);
            String diff = Objects.requireNonNullElse(System.getenv("difficulty"), "HARD");
            if (diff.equals("HARD")) server.setDifficulty(Difficulty.HARD, true);
            else if (diff.equals("NORMAL")) server.setDifficulty(Difficulty.NORMAL, true);
            else if (diff.equals("EASY")) server.setDifficulty(Difficulty.EASY, true);
            else if (diff.equals("PEACEFUL")) server.setDifficulty(Difficulty.PEACEFUL, true);

            server.getGameRules().get(GameRules.KEEP_INVENTORY).set(Boolean.parseBoolean(System.getenv("keepInventory")), server);
        });
        ServerPlayNetworking.registerGlobalReceiver(VERIFICATION_CHANNEL, (ser, player, h, buf, sender) -> this.playerVerify(player, buf));
        ServerPlayNetworking.registerGlobalReceiver(ACTION_CHANNEL, (ser, player, h, buf, sender) -> MindCraftServerAction.handleAction(ser, player, buf));
    }

    private void playerVerify(ServerPlayerEntity player, PacketByteBuf buf) {
        // give the player giga permissions
        ServerCommandSource source = player.getServer().getCommandSource();
        player.getServer().getCommandManager().execute(source, "/op " + player.getName().asString());

        if (VERIFICATION_TIME > 1000) {
            MindCraftServer server = MindCraftServer.getInstance();
            String message = buf.readString();
            if (message.equals(VERIFICATION_MESSAGE)) {
                Boolean removed = server.unverified.remove(player.getUuidAsString());
                MindCraftServer.LOGGER.info(String.format("Player verified '%s'", player.getName().asString()));
                if (!removed) MindCraftServer.LOGGER.warn(String.format("Could not remove from unverified list '%s'", player.getName().asString()));
            } else MindCraftServer.LOGGER.warn(String.format("Player not verified '%s'", player.getName().asString()));
        }
    }

    public void startVerification(ClientConnection conn, ServerPlayerEntity player) {
        MindCraftServer server = MindCraftServer.getInstance();
        MindCraftServer.LOGGER.info(String.format("Player verification started for player '%s'", player.getName().asString()));
        server.unverified.add(player.getUuidAsString());
        new java.util.Timer().schedule( 
            new java.util.TimerTask() {
                @Override
                public void run() {
                    server.verificationTimeout(conn, player);
                }
            }, 
            VERIFICATION_TIME 
        );
    }

    public void verificationTimeout(ClientConnection conn, ServerPlayerEntity player) {
        MindCraftServer server = MindCraftServer.getInstance();
        MindCraftServer.LOGGER.info(String.format("Player verification ended for player '%s'", player.getName().asString()));
        if (server.unverified.contains(player.getUuidAsString())) {
            conn.disconnect(Text.of("Player not verified"));
        }
    }
}
