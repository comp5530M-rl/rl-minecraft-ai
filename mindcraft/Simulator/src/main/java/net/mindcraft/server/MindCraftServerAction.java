package net.mindcraft.server;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;

public class MindCraftServerAction {
    static public void handleAction(MinecraftServer server, ServerPlayerEntity player, PacketByteBuf buf) {
        String message = buf.readString();
        String[] words = message.split(" ");
        switch (words[0]){
            case "INIT":
                MindCraftServer.getInstance().scenarioLoader.loadScenario(words.length > 1 ? words[1] : "default", player.getUuidAsString());
                break;
            case "SWAP":
                if (words.length == 3) swapInventorySlots(player,Integer.parseInt(words[1]),Integer.parseInt(words[2]));
                else MindCraftServer.LOGGER.error(String.format("Required 2 parameters for SWAP command '%s'", message));
                break;
            case "DROP":
                if (words.length == 2) dropInventorySlot(player,Integer.parseInt(words[1]));
                else MindCraftServer.LOGGER.error(String.format("Required 1 parameters for DROP command '%s'", message));
                break;
            default:
                MindCraftServer.LOGGER.error(String.format("Undefined action provided '%s'", words[0]));
        }
    }

    static private void swapInventorySlots(ServerPlayerEntity player, int slot1, int slot2) {
        PlayerInventory inv = player.getInventory();
        ItemStack temp = inv.getStack(slot1);
        inv.setStack(slot1, inv.getStack(slot2));
        inv.setStack(slot2, temp);
        MindCraftServer.LOGGER.info(String.format("SWAPPED SLOTS '%s', '%s'", slot1, slot2));
    }

    static private void dropInventorySlot(ServerPlayerEntity player, int slot) {
        PlayerInventory inv = player.getInventory();
        ItemStack items = inv.removeStack(slot);
        player.dropItem(items, false, false);
        MindCraftServer.LOGGER.info(String.format("PLAYER DROPPED '%s'", slot));
    }
}
