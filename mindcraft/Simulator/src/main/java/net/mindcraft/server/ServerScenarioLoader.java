package net.mindcraft.server;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;
import java.util.UUID;

import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.mindcraft.loader.Mindclient;
import net.mindcraft.loader.Scenario;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.util.math.Direction.Axis;

public class ServerScenarioLoader {
    private Queue<String> scenario_queue = new LinkedList<String>();
    private HashMap<UUID,Vec3i> scenario_positions = new HashMap<UUID,Vec3i>();

    private static final Identifier SCENARIO_CHANNEL = new Identifier("modid","scenario");
    
    private Vec3i scenario_pos;
    protected final int scenario_buffer = Integer.parseInt(Objects.requireNonNullElse(System.getenv("scenarioSpacing"), "6"));
    private boolean is_running = true;
    public MinecraftServer server;
    private Thread runner;

    public ServerScenarioLoader(MinecraftServer server) {
        String[] pos = Objects.requireNonNullElse(System.getenv("scenarioPos"), "100,200,100").split(",");
        scenario_pos = new Vec3i(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]), Integer.parseInt(pos[2]));

        this.server = server;
        this.runner = new Thread(() -> {
            emptyQueue();
        }, "Server Scenario Loader");
        this.runner.start();
    }

    public void stop() {
        this.is_running = false;
    }

    public void threadCheck() {
        if (!this.runner.isAlive()) {
            this.runner = new Thread(() -> {
                emptyQueue();
            }, "Server Scenario Loader");
            this.is_running = true;
            this.runner.start();
        }
    }

    public Boolean serverCheck() {
        return this.server != null;
    }

    private void emptyQueue() {
        while (is_running) {
            if (scenario_queue.peek() == null || !this.serverCheck()) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {}
                continue;
            }
            String[] data = scenario_queue.poll().split(" ");

            Scenario scenario = new Scenario(data[0]);
            ServerPlayerEntity player = this.server.getPlayerManager().getPlayer(UUID.fromString(data[1]));

            PacketByteBuf buf = PacketByteBufs.create();
            buf.writeString(String.join(",", scenario.getRewardFunctions()));
            ServerPlayNetworking.send(player, SCENARIO_CHANNEL, buf);

            Vec3i pos = this.calculatePlacement(player, scenario);
            this.layBlocks(scenario, pos, player.getWorld());
            this.setInventory(scenario, player);
            this.setPlayerData(scenario, player);
            this.spawnEntities(scenario, pos, player.getWorld());
            this.teleportPlayer(scenario, pos, player);
        }
    }

    private void layBlocks(Scenario scenario, Vec3i pos, ServerWorld world) {
        Vec3i size = scenario.getAreaSize();
        BlockState[] blocks = scenario.getBlocks();

        for(int z = 0 ; z < size.getZ(); z++){
            for(int y = 0 ; y < size.getY(); y++){
                for(int x = 0 ; x < size.getX() ; x++){
                    world.setBlockState(new BlockPos(pos.add(x, y, z)), blocks[scenario.getIndex(x, y, z)]);
                }
            }
        }
    }

    private void setInventory(Scenario scenario, ServerPlayerEntity player) {
        player.getInventory().clear();
        HashMap<Byte, ItemStack> inventory = scenario.getInventory();
        for (byte slot : inventory.keySet()) {
            player.getInventory().setStack(slot, inventory.get(slot));
        }
    }

    private void setPlayerData(Scenario scenario, ServerPlayerEntity player) {
        player.setHealth(scenario.getPlayerHealth());
        player.getHungerManager().setFoodLevel(scenario.getHunger());
        player.getHungerManager().setSaturationLevel(scenario.getSaturation());
    }

    private void spawnEntities(Scenario scenario, Vec3i pos, ServerWorld world) {
        world.getEntitiesByClass(Entity.class, new Box(Vec3d.of(pos), Vec3d.of(pos).add(Vec3d.of(scenario.getAreaSize()))).expand(1), EntityPredicates.VALID_ENTITY).stream().forEach(entity -> {
            if (!entity.isPlayer()) entity.discard();
        });
        Entity[] entities = scenario.getEntities(world);
        for (Entity entity : entities) {
            entity.setPosition(entity.getPos().add(Vec3d.of(pos)));
            world.spawnEntity(entity);
        }
    }

    private void teleportPlayer(Scenario scenario, Vec3i pos, ServerPlayerEntity player) {
        Vec3d spawnpoint = scenario.getSpawnOffsets().add(Vec3d.of(pos));
        if (spawnpoint.getY() > -64) {
            player.teleport(spawnpoint.getX(), spawnpoint.getY(), spawnpoint.getZ());
        }
    }

    private Vec3i calculatePlacement(ServerPlayerEntity player, Scenario scenario) {
        if (this.scenario_positions.containsKey(player.getUuid())) {
            return this.scenario_positions.get(player.getUuid());
        } 
        Vec3i size = scenario.getAreaSize();
        Vec3i pos = findFirstBlockUnder(this.scenario_pos, player.getWorld());
        this.scenario_pos = this.scenario_pos.offset(Axis.X, size.getX() + this.scenario_buffer);

        this.scenario_positions.put(player.getUuid(), pos);
        return pos;
    }

    private Vec3i findFirstBlockUnder(Vec3i pos, ServerWorld world) {
        Vec3i newPos = new Vec3i(pos.getX(), pos.getY(), pos.getZ());
        while (!world.getBlockState(new BlockPos(newPos.down())).isOpaque()) {
            newPos = newPos.down();
            if (newPos.getY() < -64) {
                return pos;
            }
        }
        return newPos;
    }

    public void loadScenario(String scenario_name, String player_id) {
        scenario_queue.offer(scenario_name + " " + player_id);
        this.threadCheck();
    }
    
}
