package net.mindcraft.loader;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.command.v1.ClientCommandManager;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientLifecycleEvents;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents;
import net.mindcraft.mixin.MinecraftClientMixin;
import net.mindcraft.mixin.RenderTickCounterMixin;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.*;
import net.minecraft.text.LiteralText;
import net.minecraft.text.MutableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;

import java.util.Objects;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mojang.brigadier.arguments.FloatArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;

public class Mindclient implements ClientModInitializer {
    // This logger is used to write text to the console and the log file.
    // It is considered best practice to use your mod id as the logger's name.
    // That way, it's clear which mod wrote info, warnings, and errors.
    private static final Identifier SCENARIO_CHANNEL = new Identifier("modid","scenario");
    protected static String reward_string = "";
    
    public static final Logger LOGGER = LoggerFactory.getLogger("MindCraft Client");
    public static ClientScenarioLoader scenarioLoader;
    private static final int[] SCREEN_SIZE = Stream.of(Objects.requireNonNullElse(System.getenv("screen_size"), "480,480").split(",")).mapToInt(Integer::parseInt).toArray(); 
    private static Boolean screenLock = Boolean.parseBoolean(Objects.requireNonNullElse(System.getenv("screen_locked"), "False").toLowerCase());

	@Override
	public void onInitializeClient() {
        scenarioLoader = new ClientScenarioLoader(MinecraftClient.getInstance());

        ClientPlayNetworking.registerGlobalReceiver(SCENARIO_CHANNEL, (client, networkHandler, buf, packetSender) -> {
            Mindclient.reward_string = buf.readString();
        });

        ClientPlayConnectionEvents.JOIN.register((cpnh, ps, client) -> {
            MindclientAction.camera_x = client.player.getYaw();
            MindclientAction.camera_y = client.player.getPitch();
        });

        ClientPlayConnectionEvents.DISCONNECT.register((handle, client) -> {
            Mindclient.scenarioLoader.stop();
        });

        ClientLifecycleEvents.CLIENT_STARTED.register(client -> {
            // set to minimum video settings
            // graphics fast, smooth lighting minimum, render distance 8,
            // clouds off, entity shadows off, particles decreased, mipmaps off,
            // biome blend off

            Boolean performanceMode = Boolean.parseBoolean(Objects.requireNonNullElse(System.getenv("graphics_performance_mode"), "True").toLowerCase());

            client.options.graphicsMode = performanceMode ? GraphicsMode.FAST : GraphicsMode.FANCY;
            client.options.ao = performanceMode ? AoMode.MIN : AoMode.MAX;
            client.options.maxFps = performanceMode ? 30 : 160;
            client.options.enableVsync = performanceMode ? false : true;
            client.options.bobView = performanceMode ? false : true;
            client.options.gamma = 100;
            client.options.entityDistanceScaling = performanceMode ? 0.75f : 2;
            client.options.simulationDistance = 8;
            client.options.viewDistance = 8;
            client.options.cloudRenderMode =  performanceMode ? CloudRenderMode.OFF : CloudRenderMode.FANCY;
            client.options.entityShadows = performanceMode ? false : true;
            client.options.particles = performanceMode ? ParticlesMode.MINIMAL : ParticlesMode.ALL;
            client.options.mipmapLevels = performanceMode ? 0 : 4;
            client.options.biomeBlendRadius = performanceMode ? 0 : 2;
            client.options.hudHidden = Boolean.parseBoolean(Objects.requireNonNullElse(System.getenv("hide_hud"), "True").toLowerCase());
            client.options.fullscreen = false;
            client.options.pauseOnLostFocus = false;

            client.options.write();
            client.getWindow().applyVideoMode();


            // use 480x480 window size
            client.getWindow().setWindowedSize(SCREEN_SIZE[0], SCREEN_SIZE[1]);
        });

        ClientTickEvents.START_CLIENT_TICK.register(client -> {
            if (screenLock && !(client.getWindow().getWidth() == SCREEN_SIZE[0] && client.getWindow().getHeight() == SCREEN_SIZE[1])) {
                client.getWindow().setWindowedSize(SCREEN_SIZE[0], SCREEN_SIZE[1]);
                    if (client.getWindow().isFullscreen()) client.getWindow().toggleFullscreen();
            }
        });

        ClientCommandManager.DISPATCHER.register(ClientCommandManager.literal("lock_screen").executes(
                context -> {
                    Mindclient.screenLock = true;
                    return 1;
                }
            )
        );

        ClientCommandManager.DISPATCHER.register(ClientCommandManager.literal("unlock_screen").executes(
                context -> {
                    Mindclient.screenLock = false;
                    return 1;
                }
            )
        );

        ClientCommandManager.DISPATCHER.register(ClientCommandManager.literal("lock_camera")
            .then(ClientCommandManager.argument("speed", FloatArgumentType.floatArg(0, 100))
            .executes((c) -> {
                MindclientAction.camera_speed = FloatArgumentType.getFloat(c, "speed");
                return 1;
            }))
        .executes(
            c -> {
                MindclientAction.camera_speed = 50f;
                return 1;
            }
        ));

        ClientCommandManager.DISPATCHER.register(ClientCommandManager.literal("unlock_camera").executes(
                context -> {
                    MindclientAction.camera_speed = 0f;
                    return 1;
                }
            )
        );

        ClientCommandManager.DISPATCHER.register(ClientCommandManager.literal("debug_action")
                .then(ClientCommandManager.argument("action", StringArgumentType.greedyString())
                .executes(
                    context -> {
                        MinecraftClient client = context.getSource().getClient();
                        assert client.player != null;

                        String action = StringArgumentType.getString(context, "action");
                        MindclientAction act = MindclientAction.getInstance();
                        act.handleAction(action);
                        return 1;
                    }
            ))
        );

        ClientCommandManager.DISPATCHER.register(ClientCommandManager.literal("movecamera")
            .then(ClientCommandManager.argument("horizontal", FloatArgumentType.floatArg(-180, 180))
                .then(ClientCommandManager.argument("vertical", FloatArgumentType.floatArg(-90, 90))
                .executes(context -> {
                    MinecraftClient client = context.getSource().getClient();
                    assert client.player != null;
                    float h = FloatArgumentType.getFloat(context, "horizontal");
                    float v = FloatArgumentType.getFloat(context, "vertical");
                    MindclientAction act = MindclientAction.getInstance();
                    act.setRelativeCameraAngle(h, v);
                    return 1;
                }))
            )
        );

        ClientCommandManager.DISPATCHER.register(ClientCommandManager.literal("scenario")
            .then(ClientCommandManager.argument("scenario_name", StringArgumentType.greedyString())
            .executes(context -> {
                MinecraftClient client = context.getSource().getClient();
                assert client.player != null;
                String name = StringArgumentType.getString(context, "scenario_name");
                MindclientAction act = MindclientAction.getInstance();
                act.init(name);;
                return 1;
            }))
        );

        MutableText errorText = new LiteralText("Unknown action or incomplete command").formatted(Formatting.RED);
        ClientCommandManager.DISPATCHER.register(ClientCommandManager.literal("action")
            .then(ClientCommandManager.argument("action_name", StringArgumentType.greedyString())
                .then(ClientCommandManager.argument("action_param_1", StringArgumentType.greedyString())
                    .then(ClientCommandManager.argument("action_param_2", StringArgumentType.greedyString())
                    .executes(context -> {
                        MinecraftClient client = context.getSource().getClient();
                        assert client.player != null;
                        String cmd = StringArgumentType.getString(context, "action_name").toUpperCase() + StringArgumentType.getString(context, "action_param_1") + StringArgumentType.getString(context, "action_param_2");
                        MindclientAction act = MindclientAction.getInstance();
                        if (!act.handleAction(cmd)) context.getSource().sendError(errorText);
                        return 1;
                    }))
                .executes(context -> {
                    MinecraftClient client = context.getSource().getClient();
                    assert client.player != null;
                    String cmd = StringArgumentType.getString(context, "action_name").toUpperCase() + StringArgumentType.getString(context, "action_param_1");
                    MindclientAction act = MindclientAction.getInstance();
                    if (!act.handleAction(cmd)) context.getSource().sendError(errorText);
                    return 1;
                }))
            .executes(context -> {
                MinecraftClient client = context.getSource().getClient();
                assert client.player != null;
                String cmd = StringArgumentType.getString(context, "action_name").toUpperCase();
                MindclientAction act = MindclientAction.getInstance();
                if (!act.handleAction(cmd)) context.getSource().sendError(errorText);
                return 1;
            }))
        );

        LOGGER.info("Commands Loaded");
    }

    public static void updateTPS() {
        RenderTickCounterMixin rtc = null;
        MinecraftClientMixin client = (MinecraftClientMixin) MinecraftClient.getInstance();
        rtc = (RenderTickCounterMixin) client.getRenderTickCounter();
        //ticks per second(set to 40) WARNING: IF YOU CHANGE THIS YOU MUST CHANGE THE OVERCLOCKSERVER MIXIN TO MATCH
        float tps = 20.0F;
        rtc.setTickTime(1000f / tps);

    }
}
