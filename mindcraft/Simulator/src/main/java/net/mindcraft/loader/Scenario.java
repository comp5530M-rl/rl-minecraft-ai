package net.mindcraft.loader;

import net.minecraft.block.BlockState;
import net.minecraft.command.argument.BlockArgumentParser;
import net.minecraft.command.argument.ItemStackArgument;
import net.minecraft.command.argument.ItemStringReader;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;

import net.minecraft.nbt.NbtHelper;
import net.minecraft.nbt.NbtList;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.util.registry.Registry;
import net.querz.nbt.io.NBTUtil;
import net.querz.nbt.io.SNBTUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

public class Scenario {

    public NbtCompound allData;
    public String name;

    private final String NOT_EXIST_SCENARIO = "default";
    private final String SCENARIO_FORMAT = "../scenarios/%s.scenario";
    private final Vec3d ENTITY_OFFSET = new Vec3d(.5, .5, .5);

    public static final Logger LOGGER = LoggerFactory.getLogger("Scenario");

    public Scenario(String name){
        try {
            try{
                allData = readScenarioFile(name);
                LOGGER.info(String.format("Loaded Scenario '%s'", name));
            } catch (FileNotFoundException e) {
                LOGGER.error(String.format("Scenario with name '%s' does not exist", name));
                allData = readScenarioFile(NOT_EXIST_SCENARIO);
                LOGGER.info(String.format("Loaded Scenario '%s'", NOT_EXIST_SCENARIO));
            }
        } catch (Exception e) {
            LOGGER.error(String.format("An error occurred when loading scenario: '%s' at %s:%s", e.getLocalizedMessage(), e.getStackTrace()[0].getFileName(), e.getStackTrace()[0].getLineNumber()));
        }
    }

    private NbtCompound readScenarioFile(String name) throws CommandSyntaxException, IOException {
        String nbt = SNBTUtil.toSNBT(NBTUtil.read(String.format(SCENARIO_FORMAT, name)).getTag());
        nbt = nbt.replaceFirst("BlockData:\\[B;", "BlockData:[");
        return NbtHelper.fromNbtProviderString(nbt);
    }


    public Vec3i getAreaSize(){
        LOGGER.debug("Reading Scenario Area Size");
        return new Vec3i(
            allData.getInt("Width"),
            allData.getInt("Height"),
            allData.getInt("Length")
        );
    }

    public int getIndex(int x, int y, int z) {
        Vec3i area = this.getAreaSize();
        return x + z * area.getX() + y * area.getX() * area.getZ();
    }

    public BlockState[] getBlocks(){
        LOGGER.debug("Reading Scenario Block List");
        String[] palette = new String[allData.getInt("PaletteMax")];

        for (String block : allData.getCompound("Palette").getKeys()) {
            palette[allData.getCompound("Palette").getInt(block)] = block;
        }

        HashMap<Integer,String> entities = new HashMap<Integer,String>();
        if (allData.contains("BlockEntity")) {
            allData.getList("BlockEntity", NbtElement.COMPOUND_TYPE).forEach((be) -> {
                NbtCompound blockE = (NbtCompound) be;
                NbtList pos = blockE.getList("Pos", NbtElement.INT_TYPE);
                blockE.remove("Pos");
                blockE.remove("Id");
                entities.put(this.getIndex(pos.getInt(0), pos.getInt(1), pos.getInt(2)), blockE.asString());
            });
        }

        NbtList blockData = allData.getList("BlockData", NbtElement.INT_TYPE);
        BlockState[] blocks = new BlockState[blockData.size()];
        for (int index = 0; index < blockData.size(); index++) {
            String bs = palette[blockData.getInt(index)];
            if (entities.containsKey(index)) bs += " " + entities.get(index);
            BlockArgumentParser b = new BlockArgumentParser(new StringReader(bs), true);
            try {
                b.parse(true);
                LOGGER.debug(String.format("Scenario Block: %s", bs));
            } catch (CommandSyntaxException e) {
                LOGGER.error(String.format("Invalid Block: %s", bs));
            } 
            blocks[index] = b.getBlockState();
        }

        return blocks;
    }

    public Vec3d getSpawnOffsets(){
        LOGGER.debug("Reading Scenario Spawn Point");
        int[] pos = allData.getCompound("Metadata").getCompound("PlayerDat").getIntArray("Pos");
        return new Vec3d(pos[0],pos[1],pos[2]).add(ENTITY_OFFSET);
    }

    public int getHunger(){
        LOGGER.debug("Reading Scenario Hunger");
        return allData.getCompound("Metadata").getCompound("PlayerDat").getInt("foodLevel");
    }

    public int getSaturation(){
        LOGGER.debug("Reading Scenario Saturation");
        return allData.getCompound("Metadata").getCompound("PlayerDat").getInt("foodSaturationLevel");
    }

    public float getPlayerHealth(){
        LOGGER.debug("Reading Scenario Health");
        return allData.getCompound("Metadata").getCompound("PlayerDat").getFloat("Health");
    }

    public String[] getRewardFunctions(){
        LOGGER.debug("Reading Scenario Rewards");
        NbtList rewards = allData.getCompound("Metadata").getList("RewardFunctions", NbtElement.STRING_TYPE);
        String[] reward_list = new String[rewards.size()];
        for (int index = 0; index < rewards.size(); index++) {
            reward_list[index] = rewards.getString(index);
        }
        return reward_list;
    }

    public HashMap<Byte, ItemStack> getInventory(){
        LOGGER.debug("Reading Scenario Inventory");
        HashMap<Byte, ItemStack> inv = new HashMap<Byte,ItemStack>();
        allData.getCompound("Metadata").getCompound("PlayerDat").getList("Inventory",NbtElement.COMPOUND_TYPE).forEach(slot -> {
            NbtCompound sc = (NbtCompound) slot;
            byte slotNum = sc.getByte("Slot");
            byte slotCount = sc.getByte("Count");
            String id = sc.getString("id");
            sc.remove("Slot");
            sc.remove("Count");
            sc.remove("id");
            String item = id;
            if (sc.getSize() > 0) {
                item += sc.asString();
            }
            try {
                ItemStringReader itemStringReader = new ItemStringReader(new StringReader(item), true).consume();
                inv.put(slotNum,new ItemStackArgument(itemStringReader.getItem(), itemStringReader.getNbt()).createStack(slotCount, true));
                LOGGER.debug(String.format("Scenario Item: %s", item));
            } catch (CommandSyntaxException e) {
                LOGGER.warn(String.format("Invalid Item: %s", item));
            }
        });

        return inv;
    }

    public Entity[] getEntities(ServerWorld world){
        LOGGER.debug("Reading Scenario Entities");
        if (!allData.contains("Entities")) return new Entity[0];
        NbtList entityData = allData.getList("Entities", NbtElement.COMPOUND_TYPE);
        Entity[] entities = new Entity[entityData.size()];
        for (int index = 0; index < entityData.size(); index++) {
            NbtCompound ec = entityData.getCompound(index);
            LOGGER.debug(String.format("Scenario Entity: %s", ec.asString()));
            Entity entity = Registry.ENTITY_TYPE.get(new Identifier(ec.getString("Id"))).create(world);
            ec.remove("Id");
            entity.readNbt(ec);
            entity.setPosition(entity.getPos().add(ENTITY_OFFSET));
            entities[index] = entity;
        }

        return entities;
    }

}
