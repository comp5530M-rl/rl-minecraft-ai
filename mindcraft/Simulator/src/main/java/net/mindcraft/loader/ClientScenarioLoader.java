package net.mindcraft.loader;

import net.mindcraft.server.ServerScenarioLoader;
import net.minecraft.client.MinecraftClient;

public class ClientScenarioLoader extends ServerScenarioLoader {
    private MinecraftClient client;

    ClientScenarioLoader(MinecraftClient client) {
        super(client.getServer());
        this.client = client;
    }

    @Override
    public Boolean serverCheck() {
        this.server = client.getServer();

        if (this.server == null) {
            Mindclient.LOGGER.error("Cannot load client side scenario where client server is not running");
            this.stop();
            return false;
        }
        return true;
    } 
}
