package net.mindcraft.loader;

import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.texture.NativeImage;
import net.minecraft.client.util.ScreenshotRecorder;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import com.google.gson.Gson;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class MindclientObservation {
    private static MindclientObservation INSTANCE;

    public InventorySlot[] inventory;
    public List<InventoryChange> inventory_changes;
    public String[][][] blocks;
    public List<BlockChange> block_changes;
    public EntityData[] entities;
    public PlayerData playerData;
    public byte[] screenBytes;
    public int screenWidth;
    public int screenHeight;
    public String[] reward_functions;

    private World world;
    private PlayerEntity player;
    private MinecraftClient client;
    private Boolean[] exports = new Boolean[]{false, false, false, false, false, false, false}; // inventory, blocks, entities, playerDat, pixels, changesOnly, scenario
    private final Gson gson = new Gson();
    private static Boolean waiting = false;


    public static MindclientObservation getInstance(World world, PlayerEntity player) {
        if(MindclientObservation.INSTANCE == null) {
            MindclientObservation.INSTANCE = new MindclientObservation(world, player);
            MindclientObservation.INSTANCE.client = MinecraftClient.getInstance();
        }
        MindclientObservation.INSTANCE.exports = new Boolean[]{false, false, false, false, false, false, false};
        MindclientObservation.INSTANCE.player = player;
        MindclientObservation.INSTANCE.world = world;
        return MindclientObservation.INSTANCE;
    }

    private MindclientObservation(World world, PlayerEntity player) {
        this.inventory = new InventorySlot[player.getInventory().size()];
        this.blocks = new String[16][16][16];
        MindclientObservation.INSTANCE = this;
    }

    public void handleObservation(String action){
        String[] words = action.split(" ");
        switch (words[0]){
            case "INVENTORY":
                if (words.length <= 1) this.inventoryGet();
                else {
                    Mindclient.LOGGER.error(String.format("Required 0 parameters for INVENTORY command '%s'", action));
                }
                break;
            case "CHANGES":
                if (words.length <= 1) this.exports[5] = true;
                else {
                    Mindclient.LOGGER.error(String.format("Required 0 parameters for CHANGES command '%s'", action));
                }
                break;
            case "BLOCKS":
                if (words.length <= 2) this.blocksGet(words.length > 1 ? Integer.parseInt(words[1]) : 16);
                else {
                    Mindclient.LOGGER.error(String.format("Required 0 or 1 parameter for BLOCKS command '%s'", action));
                }
                break;
            case "ENTITIES":
                if (words.length <= 2) this.entitiesGet(words.length > 1 ? Integer.parseInt(words[1]) : 16);
                else {
                    Mindclient.LOGGER.error(String.format("Required 0 or 1 parameter for ENTITIES command '%s'", action));
                }
                break;
            case "PLAYER_DAT":
                if (words.length <= 1) this.playerDataGet();
                else {
                    Mindclient.LOGGER.error(String.format("Required 0 parameters for PLAYER_DATA command '%s'", action));
                }
                break;
            case "PIXELS":
                if (words.length <= 1) this.framebufferGet();
                else {
                    Mindclient.LOGGER.error(String.format("Required 0 parameters for PIXELS command '%s'", action));
                }
                break;
            case "SCENARIO":
                if (words.length <= 1) this.scenarioGet();
                else {
                    Mindclient.LOGGER.error(String.format("Required 0 parameters for SCENARIO command '%s'", action));
                }
                break;
            case "ALL":
                if (words.length <= 1) {
                    this.playerDataGet();
                    this.entitiesGet(16);
                    this.blocksGet(16);
                    this.inventoryGet();
                    this.framebufferGet();
                }
                else {
                    Mindclient.LOGGER.error(String.format("Required 0 parameters for PLAYER_DATA command '%s'", action));
                }
                break;
            default:
                Mindclient.LOGGER.error(String.format("Undefined observation provided '%s'", words[0]));
        }
    }

    public void framebufferGet() {
        this.exports[4] = true;
        waiting = true;
        MinecraftClient.getInstance().execute(() -> {
            ByteBuffer pixBuf = BufferUtils.createByteBuffer(this.client.getFramebuffer().textureWidth * this.client.getFramebuffer().textureHeight * 4);
            GL11.glReadPixels(0, 0, this.client.getFramebuffer().textureWidth, this.client.getFramebuffer().textureHeight, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, pixBuf);
            screenBytes = new byte[this.client.getFramebuffer().textureWidth * this.client.getFramebuffer().textureHeight * 4];
            pixBuf.get(screenBytes);
            screenWidth = this.client.getFramebuffer().textureWidth;
            screenHeight = this.client.getFramebuffer().textureHeight;
            
            MindclientObservation.waiting = false;
        });
        while (waiting) {
            try {
                Thread.sleep(7);
            } catch (InterruptedException e) {
                continue;
            }
        }
    }

    public void inventoryGet() {
        this.exports[0] = true;
        this.inventory_changes = new ArrayList<>();
        PlayerInventory playerInv = this.player.getInventory();
        for (int i = 0; i < this.player.getInventory().size(); i++) {
            ItemStack stack = playerInv.getStack(i);
            InventorySlot slot = new InventorySlot(Registry.ITEM.getId(stack.getItem()).toString(), stack.getDamage(), stack.getCount(), playerInv.selectedSlot == i);
            if (this.inventory[i] == null || !slot.itemName.equals(this.inventory[i].itemName) || slot.stackSize != this.inventory[i].stackSize) {
                this.inventory_changes.add(new InventoryChange(i, slot));
            }
            this.inventory[i] = slot;
        }
    }

    public void scenarioGet() {
        this.exports[6] = true;
        this.reward_functions = Mindclient.reward_string.length() > 0?Mindclient.reward_string.split(","):new String[0];
    }

    public void blocksGet(int size) {
        this.exports[1] = true;
        this.block_changes = new ArrayList<>();
        int length = (size*2) + 1;
        if (this.blocks.length != length)
            this.blocks = new String[length][length][length];
        for (int x = -size; x <= size; x++) {
            for (int y = -size; y <= size; y++) {
                for (int z = -size; z <= size; z++) {
                    BlockState blockState = this.world.getBlockState(new BlockPos(this.player.getPos().add(x, y, z)));
                    String block = Registry.BLOCK.getId(blockState.getBlock()).toString();
                    if (this.blocks[x+size][y+size][z+size] == null || !block.equals(blocks[x+size][y+size][z+size])) {
                        block_changes.add(new BlockChange(x, y, z, block));
                    }
                    blocks[x+size][y+size][z+size] = block;
                }
            }
        }
    }

    public void entitiesGet(int size) {
        this.exports[2] = true;
        this.entities = this.world.getEntitiesByClass(Entity.class, this.player.getBoundingBox().expand(size), EntityPredicates.VALID_ENTITY).stream().filter(entity -> entity != this.player.getRootVehicle()).map(
            entity -> new EntityData(Registry.ENTITY_TYPE.getId(entity.getType()).toString(), entity.getPos(), entity.getVelocity(), entity.getRotationVector())
        ).toArray(EntityData[]::new);
    }

    public void playerDataGet() {
        this.exports[3] = true;
        this.playerData = new PlayerData(
            this.player.getPos(),
            this.player.getVelocity(),
            this.player.getRotationVector(),
            this.player.isOnGround(),
            this.player.isSneaking(),
            this.player.getHealth(),
            this.player.getHungerManager().getSaturationLevel(),
            this.player.getArmor(),
            this.player.getHungerManager().getFoodLevel(),
            this.player.totalExperience,
            this.player.getAir()
        );
    }

    public String observe() {
        if (!(this.exports[0] || this.exports[1] || this.exports[2] || this.exports[3] || this.exports[4] || this.exports[6])) this.handleObservation("ALL");
        if (exports[5]) {
            return
                    gson.toJson(
                        new ChangesOnlyData(
                            this.exports[0]?this.inventory_changes:null,
                            this.exports[1]?this.block_changes:null,
                            this.exports[2]?this.entities:null,
                            this.exports[3]?this.playerData:null,
                            this.exports[4]?this.screenBytes:null,
                            this.exports[4]?this.screenWidth:null,
                            this.exports[4]?this.screenHeight:null,
                            this.exports[6]?this.reward_functions:null
                        )
                    )
                    + "\n";
        }
        return
                gson.toJson(
                    new AllData(
                        this.exports[0]?this.inventory:null,
                        this.exports[1]?this.blocks:null,
                        this.exports[2]?this.entities:null,
                        this.exports[3]?this.playerData:null,
                        this.exports[4]?this.screenBytes:null,
                        this.exports[4]?this.screenWidth:null,
                        this.exports[4]?this.screenHeight:null,
                        this.exports[6]?this.reward_functions:null
                    )
                )
                + "\n";
    }

    public record InventorySlot(String itemName, int itemDamage, int stackSize,
                                boolean isActive) {
    }

    public record InventoryChange(int i, InventorySlot slot) {
    }

    public record BlockChange(int x, int y, int z, String block) {
    }

    public record EntityData(String entityId, Vec3d position, Vec3d velocity, Vec3d lookVector) {

    }

    public record PlayerData(Vec3d position, Vec3d velocity,
                             Vec3d lookVector, boolean isOnGround, boolean isSneaking, float health,
                             float saturation, int armorLevel, int hunger, int xp, int breath) {
    }


    public record ChangesOnlyData(@Nullable List<InventoryChange> inventory_changes,
                                  @Nullable List<BlockChange> block_changes,
                                  @Nullable EntityData[] entities,
                                  @Nullable PlayerData playerData,
                                  @Nullable byte[] screenBytes,
                                  @Nullable Integer screenWidth,
                                  @Nullable Integer screenHeight,
                                  @Nullable String[] reward_functions) {
    }

    public record AllData(@Nullable InventorySlot[] inventory,
                          @Nullable String[][][] blocks,
                          @Nullable EntityData[] entities,
                          @Nullable PlayerData playerData,
                          @Nullable byte[] screenBytes,
                          @Nullable Integer screenWidth,
                          @Nullable Integer screenHeight,
                          @Nullable String[] reward_functions) {
    }
}
