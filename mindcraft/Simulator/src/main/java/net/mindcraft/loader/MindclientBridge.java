package net.mindcraft.loader;

import net.fabricmc.api.ClientModInitializer;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MindclientBridge implements ClientModInitializer {
    private ServerSocket sock;

    protected static final String VERIFICATION = "MindclientProtocol/0.1 VERIFICATION";
    private static final String CONFIRM_VERIFICATION = "MindclientProtocol/0.1 CONFIRM VERIFICATION";
    private static final String OBSERVATION = "MindclientProtocol/0.1 REQUEST OBSERVATION";
    private static final String ACTION = "MindclientProtocol/0.1 REQUEST ACTION";
    private static final String CONNECTION = "MindclientProtocol/0.1 REQUEST CONNECTION";
    private static final String CONFIRM_ACTION = "MindclientProtocol/0.1 CONFIRM ACTION";
    private static final String END_REQUEST = "MindclientProtocol/0.1 END_REQUEST";
    private static final int PORT = Integer.parseInt(Objects.requireNonNullElse(System.getenv("port"), "42000"));

    public static final Logger LOGGER = LoggerFactory.getLogger("MindCraft Bridge");

    @Override
    public void onInitializeClient() {
        boolean socketOpen = startServerSocket(PORT);
        
        if (!socketOpen) {
            LOGGER.error("Unable to start Bridge Socket!");
        } else {
            listenForClients();
        }
    }

    private boolean startServerSocket(int port) {
        LOGGER.info(String.format("Initialising Bridge Socket on port [%d]", port));
        try {
            sock = new ServerSocket(port);
            sock.setSoTimeout(10000);
            return true;
        } catch (IOException e) {
            LOGGER.warn(String.format("Unable to start Bridge Socket on port [%d] '%s'", port, e.getMessage()));
            return false;
        }
    }

    private void listenForClients() {
        new Thread(() -> {
            int clientNum = 1;
            while (!sock.isClosed()) {
                try {
                    Socket client = sock.accept();
                    new Thread(() -> {
                        if (verifyClient(client)) {
                            try {
                                handleClient(client);
                            } catch (InterruptedException e) {
                                LOGGER.error(String.format("Client Socket Thread was interrupted '%s'", e.getMessage()));
                            }
                        }
                        try {
                            client.close();
                        } catch (IOException e) {
                            LOGGER.error(String.format("Unable to close client socket '%s'", e.getMessage()));
                        }
                    }, String.format("Client Socket %d",clientNum)).start();
                    clientNum++;
                } catch (SocketTimeoutException e) {
                    LOGGER.info("Waiting for clients on port: " + String.valueOf(PORT));
                } catch (IOException e) {
                    LOGGER.warn("Could not accept client connection: " + e.getMessage());
                }
            }
            LOGGER.warn("Socket Listener Closed - Trying Restart of Socket");
            onInitializeClient();
        }, "Socket Listener").start();
    }

    private boolean verifyClient(Socket client) {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            OutputStream out = client.getOutputStream();
            try {
                client.setSoTimeout(30000);
                String line = in.readLine();
                if (line != null && line.equals(VERIFICATION)) {
                    out.write((CONFIRM_VERIFICATION+"\n").getBytes());
                    out.flush();
                    return true;
                }
                LOGGER.warn("Client did not provide valid verification");
                return false;
            } catch (SocketTimeoutException e) {
                LOGGER.warn("Client did not provide verification within 30 seconds");
                return false;
            }
        } catch (IOException e) {
            LOGGER.error(String.format("Unable to read client input stream for verification '%s'", e.getMessage()));
            return false;
        }
    }

    private void handleClient(Socket client) throws InterruptedException {
        try {
            // Get input / output stream of socket
            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            OutputStream out = client.getOutputStream();
            int timeouts = 0;
            client.setSoTimeout(1000);

            // Keeps socket open for a maximum of 10 minutes without a request
            while (!client.isClosed() && timeouts < 600) {
                // Check if player is in a world
                MinecraftClient instance = MinecraftClient.getInstance();
                World world = instance.world;
                PlayerEntity player = instance.player;
                Boolean allowAction = !(world == null || player == null);

                // Read socket REQUEST
                String request;
                try {
                    request = in.readLine();
                } catch (SocketTimeoutException e) {
                    if (timeouts % 30 == 0) LOGGER.info("No request received in the last 30 seconds...");
                    timeouts++;
                    continue;
                }

                if (request == null) break;

                // Process request
                switch (request) {
                    case CONNECTION -> {
                        LOGGER.info("Performing Action: RECONNECT");
                        MindclientAutoloader.reconnect(instance);
                    }
                    case OBSERVATION -> {
                        if (!allowAction) {
                            LOGGER.warn("World/PlayerEntity not ready.");
                            continue;
                        }
                        MindclientObservation obs = MindclientObservation.getInstance(world, player);
                        String chunk = in.readLine();
                        while (!chunk.toString().equals(END_REQUEST) && chunk !=null){
                            LOGGER.info("Observation Request: "+chunk.toString());
                            obs.handleObservation(chunk);
                            chunk = in.readLine();
                        }
                        LOGGER.info("Requesting Observation");
                        out.write(obs.observe().getBytes());
                        out.flush();
                    }
                    case ACTION -> {
                        if (!allowAction) {
                            LOGGER.warn("World/PlayerEntity not ready.");
                            continue;
                        }
                        MindclientAction act = MindclientAction.getInstance();
                        act.refreshKeypresses();
                        String chunk = in.readLine();
                        while (!chunk.toString().equals(END_REQUEST) && chunk !=null){
                            LOGGER.info("Performing Action: "+chunk.toString());
                            act.handleAction(chunk);
                            chunk = in.readLine();
                        }
                        act.unpressKeys();
                        out.write((CONFIRM_ACTION+"\n").getBytes());
                        out.flush();
                    }
                    case "ping" -> {
                        out.write("ping\n".getBytes());
                        out.flush();
                    }
                }

                timeouts = 0;
            }
            LOGGER.info("Client Disconnected.");
            out.close();
        } catch (IOException e) {
            LOGGER.error(String.format("Unable to read client input / output stream '%s'", e.getMessage()));
        }
    }
}
