package net.mindcraft.loader;

import java.util.List;
import java.util.Objects;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientLifecycleEvents;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientLifecycleEvents.ClientStarted;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents.Join;
import net.minecraft.client.gui.screen.TitleScreen;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerScreen;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.ServerAddress;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ConnectScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ScreenTexts;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.world.level.storage.LevelStorageException;
import net.minecraft.world.level.storage.LevelSummary;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PacketSender;

class onStart implements ClientStarted {
    public void onClientStarted(MinecraftClient client) {
        if (MindclientAutoloader.AUTOCONNECT_TO_SERVER) MindclientAutoloader.connectToServer();
        else MindclientAutoloader.loadWorld();
    }
}

class onConnect implements Join {
    private static final Identifier VERIFICATION_CHANNEL = new Identifier("modid","verify");

    public void onPlayReady(ClientPlayNetworkHandler cpnh, PacketSender ps, MinecraftClient client) {
        if (client.isInSingleplayer()) Mindclient.LOGGER.info("[AUTOLOAD] Connected to singleplayer world");
        else Mindclient.LOGGER.info("[AUTOLOAD] Connected to multiplayer world");

        Mindclient.LOGGER.info("[SERVER VERIFY] Sending verification to server");
        PacketByteBuf packet = PacketByteBufs.create();
        packet.writeString(MindclientBridge.VERIFICATION);
        ps.sendPacket(VERIFICATION_CHANNEL, packet);
    }
}


public class MindclientAutoloader implements ClientModInitializer {
    private static final String PRESELECTED_WORLD_NAME = Objects.requireNonNullElse(System.getenv("defaultWorld"), "Void World");
    private static final ServerInfo PRESELECTED_SERVER = new ServerInfo("SERVER", String.format("%s:%s", Objects.requireNonNullElse(System.getenv("serverIP"), "127.0.0.1"), Objects.requireNonNullElse(System.getenv("serverPort"), "25565")), false);
    protected static final Boolean  AUTOCONNECT_TO_SERVER = (Objects.requireNonNullElse(System.getenv("serverAutoconnect"), "true")).equalsIgnoreCase("true") ;

    private static Boolean SELF_DISCONNECT = false;

    private static final String RETRY_MULTIPLAYER_MESSAGE = "[AUTOLOAD] Retrying server connection";
    private static final String ABORT_MULTIPLAYER_MESSAGE = "[AUTOLOAD] Resorting to singleplayer world";

    @Override
    public void onInitializeClient() {
        ClientLifecycleEvents.CLIENT_STARTED.register(new onStart());
        ClientPlayConnectionEvents.JOIN.register(new onConnect()); 
    }

    static public void disconnect(MinecraftClient client) {
        SELF_DISCONNECT = true;
        client.execute(() -> {
            client.world.disconnect();
            client.disconnect();
            client.setScreen(new MultiplayerScreen(new TitleScreen()));
        });
    }

    static public void reconnect(MinecraftClient client) {
        new onStart().onClientStarted(client);
    }

    static private void retryMultiplayer(String reason) {
        Mindclient.LOGGER.warn("[AUTOLOAD] Disconnected: " + reason);
        Mindclient.LOGGER.info(RETRY_MULTIPLAYER_MESSAGE);
        MindclientAutoloader.connectToServer();
    }

    static private void abortMultiplayer(String reason) {
        Mindclient.LOGGER.warn("[AUTOLOAD] Disconnected: " + reason);
        Mindclient.LOGGER.warn(ABORT_MULTIPLAYER_MESSAGE);
        MindclientAutoloader.loadWorld();
    }

    static public void onDisconnect(Screen screen, TranslatableText title, TranslatableText reason) {
        MinecraftClient.getInstance().setScreen(screen);
        if (title.equals(ScreenTexts.CONNECT_FAILED)) { // Connection failed for some reason
            if (reason.getArgs().length == 0) {
                retryMultiplayer("No reason given");
                return;
            }
            Object reasonObject = reason.getArgs()[0];
            if (reasonObject instanceof TranslatableText) {
                TranslatableText reasonText = (TranslatableText) reasonObject;
                if (reasonText.getKey().equals("disconnect.unknownHost")) abortMultiplayer("Unknown Host");
                else retryMultiplayer(reasonText.getString());
            } else {
                String reasonString = (String) reasonObject;
                if (reasonString.contains("Connection timed out")) retryMultiplayer("Timed Out");
                else if (reasonString.contains("Connection refused")) abortMultiplayer("Connection Refused");
                else retryMultiplayer(reasonString);
            }
        } else { // Player was disconnected for some reason
            if (SELF_DISCONNECT) {
                SELF_DISCONNECT = false;
                Mindclient.LOGGER.info("[AUTOLOAD] Disconnected: Forced Disconnect");
                Mindclient.LOGGER.info("[AUTOLOAD] Waiting for manual action from client");
            } else {
                abortMultiplayer(reason.getString());
            }
        }
    }

    static protected void connectToServer() {
        Mindclient.LOGGER.info("[AUTOLOAD] Attempting server connection");
        MinecraftClient client = MinecraftClient.getInstance();
        client.execute(() -> {
            client.setScreen(new MultiplayerScreen(new TitleScreen()));
            ConnectScreen.connect(
                client.currentScreen,
                client,
                ServerAddress.parse(PRESELECTED_SERVER.address),
            PRESELECTED_SERVER);
        });
    }

    static protected void loadWorld() {
        MinecraftClient client = MinecraftClient.getInstance();
        client.execute(() -> {
            List<LevelSummary> levels;
            try {
                levels = client.getLevelStorage().getLevelList();
            } catch (LevelStorageException e) {
                Mindclient.LOGGER.error("[AUTOLOAD] Could not get level list");
                return;
            }
            if (levels.size() == 0) {
                Mindclient.LOGGER.warn("[AUTOLOAD] No worlds exist to autoload");
                return;
            }
            LevelSummary selectedLevel = levels.get(0);
            for (int i=0; i < levels.size(); i++) {
                if (levels.get(i).getName().equals(PRESELECTED_WORLD_NAME)) {
                    selectedLevel = levels.get(i);
                    return;
                }
                if (i == levels.size() - 1) Mindclient.LOGGER.warn(String.format("[AUTOLOAD] Preselected world '%s' does not exist", PRESELECTED_WORLD_NAME));
            }
            int selectedLevelInt = -1;
            while (selectedLevel.isLocked()) {
                selectedLevelInt++;
                if (levels.size() <= selectedLevelInt) {
                    Mindclient.LOGGER.error("[AUTOLOAD] No unlocked worlds exist to autoload");
                    break;
                }
                selectedLevel = levels.get(selectedLevelInt);
            }
            if (!selectedLevel.isLocked()) {
                Mindclient.LOGGER.info(String.format("[AUTOLOAD] Loading world '%s'", selectedLevel.getName()));
                client.startIntegratedServer(selectedLevel.getName());
            }
        });
    }
}