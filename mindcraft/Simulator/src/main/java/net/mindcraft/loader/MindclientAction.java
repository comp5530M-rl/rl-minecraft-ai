package net.mindcraft.loader;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.util.Identifier;

public class MindclientAction {
    private static MindclientAction INSTANCE;

    private Map<String,KeyBinding> keyMap = new HashMap<String,KeyBinding>() ;
    private static final Identifier ACTION_CHANNEL = new Identifier("modid","action");
    private static Boolean camera_handler = false;
    protected static float camera_x = 180f;
    protected static float camera_y = 0.0f;
    protected static float camera_speed = Float.parseFloat(Objects.requireNonNullElse(System.getenv("camera_move_speed"), "50"));

    public static MindclientAction getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MindclientAction();
        }
        return INSTANCE;
    }

    private MindclientAction(){
        refreshKeypresses();
        if (!camera_handler) {
            camera_handler = true;
            ClientTickEvents.START_CLIENT_TICK.register((c) -> {
                if (c.player != null) {
                    c.player.setPitch(c.player.getPitch() + (camera_y - c.player.getPitch()) * (camera_speed/100));
                    c.player.setYaw(c.player.getYaw() + (camera_x - c.player.getYaw()) * (camera_speed/100));
                }
            });
        }
    }

    public void refreshKeypresses() {
        MinecraftClient client = MinecraftClient.getInstance();
        if (!keyMap.isEmpty()) keyMap = new HashMap<String,KeyBinding>();
        keyMap.put("JUMP", client.options.jumpKey);
        keyMap.put("LEFT", client.options.leftKey);
        keyMap.put("RIGHT", client.options.rightKey);
        keyMap.put("FWD", client.options.forwardKey);
        keyMap.put("BACK", client.options.backKey);
        keyMap.put("ATTACK", client.options.attackKey);
        keyMap.put("USE", client.options.useKey);
        keyMap.put("SNEAK", client.options.sneakKey);
        keyMap.put("SPRINT", client.options.sprintKey);
        keyMap.put("PICK", client.options.pickItemKey);
    }

    /**
     * Loads a scenario into the world
     * @param name The name of the scenario to load, will load file located at '/scenarios/$name$.scenario'
     */
    public void init(String name){
        respawn();
        MinecraftClient client = MinecraftClient.getInstance();
        if (client.isInSingleplayer()) {
            Mindclient.scenarioLoader.loadScenario(name, client.player.getUuidAsString());
        } else {
            PacketByteBuf buf = PacketByteBufs.create();
            buf.writeString("INIT "+name);
            ClientPlayNetworking.send(ACTION_CHANNEL, buf);
        }
    }

    /**
     * Calls a command as the player
     * @param command The command string, as typed in the chat, for example: "/kill &#064;p"
     */
    public void callCommand(String command){
        MinecraftClient client = MinecraftClient.getInstance();
        ClientPlayerEntity player = client.player;
        if (player != null) {
            player.sendChatMessage(command);
        }
    }

    /**
     * Kills all entities around the player
     */
    public void reset(){
        String kill = "/tp @e[distance=..10,type=!player] ~ -100000 ~";
        callCommand(kill);
    }



    /**
     * Sets the camera position of the player absolutely
     * @param h Horizontal degrees ranging -180deg to 180deg
     * @param v Vertical degrees ranging -90deg to 90deg
     */
    public void setCameraAngle(Float h, Float v) {
        camera_x = h;
        camera_y = Math.min(Math.max(v, -90),90);
        Mindclient.LOGGER.info(String.format("Set Camera %f %f", h, v));
    }

    /**
     * Changes the selected hotbar slot
     * @param slot The slot in the hotbar to select
     */
    public void setSelectedSlot(int slot) {
        if (slot < 0 || slot > 8) {
            Mindclient.LOGGER.error(String.format("Invalid slot for HOTBAR command '%s'",slot));
            return;
        }
        MinecraftClient client = MinecraftClient.getInstance();
        client.player.getInventory().selectedSlot = slot;
        Mindclient.LOGGER.info(String.format("Selected Slot %d", slot));
    }

    /**
     * Swaps the selected slot with the given slot
     * @param slot The given slot to swap with
     */
    public void swapSelectedSlot(int slot) {
        if ((slot < 0 || slot > 40)) {
            Mindclient.LOGGER.error(String.format("Invalid slot for HOTBAR command '%s'",slot));
            return;
        }
        MinecraftClient client = MinecraftClient.getInstance();
        client.interactionManager.clickSlot( 0, slot + 36, 0, SlotActionType.PICKUP, client.player);
        client.interactionManager.clickSlot(0, client.player.getInventory().selectedSlot + 36, 0, SlotActionType.PICKUP, client.player);
        client.interactionManager.clickSlot(0, slot + 36, 0, SlotActionType.PICKUP, client.player);
        Mindclient.LOGGER.info(String.format("Swapped Slot %d", slot));
    }

    /**
     * Drops an item from a specified slot.
     * @param slot The given slot to drop from
     */
    public void dropSlot(int slot) {
        MinecraftClient client = MinecraftClient.getInstance();
        if (slot == -1) slot = client.player.getInventory().selectedSlot;
        if (slot < 0 || slot > 40) {
            Mindclient.LOGGER.error(String.format("Invalid slot for DROP command '%s'",slot));
            return;
        }
        client.interactionManager.clickSlot(0, slot + 36, 1, SlotActionType.THROW, client.player);
        Mindclient.LOGGER.info(String.format("Dropped Slot %d", slot));
    }

    /**
     * Sets the camera position of the player relatively
     * @param h Horizontal degrees to move camera in the right direction
     * @param v Vertical degrees to move camera in the upwards direction
     */
    public void setRelativeCameraAngle(Float h, Float v) {
        camera_x += h;
        camera_y = Math.min(Math.max(camera_y-v, -90),90);;
        Mindclient.LOGGER.info(String.format("Moved Camera %f %f", h, v));
    }

    /**
     * Sets key to be pressed
     * @param keyName The action name relating to a key
     */
    public void keyPress(String keyName) {
        KeyBinding key = this.keyMap.remove(keyName);
        Mindclient.LOGGER.info(String.format("Pressing key '%s'", key.getBoundKeyTranslationKey()));
        KeyBinding.setKeyPressed(key.getDefaultKey(), true);
        KeyBinding.onKeyPressed(key.getDefaultKey());
    }

    /**
     * Sets all keys that have not been pressed within this action to be unpressed
     */
    public void unpressKeys() {
        Iterator<KeyBinding> remainingKeys = this.keyMap.values().iterator();
        while (remainingKeys.hasNext()) {
            KeyBinding.setKeyPressed(remainingKeys.next().getDefaultKey(), false);
        }
    }

    /**
     * Respawns the player if the player is dead
     */
    public void respawn() {
        MinecraftClient client = MinecraftClient.getInstance();
        if (!client.player.isAlive()) {
            client.player.requestRespawn();
            Mindclient.LOGGER.info(String.format("Respawned Player [%s]", client.player.getName().asString()));
        }
        else Mindclient.LOGGER.warn(String.format("Cannot Respawn Alive Player [%s]", client.player.getName().asString()));
    }

    /**
     * Takes string action line received from controller, and performs the required action
     * @param action Single action line from a controller action request
     */
    public Boolean handleAction(String action){
        String[] words = action.split(" ");
        switch (words[0]){
            case "INIT":
                if (words.length <= 2) this.init(words.length > 1 ? words[1] : "default");
                else {
                    Mindclient.LOGGER.error(String.format("Required 0 or 1 parameter for INIT command '%s'", action));
                    return false;
                }
                break;
            case "CAMERA":
                if (words.length == 3) this.setRelativeCameraAngle(Float.parseFloat(words[1]),Float.parseFloat(words[2]));
                else {
                    Mindclient.LOGGER.error(String.format("Required 2 parameters for CAMERA command '%s'", action));
                    return false;
                }
                break;
            case "SETCAMERA":
                if (words.length == 3) this.setCameraAngle(Float.parseFloat(words[1]),Float.parseFloat(words[2]));
                else {
                    Mindclient.LOGGER.error(String.format("Required 2 parameters for SETCAMERA command '%s'", action));
                    return false;
                }
                break;
            case "HOTBAR":
                if (words.length == 2) this.setSelectedSlot(Integer.parseInt(words[1]));
                else {
                    Mindclient.LOGGER.error(String.format("Required 1 parameters for HOTBAR command '%s'", action));
                    return false;
                }
                break;
            case "SWAP":
                if (words.length == 2) this.swapSelectedSlot(Integer.parseInt(words[1]));
                else {
                    Mindclient.LOGGER.error(String.format("Required 1 parameters for SWAP command '%s'", action));
                    return false;
                }
                break;
            case "RESPAWN":
                if (words.length == 1) this.respawn();
                else {
                    Mindclient.LOGGER.error(String.format("Required 0 parameters for RESPAWN command '%s'", action));
                    return false;
                }
                break;
            case "DROP":
                if (words.length == 2) this.dropSlot(Integer.parseInt(words[1]));
                else if (words.length == 1) this.dropSlot(-1);
                else {
                    Mindclient.LOGGER.error(String.format("Required 0 or 1 parameters for DROP command '%s'", action));
                    return false;
                }
                break;
            case "DISCONNECT":
                if (words.length == 1) MindclientAutoloader.disconnect(MinecraftClient.getInstance());
                else {
                    Mindclient.LOGGER.error(String.format("Required 0 parameters for DISCONNECT command '%s'", action));
                    return false;
                }
                break;
            case "NONE":
                break;
            default:
                if (this.keyMap.containsKey(words[0])) {
                    this.keyPress(words[0]);
                } else {
                    Mindclient.LOGGER.error(String.format("Undefined action provided '%s'", words[0]));
                    return false;
                }
        }
        return true;
    }
}
