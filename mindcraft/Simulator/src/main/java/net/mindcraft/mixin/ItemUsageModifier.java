package net.mindcraft.mixin;

import java.util.Objects;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import org.spongepowered.asm.mixin.injection.At;

import net.minecraft.item.ItemStack;

@Mixin(ItemStack.class)
public  class ItemUsageModifier {
    private static final float eatModifier = Float.parseFloat(Objects.requireNonNullElse(System.getenv("eatModifier"), "50"));
	// TODO: Add minimum cap for eat speed to avoid desync

	@Inject(at = @At("RETURN"), method = "getMaxUseTime()I", locals = LocalCapture.CAPTURE_FAILEXCEPTION, cancellable = true)
	private void getMaxUseTime(CallbackInfoReturnable<Integer> ci) {
        if (((ItemStack)(Object)this).isFood()) ci.setReturnValue(Math.max(Math.round(ci.getReturnValue() / eatModifier),1));
	}
}
