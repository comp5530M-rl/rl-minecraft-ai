package net.mindcraft.mixin;

import net.mindcraft.loader.MindclientAutoloader;
import net.minecraft.client.gui.screen.DisconnectedScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;


@Mixin(DisconnectedScreen.class)
public abstract class DisconnectDetector {
	@Inject(at = @At("RETURN"), method = "	Lnet/minecraft/client/gui/screen/DisconnectedScreen;<init>(Lnet/minecraft/client/gui/screen/Screen;Lnet/minecraft/text/Text;Lnet/minecraft/text/Text;)V", locals = LocalCapture.CAPTURE_FAILEXCEPTION)
	private void DisconnectedScreen(Screen screen, Text title, Text reason, CallbackInfo ci) {
        MindclientAutoloader.onDisconnect(screen, (TranslatableText) title, (TranslatableText) reason);
	}
    
}