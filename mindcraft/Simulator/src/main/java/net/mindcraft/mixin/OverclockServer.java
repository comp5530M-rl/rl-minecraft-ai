package net.mindcraft.mixin;

import net.minecraft.server.MinecraftServer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

@Mixin(MinecraftServer.class)
public class OverclockServer {
    @ModifyConstant(method = "runServer()V", constant = @Constant(longValue = 50L))
    private long injected(long value) {
        return 50L;
    }
}
