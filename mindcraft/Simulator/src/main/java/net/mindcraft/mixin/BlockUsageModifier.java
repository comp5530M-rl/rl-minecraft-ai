package net.mindcraft.mixin;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;

import java.util.Objects;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;


@Mixin(PlayerEntity.class)
public abstract class BlockUsageModifier {
    private static final float breakModifier = Float.parseFloat(Objects.requireNonNullElse(System.getenv("breakModifier"), "30"));

	@Inject(at = @At("RETURN"), method = "getBlockBreakingSpeed(Lnet/minecraft/block/BlockState;)F", locals = LocalCapture.CAPTURE_FAILEXCEPTION, cancellable = true)
	private void calcBlockBreakingDelta(BlockState state, CallbackInfoReturnable<Float> cir) {
        cir.setReturnValue(cir.getReturnValue() + breakModifier);
	}
    
}