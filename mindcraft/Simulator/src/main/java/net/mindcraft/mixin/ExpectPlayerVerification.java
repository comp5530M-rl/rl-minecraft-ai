package net.mindcraft.mixin;

import net.minecraft.network.ClientConnection;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;

import net.mindcraft.server.MindCraftServer;

import java.util.Objects;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(PlayerManager.class)
public abstract class ExpectPlayerVerification {
	private static final Boolean useVerification = Integer.parseInt(Objects.requireNonNullElse(System.getenv("verificationTime"), "-1")) > 1000;

	@Inject(at = @At("RETURN"), method = "onPlayerConnect(Lnet/minecraft/network/ClientConnection;Lnet/minecraft/server/network/ServerPlayerEntity;)V", locals = LocalCapture.CAPTURE_FAILEXCEPTION)
	private void onPlayerConnect(ClientConnection conn, ServerPlayerEntity player, CallbackInfo ci) {
        if (useVerification) MindCraftServer.getInstance().startVerification(conn, player);
	}
    
}
