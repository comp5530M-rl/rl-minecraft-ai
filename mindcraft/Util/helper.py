import sys

def is_sequence(arg):
    return not hasattr(arg, "strip") and (
        hasattr(arg, "__getitem__") or hasattr(arg, "__iter__")
    )

class BufferedStreamPrinter():
    _buffer = []

    def write(self, line):
        self._buffer.append(line)

    def print(self):
        while self._buffer:
            sys.stdout.write(self._buffer.pop(0))
            sys.stdout.flush()