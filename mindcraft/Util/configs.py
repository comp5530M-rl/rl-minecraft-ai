from typing import Union, Dict, Tuple, Callable
from io import TextIOBase
import sys

class BasicConfigError(Exception):
    pass

class ConfigLockError(BasicConfigError):
    pass

class ConfigWriteError(BasicConfigError):
    pass

class ConfigReadError(BasicConfigError):
    pass

class BaseConfig():
    def __init__(self, values:Union[dict,None] = None, **kwargs: any) -> None:
        self._values: Dict[str, any] = dict()
        self._defaults: Dict[str, Tuple[any, Callable[[any],any]]] = dict()
        self._locked = False
        self._initialise_rules()
        if values != None:
            if isinstance(values, dict):
                self.set_values(values)
            else:
                raise ConfigWriteError("passed 'values' argument must be of type dict")
        self.set_values(kwargs)

    def _set_rule(self, key:str, default_value:any, validator:Callable = str):
        self._defaults[key] = (validator(default_value), validator)

    def _set_value_dict(self):
        for key in self._defaults.keys():
            self._values[key] = self.__getitem__(key)

    def _initialise_rules(self):
        pass

    def __getstate__(self):
        self._set_value_dict()
        values = {}
        for k,i in self._values.items():
            if isinstance(i, TextIOBase) and i.name == "<stdout>":
                values[k] = "__stdout__"
            else:
                values[k] = i
        return (values, self._locked)

    def __setstate__(self, items):
        for k,i in items[0].items():
            if i == "__stdout__":
                items[0][k] = sys.stdout
        self._values = items[0]
        self._locked = items[1]
        self._defaults: Dict[str, Tuple[any, Callable[[any],any]]] = dict()
        self._initialise_rules()

    def __repr__(self):
        self._set_value_dict()
        return "{}(locked={}, values={!r})" % (self.__class__.__name__, self._locked, self._values)
    
    def __str__(self):
        self._set_value_dict()
        s = ["{}(locked={}):".format(self.__class__.__name__,self._locked)] + ["-> {} = {!r}".format(k, v) for k,v in self._values.items()] + ["\n"]
        return "\n".join(s) 
    
    def __setitem__(self, key, value):
        if self._locked:
            raise ConfigLockError("cannot write to config whilst it is in use")
        if not key in self._defaults.keys():
            raise ConfigWriteError("key '{}' is not a valid config option for {}".format(key, self.__class__.__name__))
        try:
            transformed = self._defaults[key][1](value)
            if isinstance(transformed, Exception):
                raise transformed
            self._values[key] = transformed
        except Exception as exc:
            raise ConfigWriteError("value {!r} does not conform to {}.{} rules".format(value, self.__class__.__name__, key)) from exc

    def __getitem__(self, key):
        if not key in self._defaults.keys():
            raise ConfigReadError("key '{}' is not a valid config option for {}".format(key, self.__class__.__name__))
        if not key in self._values.keys():
            return self._defaults[key][0]
        else:
            return self._values[key]

    def __delitem__(self, key):
        if self._locked:
            raise ConfigLockError("cannot delete from config whilst it is in use")
        if not key in self._defaults.keys():
            raise ConfigReadError("key '{}' is not a valid config option for {}".format(key, self.__class__.__name__))
        self._values.__delitem__(key)

    def keys(self):
        return self._defaults.keys()

    def values(self):
        self._set_value_dict()
        return self._values.values()

    def items(self):
        self._set_value_dict()
        return self._values.items()

    def __contains__(self, item):
        return item in self._defaults

    def __iter__(self):
        return iter(self._defaults)
    
    def lock(self):
        self._locked = True

    def unlock(self):
        self._locked = False

    def set_values(self, values:dict):
        for k,v in values.items():
            self.__setitem__(k,v)

    def copy(self):
        return self.__class__(self._values)
    
class ClientGraphicsConfig(BaseConfig):
    def _initialise_rules(self) -> None:
        self._set_rule("graphics_performance_mode",True,bool)
        self._set_rule("hide_hud",True,bool)
        self._set_rule("screen_size",[480, 480],lambda v: list(map(int,v)) if len(list(map(int,v))) == 2 else ValueError("Value must be a size list (x, y) containing exactly 2 integers"))
        self._set_rule("screen_locked",False,bool)
        self._set_rule("camera_move_speed",50,lambda v: float(v) if float(v) <= 100 and float(v) >= 0 else ValueError("Value must be a float between 0 and 100 inclusive"))

class ControllerConfig(BaseConfig):
    def _initialise_rules(self) -> None:
        self._set_rule("blacklisted_ports",[],lambda v: list(map(int,v)))
        self._set_rule("default_singleplayer","Void World")
        self._set_rule("server_ip","127.0.0.1")
        self._set_rule("bridge_port","42000",int)
        self._set_rule("server_port","25565",int)
        self._set_rule("server_autoconnect",True,bool)
        self._set_rule("autostart_client_instance",True,bool)
        self._set_rule("break_modifier",30,lambda v: float(v) if float(v) >= 0 and float(v) <= 50 else ValueError("Break Speed modifier must be a float between 0 and 50 inclusive (efficiency x ~ x^2)"))
        self._set_rule("eat_modifier",100,float)
        self._set_rule("logger_name","Minecraft Client")
        self._set_rule("graphics_config",ClientGraphicsConfig(),lambda v: v if isinstance(v,ClientGraphicsConfig) else ValueError("Value must be an instance of ClientGraphicsConfig"))

class ServerConfig(BaseConfig):
    def _initialise_rules(self) -> None:
        self._set_rule("monitor_frequency",-1,int)
        self._set_rule("client_connection","local_server",lambda v: str(v) if str(v) in ["local_server", "external_server", "singleplayer"] else ValueError("Value not a valid string in [\"local_server\", \"external_server\", \"singleplayer\"]"))
        self._set_rule("server_ip","127.0.0.1")
        self._set_rule("server_port","25565",int)
        self._set_rule("server_world","default")
        self._set_rule("online_mode",False,bool)
        self._set_rule("break_modifier",30,lambda v: float(v) if float(v) >= 0 and float(v) <= 50 else ValueError("Break Speed modifier must be a float between 0 and 50 inclusive (efficiency x ~ x^2)"))
        self._set_rule("eat_modifier",100,float)
        self._set_rule("server_verification_time",-1,int)
        self._set_rule("scenario_spacing",1,lambda v: int(v) if int(v) >= 0 else ValueError("Value must be greater than or equal to 0"))
        self._set_rule("scenario_pos",[100,200,100],lambda v: list(map(int,v)) if len(list(map(int,v))) == 3 else ValueError("Value must be a position coordinate containing exactly 3 integers"))
        self._set_rule("auto_spawn",False,bool)
        self._set_rule("logger_name","Minecraft Server")
        self._set_rule("difficulty","HARD",lambda v: str(v) if str(v) in ["PEACEFUL", "EASY", "NORMAL", "HARD"] else ValueError("Value not a valid string in [\"PEACEFUL\", \"EASY\", \"NORMAL\", \"HARD\"]"))
        self._set_rule("keep_inventory", True, bool)
    
class GlobalConfig(BaseConfig):
    def _initialise_rules(self) -> None:
        self._set_rule("logging_string","%(cb)s[%(time)s]%(ec)s %(logging_colour)s[%(logging_level)s:%(caller_file)s/%(caller_line)s]%(ec)s %(cv)s(%(channel)s)%(ec)s %(message)s")
        self._set_rule("logging_stream",sys.stdout,lambda v:v if callable(getattr(v, "write", None)) else ValueError("Stream must be writable"))
        self._set_rule("logging_level","INFO",lambda v:str(v) if str(v) in ["ERROR","WARN","INFO","DEBUG","NONE"] else ValueError("Value '{!r}' not a valid string in [\"ERROR\", \"WARN\", \"INFO\", \"DEBUG\", \"NONE\"]".format(v)))

GLOBAL_CONFIG = GlobalConfig()