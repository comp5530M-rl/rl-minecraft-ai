from typing import Union
from datetime import datetime
from threading import Thread
import inspect, re

from mindcraft.Util import GLOBAL_CONFIG

class logger:
    def __init__(self) -> None:
        self.config = GLOBAL_CONFIG
        self._history = []
        self._streams = []

    def __getstate__(self):
        return self.config
    
    def __setstate__(self, config):
        self.config = config
        self._history = []
        self._streams = []

    def info(self, message: str, channel: Union[str, None] = None) -> None:
        self._log("INFO", message, channel)

    def warn(self, message: str, channel: Union[str, None] = None) -> None:
        self._log("WARN", message, channel)

    def error(self, message: str, channel: Union[str, None] = None) -> None:
        self._log("ERROR", message, channel)

    def debug(self, message: str, channel: Union[str, None] = None) -> None:
        self._log("DEBUG", message, channel)  

    @staticmethod
    def format_java_string(line: str, defaultChannel = "Minecraft") -> dict:
        match = re.match(r"(?:(?:\x1b\[\d?\d?m| |\t){0,5}?\[(?P<time>.{8})\])?(?:(?:\x1b\[\d?\d?m| |\t){0,5}?\[(?P<caller_line>.*?)/(?P<level>.*?)\])?(?:(?:\x1b\[\d?\d?m| |\t){0,5}?\((?P<channel>.*?)\)(?:\x1b\[\d?\d?m| |\t){0,5}?)?(?P<message>.*?)\n", line)
        
        if not match:
            return None
        data = match.groupdict()
        if not data["message"]:
            return None
        if data["channel"] == None:
            data["channel"] = defaultChannel
        if data["time"] == None:
            del data["time"]
        if data["caller_line"] == None:
            data["caller_line"] = ""
        return data
    
    @staticmethod
    def format_textonly_string(chanel: str):
        return lambda l: {
            "message": l.rstrip(),
            "channel": chanel,
            "level": "INFO",
            "caller_line": "",
        }

    def add_stream(self, stream, formatter, name):
        self._streams.append((stream,formatter,name))
        Thread(name="Logging Stream Monitor", target=self._stream_monitor, args=(len(self._streams)-1,)).start()

    def _stream_monitor(self, pos):
        (stream, validator, name) = self._streams[pos]
        last_level = "INFO"
        for line in iter(stream.readline, ""):
            data = validator(line)
            if isinstance(data,dict):
                data["caller_file"] = name
                if not "level" in data or data["level"] == None:
                    data["level"] = last_level
                last_level = data["level"]
                self._log(**data)

    def _log(self, level:str, message: str, channel: Union[str, None], **kwargs) -> None:
        self.config.lock()
        caller = inspect.getouterframes(inspect.currentframe(),2)
        formatting_vars = {
            "ec":'\33[0m',
            "cr":'\33[31m',
            "cg":'\33[32m',
            "cy":'\33[33m',
            "cb":'\33[34m',
            "cv":'\33[35m',
            "cw":'\33[37m',
            "time": datetime.now().strftime("%H:%M:%S"),
            "logging_level":level,
            "caller_file":caller[2].filename.split("\\")[-1],
            "caller_line":caller[2].lineno,
            "channel":caller[2].function if channel == None else channel,
            "message":message,
            **kwargs
        }
        formatting_vars["logging_colour"] = {
            "WARN":formatting_vars["cy"],
            "ERROR":formatting_vars["cr"],
            "INFO":formatting_vars["cg"],
            "DEBUG":formatting_vars["cb"],
        }[formatting_vars["logging_level"]]

        message_string = self.config["logging_string"] % formatting_vars
        self._output(formatting_vars["logging_level"], message_string)

    def _output(self, level:str, message_string: str):
        ranks = {
            "WARN":2,
            "ERROR":3,
            "DEBUG":0,
            "INFO":1,
            "NONE":999,
        }
        self._history.append(message_string)
        if len(self._history) > 100:
            self._history.pop(0)
        if ranks[level] >= ranks[self.config["logging_level"]]:
            self.config["logging_stream"].write(message_string+"\n")
            if callable(getattr(self.config["logging_stream"], "flush", None)):
                self.config["logging_stream"].flush()

LOGGER = logger()