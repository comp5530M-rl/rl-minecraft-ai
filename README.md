# Project MindCraft

This project explores the usage of a sandbox environment such as Minecraft and explores how it can be utilised to create a generally intelligent embodied agent using deep neural networks. The goal is to use techniques such as  video pretraining and self-supervised learning to teach our agent how to navigate and interact within it's environment, and perform arbitrarily more difficult tasks as time progresses.

## Libraries & Languages

| Name | Description | Version | Installation |
| --- | --- | --- | --- |
| Python | We used python as our main programming language for this project | 3.11.0 | [Website](https://www.python.org/downloads/release/python-3110/) |
| PyTorch | Python Machine Learning Library used to develop most commonly neural networks | 1.13.0 | `pip3.11 install torch==1.13.0+cu117 torchvision==0.14.0+cu117 torchaudio==0.13.0 --extra-index-url https://download.pytorch.org/whl/cu117` |

## Attainable Goals

| Task | Description | Achieved? |
| --- | --- | --- |
| Navigate to a target | | :x: |
| Find a tree | | :x: |
| Break a tree | | :x: |
| Kill a mob | | :x: |
| Survive one night | | :x: |

## Techniques Used

| Technique | Description | Implemented? |
| --- | --- | --- |
| Sensory Inputs (Visual/Audio/Instinctive) | Use of render, audio and instinctive measures such as health/hunger  | :x: |
| Decision Making RL | Train our model to be able to make decisions | :x: |
| Fitness Scoring | Use of either built-in minecraft scoring, achievement based scoring to track progress or a custom defined points based system for specific environments | :x: |
| Specialised Environments | Use of different environments to allow our model to generalise training | :x: |
| YouTube Frame Capture | Capture frames from relevant YouTube videos to use in training | :x: |
| Input Preprocessing | Preprocess our inputs as to not overwhelm our model and reduce training times | :x: |
| Video Pretraining | Feed data from videos to train our network | :x: |
| Incremental Learning | Start with a basic task and slowly build up to more difficult tasks | :x: |
